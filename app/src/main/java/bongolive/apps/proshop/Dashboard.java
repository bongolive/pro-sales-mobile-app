/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;


import android.accounts.Account;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import bongolive.apps.proshop.db.ContentProviderEngine;
import bongolive.apps.proshop.db.Settings;
import bongolive.apps.proshop.db.Stock;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.GpsTracker;
import bongolive.apps.proshop.networking.LocationTrigger;
import bongolive.apps.proshop.networking.MCrypt;

public class Dashboard extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final int REQUEST_ENABLE_BT = 2;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final int REQUEST_PRINT = 1;
    private static  int CONNECTIONSTATUS = 0;
    DrawerLayout drawerLayout;
    Account mAccount;
    AlertDialogManager alert;
    ContentResolver mContentResolver ;
	Uri orderitems,customers,orders ;
	AppPreference appPreference  ;
    GpsTracker gpsTracker;
    BluetoothAdapter mBtAdapter;
    int accounttype;
    Button start;
    ProgressBar progressBar;
    TextView txtsyncsta;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private boolean firstrun;
    String extra = null;

    boolean tabletSize = false;
    ActionBar actionBar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		appPreference = new AppPreference(getApplicationContext());
		String languageToLoad = appPreference.getDefaultLanguage();

		alert = new AlertDialogManager();
		Locale locale = new Locale(languageToLoad);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
		setContentView(R.layout.dashboard);

        extra = Constants.SEARCHSOURCE_MAIN;
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
		gpsTracker = new GpsTracker(this);

		mAccount = Constants.createSyncAccount(this);
		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		firstrun = appPreference.check_firstrun();
		if (firstrun) {
			validate(Constants.TAGREGISTRATION);
			appPreference.set_firstrun();
			appPreference.save_last_sync(Constants.getDateOnly2());
		} else {
			String auth = appPreference.getAuthkey();
			String authtyping = null;
			if(!auth.isEmpty()){

				String im = Constants.getIMEINO(this);
				im = im.concat("|");
				int acctype = appPreference.get_accounttype();
				if(acctype == 0)
					authtyping = Constants.ACCOUNT_TYPE_NORMAL;
				if(acctype == 1)
					authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

				MCrypt mCrypt = new MCrypt();
				try {
					String crypted = new String(mCrypt.decrypt(auth));
					if(im.concat(authtyping).equals(crypted)){
					if(acctype == 1){
						String time = appPreference.getDefaultSync();
						if (time.isEmpty()) {
							time = "2130000";
						}
						int t = Integer.parseInt(time);
						if (Stock.getStockCount(this) < 1 || Settings.getSettingsCount(this) < 1) {
							Toast.makeText(this, getString(R.string.strsyncsstartstatus), Toast
									.LENGTH_SHORT)
									.show();
							ondemandsync();
						} else {
							periodicadapersync();
						}
					} else {
						Toast.makeText(this,
								getString(R.string.normalaccountmsg),
								Toast.LENGTH_SHORT).show();
					}
					} else {
						validate("validate");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				validate(Constants.TAGREGISTRATION);
			}
		}
		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
		alert = new AlertDialogManager();

        if(tabletSize)
            drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));


		scheduleAlarm();
//        Constants.exportPrintableFile(this);
		String address = appPreference.getAddress();
		if (mBtAdapter != null) {
			if (!mBtAdapter.isEnabled()) {
				getAddress();
			} else {
				if (address.isEmpty())
					getAddress();
			}
		} else {
			getAddress();
			Log.v("bluetoothaddress", " address is " + address);
		}


	}
    public void getAddress(){
        Intent selectDevice = new Intent(this, UniversalBluetoothInstance.class);
        startActivityForResult(selectDevice, REQUEST_PRINT);
    }
	@Override
	public void onNavigationDrawerItemSelected(int position) {
		int newposition = position+1 ; 
		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle args = new Bundle() ;
		switch(newposition)
		{
		case 1:
            extra = Constants.SEARCHSOURCE_HOME;
			Home bb = new Home();
			args.putInt(ARG_SECTION_NUMBER, newposition);
			bb.setArguments(args) ;
			fragmentManager.beginTransaction().
			replace(R.id.container,bb). 
			commit();
			break; 
		case 2:
		   extra = Constants.SEARCHSOURCE_ORDERS;
		   NewOrdersFragment oo = new NewOrdersFragment();
			args.putInt(ARG_SECTION_NUMBER, newposition);
			oo.setArguments(args) ;
			fragmentManager.beginTransaction().
			replace(R.id.container, oo).
			addToBackStack(ARG_SECTION_NUMBER).
			commit();
			break;
		case 3:
            extra = Constants.SEARCHSOURCE_CUSTOMERS;
			CustomersFragment cc = new CustomersFragment(); 
			args.putInt(ARG_SECTION_NUMBER, newposition);
			cc.setArguments(args);
			fragmentManager.beginTransaction().
			replace(R.id.container, cc).
			addToBackStack(ARG_SECTION_NUMBER).
			commit();
			break;
		case 4:
            extra = Constants.SEARCHSOURCE_PRODUCTS;
			ProductsFragment pp = new ProductsFragment(); 
			args.putInt(ARG_SECTION_NUMBER, newposition);
			pp.setArguments(args);
			fragmentManager.beginTransaction().
			replace(R.id.container,pp).
			addToBackStack(ARG_SECTION_NUMBER).
			commit();
			break;
		case 5:
           /* FiscalPrinter sfr = new FiscalPrinter();
			args.putInt(ARG_SECTION_NUMBER, newposition);
			sfr.setArguments(args);
			fragmentManager.beginTransaction().
			replace(R.id.container,sfr).
			addToBackStack(ARG_SECTION_NUMBER).
			commit();*/
            Intent intent = new Intent(this,SearchTest.class);
            startActivity(intent);
			break;
		case 6:
            if (accounttype == 1) {

                AuditingFrag adf = new AuditingFrag();
                args.putInt(ARG_SECTION_NUMBER, newposition);
                adf.setArguments(args);
                fragmentManager.beginTransaction().
                        replace(R.id.container, adf).
                        addToBackStack(ARG_SECTION_NUMBER).
                        commit();
            } else {
                FiscalPrinter fp = new FiscalPrinter();
                args.putInt(ARG_SECTION_NUMBER, newposition);
                fp.setArguments(args);
                fragmentManager.beginTransaction().
                        replace(R.id.container, fp).
                        addToBackStack(ARG_SECTION_NUMBER).
                        commit();
            }
            break;
            case 7:/*
			FiscalPrinter fp = new FiscalPrinter();
			args.putInt(ARG_SECTION_NUMBER, newposition);
			fp.setArguments(args);
			fragmentManager.beginTransaction().
			replace(R.id.container,fp).
			addToBackStack(ARG_SECTION_NUMBER).
			commit();*/
			break;
		}
	}

	public void onSectionAttached(int number) {

			switch (number) {
				case 1:
					mTitle = getString(R.string.title_section1);
					break;
				case 2:
					mTitle = getString(R.string.title_section2);
					break;
				case 3:
					mTitle = getString(R.string.title_section3);
					break;
				case 4:
					mTitle = getString(R.string.title_section4);
					break;
				case 5:
					mTitle = getString(R.string.title_section5);
					break;
			}

	}

	public void restoreActionBar() {

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);  
		actionBar.setTitle( Html.fromHtml("<font color=#ffffff>" +mTitle+ " </font>") );
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
	}
	
	public void restoreOnBackStack()
	{
		getSupportFragmentManager().addOnBackStackChangedListener(
				new FragmentManager.OnBackStackChangedListener() {
					@Override
					public void onBackStackChanged() {
						restoreActionBar();
					}
				}) ;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			getMenuInflater().inflate(R.menu.dashboard, menu);
            String address = appPreference.getAddress();
            if(address.isEmpty()){
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strscanpair));
                CONNECTIONSTATUS = 1;
            } else {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strclearpair));
                CONNECTIONSTATUS = 2;
            }
            restoreActionBar();
			restoreOnBackStack();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.action_syncnow:
			ondemandsync();/*
			Intent in = new Intent(this, UploadPhoto.class);
			startActivity(in);*/
			break;
        case R.id.action_pair:
                if(CONNECTIONSTATUS == 2){
                    appPreference.clearAddress();
                    Log.v("ADDRESS"," address cleared now it is "+appPreference.getAddress());
                } else if (CONNECTIONSTATUS == 1) {
                    getAddress();
                }
              break;
        }
		return super.onOptionsItemSelected(item);
	}


	/*
	 * running the sync adapter periodically
	 */
	public void periodicadapersync()
	{
		String pr = null;
		pr = appPreference.getDefaultSync();
		if(pr == "")
			pr = "43200000";
		int time = Integer.valueOf(pr);		
		ContentResolver.addPeriodicSync(
				mAccount,
				ContentProviderEngine.AUTHORITY,
				Bundle.EMPTY, time) ; 
	} 
	/*
	 * running the sync adapter on demand
	 */
		public void ondemandsync()
		{  
			Bundle settingsBundle = new Bundle() ;
			settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
			settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
			ContentResolver.requestSync(mAccount, ContentProviderEngine.AUTHORITY, settingsBundle);
		}

     @Override
    protected void onStart() {
        super.onStart();
         setnavdrawer();
		 if(accounttype == 1)
       turnGpsOn() ;
    }

    public void turnGpsOn(){
            Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", true);
            sendBroadcast(intent);
    }
    public void turnGpsOf(){

            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", false);
            sendBroadcast(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        setnavdrawer();
		if(accounttype == 1)
         turnGpsOn();
    }

    @Override
    protected void onPause() {
		super.onPause();
		if(accounttype == 1)
        gpsTracker.stopUsingGPS();
    }
    @Override
    protected void onStop() {
        super.onStop();
		if(accounttype == 1) {
			gpsTracker.stopUsingGPS();
			turnGpsOf();
		}
    }


    public void scheduleAlarm() {
        Intent intent = new Intent(getApplicationContext(), LocationTrigger.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, LocationTrigger.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis();
        long intervalMillis = 0;
        int minutesintervar = Settings.getTrackTime(this);
        if(minutesintervar == 0){
            intervalMillis = Integer.parseInt(Constants.DEFAULTTRACKINTERVAL);
        } else {
            String interval = String.valueOf(minutesintervar);
            long ii = Long.valueOf(interval);
            intervalMillis = TimeUnit.MINUTES.toMillis(ii);
        }
        if(intervalMillis != 0) {
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode) {
            case REQUEST_PRINT: {
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Device connectted address "+appPreference.getAddress(),Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Device not connectted",Toast.LENGTH_LONG).show();
                }
                break;
            }
            case REQUEST_ENABLE_BT: {
				if (!mBtAdapter.isEnabled()) {
					Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				}
				break;
			}
        }
    }

	private void validate(String source){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		txtsyncsta = (TextView)dialog.findViewById(R.id.txtbody);
		start = (Button)dialog.findViewById(R.id.btnstart);
		progressBar.setVisibility(View.VISIBLE);
		txtsyncsta.setVisibility(View.VISIBLE);

		new DeviceRegistration(dialog.getContext(),progressBar,txtsyncsta,start).execute(source);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String auth = appPreference.getAuthkey();
				String authtyping = null;
				if (!auth.isEmpty()) {

					String im = Constants.getIMEINO(dialog.getContext());
					im = im.concat("|");
					int acctype = appPreference.get_accounttype();
					if (acctype == 0)
						authtyping = Constants.ACCOUNT_TYPE_NORMAL;
					if (acctype == 1)
						authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

					MCrypt mCrypt = new MCrypt();
					try {
						String crypted = new String(mCrypt.decrypt(auth));
						if (im.concat(authtyping).equals(crypted)) {
							dialog.dismiss();
						} else {
							dialog.dismiss();
							invalidaccount();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					invalidaccount();
					dialog.dismiss();
				}
			}
		});
		dialog.show();
	}

	private void invalidaccount(){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		TextView txthead = (TextView)dialog.findViewById(R.id.txtheader);
		start = (Button)dialog.findViewById(R.id.btnstart);
		txthead.setText(getString(R.string.strtokenerror));
		txthead.append("\n\n");
		txthead.append(getString(R.string.strtokenerrormsg));

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		};
		Handler handler = new Handler();
		handler.postDelayed(runnable, 15000);

		start.setVisibility(View.VISIBLE);
		start.setText(getString(R.string.strclose));
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		});
		dialog.show();
	}
    @Override
    public void startActivity(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            intent.putExtra(Constants.SEARCHSOURCE, extra);
        }

        super.startActivity(intent);
    }

    private void setnavdrawer(){
        if(tabletSize){
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            drawerLayout.setScrimColor(Color.TRANSPARENT);
        }
    }
}
