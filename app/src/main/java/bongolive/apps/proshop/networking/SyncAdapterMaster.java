/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package bongolive.apps.proshop.networking;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import bongolive.apps.proshop.AppPreference;
import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.MultimediaContents;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.OrderItems;
import bongolive.apps.proshop.db.Settings;
import bongolive.apps.proshop.db.Stock;
import bongolive.apps.proshop.db.Tracking;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SyncAdapterMaster extends AbstractThreadedSyncAdapter
{
	
	private static final String TAG = "SyncAdapter";
	ContentResolver mContentResolver ;
    AppPreference preference;
	/**
	 * @param context
	 * @param autoInitialize
	 */
	public SyncAdapterMaster(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		mContentResolver = context.getContentResolver() ;
	}
	
	//android 3.0 and above compatibility
	public SyncAdapterMaster(Context context, boolean autoInitialize, boolean allowParallelSync)
	{
		super(context, autoInitialize, allowParallelSync);
		mContentResolver = context.getContentResolver() ;
	}

	/* (non-Javadoc)
	 * @see android.content.AbstractThreadedSyncAdapter#onPerformSync(android.accounts.Account, android.os.Bundle, java.lang.String, android.content.ContentProviderClient, android.content.SyncResult)
	 */
	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
        preference = new AppPreference(getContext());
        int accounttype = preference.get_accounttype();
        if(accounttype == 1) {
		/*
		 * This is where all the methods for uploading and download are done
		 */
            Log.d(TAG, "*********** start of *********   onPerformSync for account[" + account.name + "]");

            try {
                Log.d(TAG, "SEND CUSTOMERS processing");
                Customers.sendCustomersToServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "STOCK processing");
                Stock.saveStock(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Log.d(TAG, "GET CUSTOMERS processing");
                Customers.getCustomersFromServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "TRACKER processing");
                Tracking.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "order processing");

                Order.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "SETTINGS processing");
                Settings.saveSettingsAndMode(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.v(TAG, "orderItems processing");
                OrderItems.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Log.v(TAG, "multimedia processing");
                MultimediaContents.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }/*
        try {
            Log.v(TAG, "mcrypt testing processing");
            Customers.getEncryptedString(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }*/


            Log.d(TAG, "*********** end of *********   onPerformSync for account[" + account.name + "]");
        }
//        preference.save_last_sync("04-30");
        String date = Constants.getDateOnly2();
        String lastsyc = preference.getLastSync();
        if(date.compareTo(lastsyc) >= 1){
            Log.v("date"," its more than a month"+date.compareTo(lastsyc));
            Settings.perform_account_validation(getContext());
        }
	}
 
}
