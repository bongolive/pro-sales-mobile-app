/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.os.Bundle;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 */
public class Authenticator extends AbstractAccountAuthenticator
{
    private final Context mContext;
	/**
	 * @param context
	 */
    public Authenticator(Context context) {
        super(context);
        mContext = context;
    }
	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#addAccount(android.accounts.AccountAuthenticatorResponse, java.lang.String, java.lang.String, java.lang.String[], android.os.Bundle)
	 * The user has requested to add a new account to the systme. We return an intent that will launch
	 * the login screen if the user has not yet logged in otherwise we just pass the user's credentials to the account manager
	 */
	@Override
	public Bundle addAccount(
			AccountAuthenticatorResponse response,
			String string1,
			String string2,
			String[] stringarray, 
			Bundle bundle)
			throws NetworkErrorException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#confirmCredentials(android.accounts.AccountAuthenticatorResponse, android.accounts.Account, android.os.Bundle)
	 */
	@Override
	public Bundle confirmCredentials(
			AccountAuthenticatorResponse response,
			Account account,
			Bundle bundle) throws NetworkErrorException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#editProperties(android.accounts.AccountAuthenticatorResponse, java.lang.String)
	 */
	@Override
	public Bundle editProperties(
			AccountAuthenticatorResponse response,
			String string1) { 
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#getAuthToken(android.accounts.AccountAuthenticatorResponse, android.accounts.Account, java.lang.String, android.os.Bundle)
	 */
	@Override
	public Bundle getAuthToken(
			AccountAuthenticatorResponse response,
			Account account,
			String string1, 
			Bundle bundle) throws NetworkErrorException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#getAuthTokenLabel(java.lang.String)
	 */
	@Override
	public String getAuthTokenLabel(String string1) {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#hasFeatures(android.accounts.AccountAuthenticatorResponse, android.accounts.Account, java.lang.String[])
	 */
	@Override
	public Bundle hasFeatures(
			AccountAuthenticatorResponse response, 
			Account account,
			String[] stringarray) throws NetworkErrorException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see android.accounts.AbstractAccountAuthenticator#updateCredentials(android.accounts.AccountAuthenticatorResponse, android.accounts.Account, java.lang.String, android.os.Bundle)
	 */
	@Override
	public Bundle updateCredentials(
			AccountAuthenticatorResponse response,
			Account account, 
			String string1,
			Bundle bundle)
			throws NetworkErrorException {
		
		return null;
	}
	
}
