/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by nasznjoka on 3/26/15.
 */
public class LocationTrigger extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.bongolive.apps.prostock.alarm";

    @Override
    public void onReceive(Context context, Intent intent) {

       Intent i = new Intent(context, MyLocationServices.class);
       i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               Log.v("SRVICETRIGGER","The location service is triggeed");
            context.startService(i);
    }
}
