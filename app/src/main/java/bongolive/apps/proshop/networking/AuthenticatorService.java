/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 */
public class AuthenticatorService extends Service{
	private Authenticator mAuthenticator ;
	private static String TAG ="AuthenticatorService" ;
	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public void onCreate(){
		if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "SampleSyncAdapter Authentication Service started.");
        }
		mAuthenticator = new Authenticator(this);
	}
	 @Override
	    public void onDestroy() {
	        if (Log.isLoggable(TAG, Log.VERBOSE)) {
	            Log.v(TAG, "SampleSyncAdapter Authentication Service stopped.");
	        }
	    }
	 
	 @Override
	    public IBinder onBind(Intent intent) {
	        if (Log.isLoggable(TAG, Log.VERBOSE)) {
	            Log.v(TAG,
	                "getBinder()...  returning the AccountAuthenticator binder for intent "
	                    + intent);
	        }
	        return mAuthenticator.getIBinder();
	    }

}
