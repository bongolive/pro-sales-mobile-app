/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.app.Application;
import android.content.SharedPreferences;

public class AppController extends Application {

	public static final String TAG = AppController.class.getSimpleName(); 
	SharedPreferences preferences;
	boolean ranBefore;
	private static AppController mInstance;
	private static final String STRINGKEY = "prostock";

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
	}

	public static synchronized AppController getInstance() { 
		return mInstance;
	} 

	public boolean isFirstTime()
	{
	    preferences = getSharedPreferences(STRINGKEY, MODE_PRIVATE);
	    ranBefore = preferences.getBoolean("RanBefore", false);
	    if (!ranBefore) {
	        SharedPreferences.Editor editor = preferences.edit();
	        editor.putBoolean("RanBefore", true);
	        editor.commit();
	    }

	    return ranBefore;
	}
}
