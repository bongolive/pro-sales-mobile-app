/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.Xml;
import android.view.Display;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.AppPreference;
import bongolive.apps.proshop.OrderItemList;
import bongolive.apps.proshop.db.ContentProviderEngine;

public class Constants {

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE = "proshop.bongolive.co.tz";
    public static final String DEVICE_ACCOUNTTYPE = "account_type";
	public static final String ACCOUNT= "ProShop" ;
	public static final String SERVER = "http://www.pro.co.tz/login/index.php/en/api/web_service";
	public static final String SERVERAUTHENTICITY = "http://www.pro.co.tz/login/index.php/en/api/web_service/authenticate";
    public static final String AUTHTOKEN = "auth_key";
    public static final String AUTHTOKENSERVER = "authentication_key";
    public static final String DEVICEACCOUNTTYPESERVER = "account";
    public static final String FIRSTRUN = "first_run";
    public static final String TAGREGISTRATION = "register_device";
    public static final String AUTHENTICATETAG = "authenticate";
    public static final String ACCOUNT_TYPE_NORMAL = "normal";
    public static final String ACCOUNT_TYPE_PREMIUM = "premium";
    public static final String LASTSYNC = "last_sync";
    public static final String FRESHSYNC = "current_sync";
    public static final String TAGSPRODUCT = "assign_products";
    public static final String SEARCHSOURCE = "search_initilizer";
    public static final String SEARCHSOURCE_PRODUCTS = "products_fragment";
    public static final String SEARCHSOURCE_CUSTOMERS = "customers_fragment";
    public static final String SEARCHSOURCE_ORDERS = "orders_fragment";
    public static final String SEARCHSOURCE_MAIN = "dashboard";
    public static final String SEARCHSOURCE_HOME = "home_fragment";
    //    public static final String SERVER = "http://192.168.1.211/promobile/webservice/pro_web_services.php";
	public static String TAGSTOCKONCE = "assign_products"; 
	public static String TAGSTOCKUPDATE = "assign_products"; 
	public static String TAGORDERS = "upload_orders"; 
	public static String TAGSITEM = "upload_orders_items"; 
	public static String TAGSENDCUSTOMER = "upload_customers"; 
	public static String TAGSENDPRODUCT = "upload_product";
	public static String TAGSENDSTOCK = "upload_stock";
	public static String TAGGETCUSTOMER = "assign_customers";
	public static String TAGSETTINGS = "assign_settings";
    public static String TAGLOCATIONUPDATES = "device_tracking";
    public static String TAGMULTIMEDIA = "multi_media_data";
    public static String DEFAULTTRACKINTERVAL = "5400000";
	public static String SUCCESS = "success" ; 
	public static String FAILURE = "failure";
	public static String MULT_IMAGE = "image";
	public static String MULT_SOUND = "sound";
	public static String MULT_VIDEO = "video";
	public static String ACKVALUE = "1" ;
	public static String ACK = "ack" ;
	public static String TAGACK = "acktag" ;
	public static String IMEI = "imei" ;
	public static String ACKREFERENCE = "ref";
	public static String ACKKEY = "key" ;
    public static String PREFCURRENCY = "default_currency_key";
    public static String PREFSYNC = "sync_frequency";
    public static String PREFLANG = "default_language";
    public static String PREFDISCOUNT = "enable_discount_key";
    public static String PREFTAX = "include_vat_key";
    public static String PREFRECEIPT = "print_recpt_key";
    public static String PREFFIXEDPRICE = "fixed_price_key";
    public static String PREFLIMITPRICE = "limit_discount_key";
    public static String PREFADDRESS = "address";
    public static String PREFALLOWPRINTWITHOUTSTANDINGPAYMENT = "print_recpt_with_nopayment";
    public static String RCP ="RCP";
    public static String TRANSACTION ="TRANSACTIONS";
    public static String PAYMENTS="PAYMENTS";
    public static String TRN ="TRN";
    public static String PMT ="PMT";//PAYMENTS
    public static String TAX_GROUP ="TAX_GROUP";
    public static String NAME ="NAME";
    public static String SPRICE ="SPRICE";
    public static String QTY ="QTY";
    public static String MEAS_UNIT ="MEAS_UNIT";
    public static String PAY_TYPE ="PAY_TYPE";
    public static String PAY_AMOUNT ="PAY_AMOUNT";
    public static String PDISCOUNT ="PDISCOUNT";
    public static String NDISCOUNT ="NDISCOUNT";
    public static String PMARKUP ="PMARKUP";
    public static String CLIENT_DATA ="CLIENT_DATA";

    public static String ADDRESS ="ADDRESS";
    public static String CLIENTID ="ID";
    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * Authtoken type string.
     */
    public static final String AUTHTOKEN_TYPE =  "com.example.android.samplesync"; 
	private static String TAG = "prosales_account" ;
    
	public static final long SYNC_PER_USER(Context context)
	{
		long sync = 0 ;  
        try{
        	sync = Integer.parseInt(Constants.getIMEINO(context).substring(0, 8))/2 ; 
            return sync;
        } catch(NumberFormatException e)
        {
        	e.toString();
        }
        return 0 ;
	}
	
    public static Account createSyncAccount(Context context)
	{ 
		Account newaccount = new Account(ACCOUNT, ACCOUNT_TYPE) ; 
		AccountManager mAccountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE) ;
		 
		if(mAccountManager.addAccountExplicitly(newaccount, null, null)) 
		{ 
			ContentResolver.setSyncAutomatically(newaccount, ContentProviderEngine.AUTHORITY, true); 
			Log.v(TAG, "PROSALES ACCOUNT IS GOOD TO GO") ;
		} else {
			Log.v(TAG, "PROSALES MOBILE  ACCOUNT FAILED TO BE SET UP") ;
		}
		return newaccount;
		
	}

    public static String getIMEINO(Context ctx) {
		TelephonyManager tManager = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);
		String imeiid = tManager.getDeviceId();
		return imeiid;
	}
    
    public static boolean isNotConnected(Context ctx) {
		try {
			ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();

			if (netInfo != null && netInfo.isConnected()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
    
    public static String getDate() {
		Date update = null;
		String returnstring = null ;
		try {
		  update = new Date(); 

		  String[] formats = new String[] { 
				   "yyyy-MM-dd HH:mm" 
				 };
		  
		  for(String df: formats)
		  {
			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
			  
			  return sdf.format(update);
		  }
		} finally {
		
		}
		return returnstring;
	}

    public static String getDateOnly() {
        Date update = null;
        String returnstring = null ;
        try {
            update = new Date();

            String[] formats = new String[] {
                    "MM-dd HH:mm"
            };

            for(String df: formats)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String getDateOnly2() {
        Date update = null;
        String returnstring = null ;
        try {
            update = new Date();

            String[] formats = new String[] {
                    "MM-dd"
            };

            for(String df: formats)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String getYearDateMonth() {
        Date update = null;
        String returnstring = null ;
        try {
            update = new Date();

            String[] formats = new String[] {
                    "yyyy-MM-dd"
            };

            for(String df: formats)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String compareDate() {
  		Date update = null;
  		String returnstring = null ;
  		try {
  		  update = new Date();  
		  
  		  String[] formats = new String[] { 
  				   "yyyy-MM-dd" 
  				 };
  		  
  		  for(String df: formats)
  		  {
  			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
  			  
  			  return sdf.format(update);
  		  }
  		} finally {
  		
  		}
  		return returnstring;
  	}
    
    public static String compareDateWeek() {
  		Date update = null;
  		String returnstring = null ;
  		try {
  		  update = new Date(); 

  	   	  Calendar calendar = Calendar.getInstance();
	   	  calendar.setTime(update);
		  calendar.add(Calendar.DAY_OF_MONTH, -7);
		  Date newDate = calendar.getTime();
		  
  		  String[] formats = new String[] { 
  				   "yyyy-MM-dd HH:mm" 
  				 };
  		  
  		  for(String df: formats)
  		  {
  			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
  			  
  			  return sdf.format(newDate);
  		  }
  		} finally {
  		
  		}
  		return returnstring;
  	}
    
    public static String compareDateMonth() {
  		Date update = null;
  		String returnstring = null ;
  		try {
  		  update = new Date(); 

  	   	  Calendar calendar = Calendar.getInstance();
	   	  calendar.setTime(update);
		  calendar.add(Calendar.DAY_OF_MONTH, -30);
		  Date newDate = calendar.getTime();
		  
  		  String[] formats = new String[] { 
  				   "yyyy-MM" 
  				 };
  		  
  		  for(String df: formats)
  		  {
  			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
  			  
  			  return sdf.format(newDate);
  		  }
  		} finally {
  		
  		}
  		return returnstring;
  	}
     
    
    public static void LogException(Exception ex) {
		Log.d(TAG +" Exception",
				TAG+"Exception -- > " + ex.getMessage() + "\n");
		ex.printStackTrace();
	}
  
   public static String getPriceAfterBeforeTaxs(double price, double tax, int taxyes){
	   double finalprice = 0 ;
	   switch(taxyes)
	   {
	   case 1:
		   finalprice = price + price*(tax/100);
		   
		   return String.valueOf(finalprice) ; 
	   case 0:
		   return String.valueOf(price) ; 
	   }
	   return null ;
   }

    public static double calculateTotalWithTax(Context context, double price, int qty, double taxrate){
        AppPreference preference = new AppPreference(context);
        BigDecimal outpt = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty

        BigDecimal tax = new BigDecimal(taxrate).divide(new BigDecimal(100));//tax/100
        BigDecimal totaltax = outpt.multiply(tax);//taxrate*totalsale

        BigDecimal finaloutput = outpt.add(totaltax);//totalsale+taxrate

        return finaloutput.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double calculateTotalNoTax(Context context, double price, int qty, double taxrate){
        AppPreference preference = new AppPreference(context);
        BigDecimal outpt = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty


        return outpt.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

   public static int exportPrintableFile(Context context) {
       final String xmlData = "rcpREQ";
       try{
           File file = new File(Environment.getExternalStorageDirectory().getPath()+"/Download/"+xmlData+".xml");

           FileOutputStream fileOutputStream1 = new FileOutputStream(file);
           XmlSerializer serializer = Xml.newSerializer();
           StringWriter stringWriter = new StringWriter();
           serializer.setOutput(stringWriter);
           serializer.startDocument("UTF-8", true);
           serializer.startTag(null, RCP);
           serializer.startTag(null, TRANSACTION);
           serializer.startTag(null, TRN);
           serializer.startTag(null, TAX_GROUP);
           serializer.text("1");
           serializer.endTag(null, TAX_GROUP);
           serializer.startTag(null, NAME);
           serializer.text("Korosho");
           serializer.endTag(null, NAME);
           serializer.startTag(null, SPRICE);
           serializer.text("25.00");
           serializer.endTag(null, SPRICE);
           serializer.startTag(null, QTY);
           serializer.text("9");
           serializer.endTag(null, QTY);
           serializer.endTag(null, TRN);

           serializer.startTag(null, TRN);
           serializer.startTag(null, TAX_GROUP);
           serializer.text("1");
           serializer.endTag(null, TAX_GROUP);
           serializer.startTag(null, NAME);
           serializer.text("Karanga");
           serializer.endTag(null, NAME);
           serializer.startTag(null, SPRICE);
           serializer.text("15.00");
           serializer.endTag(null, SPRICE);
           serializer.startTag(null, QTY);
           serializer.text("3");
           serializer.endTag(null, QTY);
           serializer.startTag(null, PDISCOUNT);
           serializer.text("5.00");
           serializer.endTag(null, PDISCOUNT);
           serializer.endTag(null, TRN);

           serializer.endTag(null, TRANSACTION);
           serializer.startTag(null, PAYMENTS);
           serializer.startTag(null, PMT);
           serializer.startTag(null, PAY_TYPE);
           serializer.text("0");
           serializer.endTag(null, PAY_TYPE);
           serializer.startTag(null, PAY_AMOUNT);
           serializer.text("160.00");
           serializer.endTag(null, PAY_AMOUNT);
           serializer.endTag(null, PMT);
           serializer.endTag(null, PAYMENTS);
           serializer.endTag(null,RCP);
           serializer.endDocument();
           serializer.flush();

           String datawriter = stringWriter.toString();
           fileOutputStream1.write(datawriter.getBytes());
           fileOutputStream1.close();
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return 0;
   }
   /* public static String writeXml(List<Message> messages){
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "messages");
            serializer.attribute("", "number", String.valueOf(messages.size()));
            for (Message msg: messages){
                serializer.startTag("", "message");
                serializer.attribute("", "date", msg.getDate());
                serializer.startTag("", "title");
                serializer.text(msg.getTitle());
                serializer.endTag("", "title");
                serializer.startTag("", "url");
                serializer.text(msg.getLink().toExternalForm());
                serializer.endTag("", "url");
                serializer.startTag("", "body");
                serializer.text(msg.getDescription());
                serializer.endTag("", "body");
                serializer.endTag("", "message");
            }
            serializer.endTag("", "messages");
            serializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }*/
   public static int writeXml(List<OrderItemList> itemLists){
       final String xmlData = "rcpREQ";
       try{
           File file = new File(Environment.getExternalStorageDirectory().getPath()+"/Download/"+xmlData+".xml");
           File sdcard = Environment.getExternalStorageDirectory() ;
           File folder = new File(sdcard.getAbsoluteFile(),"ProShop");

           if (!folder.exists()) {
               folder.mkdirs();
           }

           String j = Constants.getDateOnly();
           File file2 = new File(folder.getAbsoluteFile(), j+".xml") ;

           FileOutputStream fileOutputStream1 = new FileOutputStream(file);
           FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
           XmlSerializer serializer = Xml.newSerializer();
           StringWriter stringWriter = new StringWriter();
           serializer.setOutput(stringWriter);

           serializer.startDocument("UTF-8", true);
           serializer.startTag(null, RCP);
           serializer.startTag(null, TRANSACTION);
           double itemSales = 0;
           for (OrderItemList orderItemList: itemLists){
               serializer.startTag(null, TRN);
               serializer.startTag(null, TAX_GROUP);
               serializer.text("1");
               serializer.endTag(null, TAX_GROUP);
               serializer.startTag(null, NAME);
               serializer.text(orderItemList.getProductname());
               serializer.endTag(null, NAME);
               serializer.startTag(null, SPRICE);
               serializer.text(String.valueOf(orderItemList.getUnitprice()));
               serializer.endTag(null, SPRICE);
               serializer.startTag(null, QTY);
               serializer.text(String.valueOf(orderItemList.getQuantity()));
               serializer.endTag(null, QTY);
               double singlesales = orderItemList.getGrandtotal();//without taxes
               itemSales += singlesales;
               serializer.endTag(null, TRN);
           }
           serializer.endTag(null, TRANSACTION);
          /* serializer.startTag(null, PAYMENTS);
           serializer.startTag(null, PMT);
           serializer.startTag(null, PAY_TYPE);
           serializer.text("0");
           serializer.endTag(null, PAY_TYPE);
           serializer.startTag(null, PAY_AMOUNT);
           serializer.text(String.valueOf(itemSales));
           serializer.endTag(null, PAY_AMOUNT);
           serializer.endTag(null, PMT);
           serializer.endTag(null, PAYMENTS);*/
           serializer.endTag(null, RCP);
           serializer.endDocument();
           String datawriter = stringWriter.toString();
           fileOutputStream1.write(datawriter.getBytes());
           fileOutputStream2.write(datawriter.getBytes());
           fileOutputStream1.close();
           fileOutputStream2.close();
           return 1;
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return 0;
   }

        // convert from bitmap to byte array
        public static byte[] getBytes(Bitmap bitmap) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
            return stream.toByteArray();
        }

        // convert from byte array to bitmap
        public static Bitmap getImage(byte[] image) {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else if (Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT){
            return data.getByteCount();
        } else{
            return data.getAllocationByteCount();
        }
    }

    public static String getSizeInKb(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static String getbase64photo(String imagpath) {
        String mCurrentPhotoPath = null;
//        Bitmap bm = BitmapFactory.decodeFile(imagpath);
        Bitmap bm = decodeFile(imagpath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        mCurrentPhotoPath = Base64.encodeToString(ba, Base64.DEFAULT);
//        Log.v("fileuri",mCurrentPhotoPath);
//        Log.v("fileuri",imagpath);
        return mCurrentPhotoPath;
    }
    public static Bitmap decodeFile(String filePath) {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);

        return  bitmap ;
    }


    public static String getBase64Vid(final String url) {try {
        File file = new File(url);
        byte[] bFile = new byte[(int) file.length()];
        Log.v("filelength", " " + file.length());
        FileInputStream inputStream = new FileInputStream(url);
        inputStream.read(bFile);
        inputStream.close();
//        Log.v("filecontent", Base64.encodeToString(bFile, Base64.NO_PADDING));
//        return Base64.encodeb(bFile, Base64.DEFAULT);
        return bongolive.apps.proshop.networking.Base64.encodeBytes(bFile);
    } catch (IOException e) {
        Log.e(TAG, Log.getStackTraceString(e));
    }
        return null;
    }

    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
        }

        return hexStr;
    }

    public static String generateBatch(){
        String batch = null;

        long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        batch = String.valueOf(number);
        return  batch;
    }

    public static void setDateTimeField(Context context, final EditText fromDateEtxt) {

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }
    public static int istablet(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int measuredWidth = 0;
        int measuredHeight = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            wm.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y;
        } else {
            Display d = wm.getDefaultDisplay();
            measuredWidth = d.getWidth();
            measuredHeight = d.getHeight();
        }
        Log.v("size","width "+measuredWidth+ " height "+measuredHeight);
        if(measuredWidth >= 600){
            //this is a tablet
            return 1;
        }
        return 0;
    }
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}