/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.networking;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import bongolive.apps.proshop.db.Tracking;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyLocationServices extends IntentService {


    public MyLocationServices() {
        super("LocationServices");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        turnGpsOn();

        Log.v("GPSON", "GPS IS TURNED ON");
        GpsTracker gpsTracker = new GpsTracker(getApplicationContext());
        double lat = gpsTracker.getLatitude();
        double lng = gpsTracker.getLongitude();
        double[] vals = {lat,lng};
        if(!vals.equals(0))
            Tracking.storeLocation(getApplicationContext(), vals);
        Log.v("GPSOF", "GPS IS TURNED OF");

        gpsTracker.stopUsingGPS();
        turnGpsOf();
    }
    public void turnGpsOn(){
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
//        sendBroadcast(intent);
    }
    public void turnGpsOf(){

        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
//        sendBroadcast(intent);
    }
}
