/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.net.Uri;

import java.io.Serializable;

/**
 * @author nasznjoka
 * 
 * Oct 9, 2014
 *
 */
public class StockItemList implements Serializable{
	String productname ;
	int quantity ;
	double price ;
	String id ;
	int prodid;
	String expirydate;

	public int getProdid() {
		return prodid;
	}


	public String getProductname() {
		return productname;
	}


	public String getExpirydate() {
		return expirydate;
	}


	public int getQuantity() {
		return quantity;
	}


	public double getPrice() {
		return price;
	}

   	public String getId(){
		return id ;
	}

	public StockItemList(String p, int q, String i, double pr, int prid){
		productname = p;
		quantity = q;
		id = i;
		price = pr;
		prodid = prid;
	}
	public StockItemList(String p, int q, String i, Uri img){
		productname = p;
		quantity = q;
		id = i;
	}
	public StockItemList(String p, int q, String i, double pr, int prid, String expirydate_){
		productname = p ;
		quantity = q;
		price = pr ; //grand total pr*qtyand tax
		id = i;
		prodid = prid; // prodid
		expirydate = expirydate_;
	} 

}
