/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package bongolive.apps.proshop;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.ref.SoftReference;
import java.util.Hashtable;

/**
 * @author nasznjoka
 *
 */
public class CustomFont extends TextView
{
    public CustomFont(Context fontext)
    {
    	super(fontext) ;
    }
    public CustomFont(Context fontext, AttributeSet fontAttr)
    {
          super(fontext, fontAttr) ;	
          ProstockUtil.setCustomFont(this, fontext, fontAttr
        		  , R.styleable.bongolive_apps_proshop_CustomFont
        		  ,R.styleable.bongolive_apps_proshop_CustomFont_font ) ;
    }
    public CustomFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ProstockUtil.setCustomFont(this,context,attrs,
                R.styleable.bongolive_apps_proshop_CustomFontButton,
                R.styleable.bongolive_apps_proshop_CustomFontButton_font);
    }
    
    public static class ProstockUtil {
    	  public static final String TAG = "ProsalesUtil" ;
    	 
    	      public static void setCustomFont(View txtViewOrBtn, Context ctx, AttributeSet attrs, int[] attribSet, int fontid)
    	      {
    	    	  TypedArray tarray = ctx.obtainStyledAttributes(attrs, attribSet) ;
    	    	  String cFont = tarray.getString(fontid);
    	    	  setCustomFont(txtViewOrBtn, ctx, cFont);
    	    	  tarray.recycle() ;
    	    	  }
    	      
    	      private static boolean setCustomFont(View txtViewOrBtn,Context ctx,String asset)
    	      {
    	    	  if(TextUtils.isEmpty(asset)) 
    	    		  return false ;
    	    	  Typeface tp = null ;
    	    	  try
    	    	  {
    	    		  tp= getFont(ctx, asset);
    	    		  if(txtViewOrBtn instanceof TextView)
    	    		  {
    	    			  ((TextView)txtViewOrBtn).setTypeface(tp) ;
    	    		  } else
    	    		  {
    	    			  ((Button)txtViewOrBtn).setTypeface(tp) ;
    	    		  }
    	    	  } catch (Exception ex)
    	    	  {
    	    		  Log.e(TAG, "couldn't get the fonts"+ asset, ex) ;
    	    		  return false ;
    	    	  }
    	    	  return true ;
    	      }
    	      
    	      private static Hashtable<String,SoftReference<Typeface>> fontCache = new Hashtable<String ,SoftReference<Typeface>>() ;
    	     
    	      public static Typeface getFont(Context cont, String fontName)
    	      {
    	    	synchronized (fontCache) 
    	    	{
    		    if(fontCache.get(fontName)!= null)
    		    {
    		    	SoftReference<Typeface> ref = fontCache.get(fontName) ;
    		    	if(ref.get()!= null)
    		    	{
    		    		return ref.get() ;
    		    	}
    			}
    		    Typeface typ = Typeface.createFromAsset(cont.getAssets(), "fonts/"+fontName) ;
    		    fontCache.put(fontName, new SoftReference<Typeface>(typ)) ;
    		   
    		    return typ ;
    	    	}
    	    }
    	}
}
