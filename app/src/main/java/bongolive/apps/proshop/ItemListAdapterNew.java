/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.db.Products;

public class ItemListAdapterNew extends BaseAdapter implements View.OnClickListener, MenuItem.OnMenuItemClickListener {
    int action;
    private LayoutInflater mInflater;
    private List<ItemList> mItems = new ArrayList<ItemList>();
    Context _context ;
    MenuItem[] menuitems;
    ItemViewHolder holder;
    ItemList cl;
    ArrayList<String> deletearray = new ArrayList<>();
    public ItemListAdapterNew() {

    }

    public ItemListAdapterNew(Context context, List<ItemList> items, int source, MenuItem[] menuItems) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        action = source;
        menuitems = menuItems;
        _context = context;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ItemViewHolder();
            holder.txtName = (TextView) convertView.findViewById(R.id.txtitem);
            holder.txtName1 = (TextView) convertView.findViewById(R.id.txtitem1);
            holder.txtName2 = (TextView) convertView.findViewById(R.id.txtitem2);
            holder.txtName3 = (TextView) convertView.findViewById(R.id.txtitemextra);
            holder.txtName4 = (TextView) convertView.findViewById(R.id.txtitem4);
            holder.chkdelete = (CheckBox) convertView.findViewById(R.id.chkdrop);
            holder.imgedit = (ImageView) convertView.findViewById(R.id.imgedit);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        cl = mItems.get(position);

        holder.txtName.setText(cl.getitemname());
        holder.txtName1.setText(cl.getitemname1());
        holder.txtName2.setText(cl.getitemname2());
        String item3 = cl.getitemname3();
        if (Validating.areSet(item3)) {
            holder.txtName3.setVisibility(View.VISIBLE);
            holder.txtName3.setText(item3);
        }
        if (cl.getId() < 1) {
            holder.chkdelete.setVisibility(View.GONE);
            holder.imgedit.setVisibility(View.GONE);
        }
        if (action == 0) {
            holder.imgedit.setVisibility(View.GONE);
        }
//		if(!cl.getitemname4().isEmpty()){
        holder.txtName4.setVisibility(View.VISIBLE);
        holder.txtName4.setText(cl.getitemname4());
//		}

        long id = cl.getId();


        return convertView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.chkdrop:
                if (deletearray.size() > 0)
                    if (holder.chkdelete.isChecked()) {
                        menuitems[0].setVisible(true);
                        menuitems[1].setVisible(false);
                        deletearray.add(String.valueOf(cl.getId()));
                    } else {
                        deletearray.remove(String.valueOf(cl.getId()));
                    }
                break;
            case R.id.imgedit:
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_delete:
                switch (action){
                    case 1:
                        break;
                    case 2 :
                        if (deletearray.size() > 0) {
                            for (int i = 0; i < deletearray.size(); i++) {
                                Products.deleteProduct(_context, deletearray.get(i));
                            }
                            notifyDataSetChanged();
                        }
                        break;
                }
                break;
        }
        return false;
    }

    static class ItemViewHolder {
        TextView txtName;
        TextView txtName1;
        TextView txtName2;
        TextView txtName3;
        TextView txtName4;
        CheckBox chkdelete;
        ImageView imgedit;
    }

}
