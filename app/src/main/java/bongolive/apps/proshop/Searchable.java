package bongolive.apps.proshop;

import android.app.Dialog;
import android.app.SearchManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.fiscalprinter.tza.FMP10TZA;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.OrderItems;
import bongolive.apps.proshop.db.Products;
import bongolive.apps.proshop.networking.Constants;

/**
 * Created by nasznjoka on 6/3/15.
 */
public class Searchable extends ActionBarActivity {
    private ListView mListView;
    TextView tv, txtindex,txtname,txtinfo ;
    ActionBar ab;
    Dialog dialog;

    boolean tabletSize = false;
    String source = null;
    private static final int REQUEST_PRINT = 1;
    LinearLayout lheader;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.search_view);

        tv = (TextView)findViewById(R.id.resultlable) ;
        txtindex = (TextView)findViewById(R.id.txtresultindex) ;
        txtname = (TextView)findViewById(R.id.txtresultname) ;
        txtinfo = (TextView)findViewById(R.id.txtresultinfo) ;
        lheader = (LinearLayout)findViewById(R.id.layheader) ;

        mListView = (ListView)findViewById(R.id.searchresultlist);
        parseIntent(getIntent()) ;
        ab = getSupportActionBar();

        ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
    }
    @Override
    protected void onNewIntent(Intent intent)
    {
        setIntent(intent) ;
        parseIntent(intent);
    }

    public void parseIntent(Intent intent)
    {
        if(Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            String Searchquery = intent.getStringExtra(SearchManager.QUERY);

            source = intent.getStringExtra(Constants.SEARCHSOURCE);
            Log.v("received","data received is "+source);
            showResults(Searchquery);
        }
    }

    private void showResults(String q) {

        ContentResolver cr = getContentResolver();
        Cursor _c ;
        switch (source){
            case Constants.SEARCHSOURCE_PRODUCTS:
                 _c = cr.query(Products.BASEURI, null, Products.PRODUCTNAME + " LIKE \'%" + q + "%\'", null,
                         Products.PRODUCTNAME + " ASC") ;
                if(_c.getCount() > 0)
                {
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(getString(R.string.strsearchresults) + " " +q);
                    tv.setTextColor(Color.RED);
                    lheader.setVisibility(View.VISIBLE);
                    txtname.setText(getString(R.string.strproductname));
                    txtinfo.setText(getString(R.string.strunitpricetinclusive));
                    String[] from = new String[]{Products.ID , Products.PRODUCTNAME,Products.UNITPRICE} ;
                    int[] to = new int[]{R.id.index,R.id.productname, R.id.productprice} ;
                    SimpleCursorAdapter sca = new SimpleCursorAdapter(this, R.layout.search_item, _c, from, to) ;

                    mListView.setAdapter(sca);
                    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                            showProduct(id, Products.getProdName(Searchable.this, String.valueOf(id)));
                        }
                    });
                } else {
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(getString(R.string.strnoproduct)+ " "+q+ ". "+ getString(R.string.strtryagain));
                }
                break;
            case Constants.SEARCHSOURCE_CUSTOMERS:
                 _c = cr.query(Customers.BASEURI, null, Customers.BUZINESSNAME + " LIKE \'%" + q + "%\' OR "+
                                 Customers.CUSTOMERNAME + " LIKE \'%" + q + "%\'", null, null) ;
                if(_c.getCount() > 0)
                {
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(getString(R.string.strsearchresults) + " "+q);
                    tv.setTextColor(Color.RED);
                    lheader.setVisibility(View.VISIBLE);
                    txtname.setText(getString(R.string.strbusiness));
                    txtinfo.setText(getString(R.string.strclientname));
                    String[] from = new String[]{Customers.ID , Customers.BUZINESSNAME,Customers.CUSTOMERNAME} ;
                    int[] to = new int[]{R.id.index,R.id.productname, R.id.productprice} ;
                    SimpleCursorAdapter sca = new SimpleCursorAdapter(this, R.layout.search_item, _c, from, to) ;

                    mListView.setAdapter(sca);
                    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                            showCustomer( id, Customers.getCustomerBusinessNameLoca(Searchable.this, String.valueOf(id)));
                        }
                    });
                } else {
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(getString(R.string.strnocustomer)+ " "+q+ ". "+ getString(R.string.strtryagain));
                }
                break;
            case Constants.SEARCHSOURCE_ORDERS:
                int customerid = Customers.getCustomerlocalId(this, q);
//                ArrayList<String> custlist = Customers.getSearchedCustomer(this,q);
                Log.v("custid"," "+customerid);


                _c = cr.query(Order.BASEURI, null, Order.LOCALCUSOMER + " = "+ customerid  , null, null) ;
                if(_c.getCount() > 0)
                {
                    final String custname = Customers.getCustomerBusinessNameLoca(this, String.valueOf(customerid));
                    Log.v("custname"," "+custname);
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(getString(R.string.strsearchresults) + " " + custname);
                    tv.setTextColor(Color.RED);
                    lheader.setVisibility(View.VISIBLE);
                    txtindex.setText(getString(R.string.stramountpaid));
                    txtname.setText(getString(R.string.strtotalcost));
                    txtinfo.setText(getString(R.string.strorderdate));
                    String[] from = new String[]{Order.PAYMENTAMOUNT , Order.TOTALSALES,Order.ORDERDATE} ;
                    int[] to = new int[]{R.id.index,R.id.productname, R.id.productprice} ;
                    SimpleCursorAdapter sca = new SimpleCursorAdapter(this, R.layout.search_item, _c, from, to) ;

                    mListView.setAdapter(sca);
                    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                          showOrders(id, custname);
                        }
                    });
                } else {
                tv.setVisibility(View.VISIBLE);
                tv.setText(getString(R.string.strnorder)+ " "+q+ ". "+ getString(R.string.strtryagain));
            }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, getIntent());
                return true;
            default:
                return false;
        }
    }
    private void showCustomer(final long i, final String businessname){
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.addcustomer, null) ;

        final EditText clname,shname,phone,email,address ;

        dialog = new Dialog(this, R.style.CustomDialog	);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        clname = (EditText)dialog.findViewById(R.id.etclientname);
        shname = (EditText)dialog.findViewById(R.id.etshopname);
        phone = (EditText)dialog.findViewById(R.id.etclientphone);
        email = (EditText)dialog.findViewById(R.id.etclientemail);
        address = (EditText)dialog.findViewById(R.id.etclientshopaddress) ;
        TextView txttitle = (TextView)dialog.findViewById(R.id.txttitle);
        txttitle.setText(businessname);


        ImageView goback = (ImageView)dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button edit = (Button)dialog.findViewById(R.id.btnSubmit);


        clname.setFocusable(false);
        shname.setFocusable(false);
        email.setFocusable(false);
        phone.setFocusable(false);
        address.setFocusable(false);
        clname.setFocusableInTouchMode(false);
        shname.setFocusableInTouchMode(false);
        phone.setFocusableInTouchMode(false);
        address.setFocusableInTouchMode(false);
        email.setFocusableInTouchMode(false);
        edit.setVisibility(View.GONE);



        final String[] details = Customers.getCustomer(this, i);
        clname.setText(details[0]);
        shname.setText(details[1]);
        email.setText(details[2]);
        phone.setText(details[3]);
        address.setText(details[4]);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cl = clname.getText().toString();
                String sh  = shname.getText().toString();
                String em = email.getText().toString();
                String ph = phone.getText().toString();
                String add = address.getText().toString();
                int userid = Customers.getCustomerlocalId(dialog.getContext(), details[1]);
                String[] vals = {cl,sh,ph,em,add,String.valueOf(userid)};
                String[] vals2 = {cl,sh,ph,add};
                if(Validating.areSet(vals2)){
                    int update = Customers.updateCustomerDetails(dialog.getContext(), vals);
                    if(update > 0){
                        Toast.makeText(dialog.getContext(), "You have edited customer details", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(dialog.getContext(), "No update for customer details",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }
            }

        });
        dialog.show();
    }


    private void showProduct(final long i, String prodname) {
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.addproduct, null);

        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final EditText etprodname, etreorder, etprice, ettax, etsku, etdesc, etqty, etupdate;
        TextView txttile, lastup, qtyinstock;

        etprodname = (EditText) dialog.findViewById(R.id.etprodname);
        etreorder = (EditText) dialog.findViewById(R.id.etreorder);
        etprice = (EditText) dialog.findViewById(R.id.etunitprice);
        ettax = (EditText) dialog.findViewById(R.id.ettax);
        etsku = (EditText) dialog.findViewById(R.id.etsku);
        etdesc = (EditText) dialog.findViewById(R.id.etdescr);
        txttile = (TextView) dialog.findViewById(R.id.txttitle);
        lastup = (TextView) dialog.findViewById(R.id.txtlastsell);
        qtyinstock = (TextView) dialog.findViewById(R.id.txtitemonhand);
        txttile.setText(prodname);
        etqty = (EditText) dialog.findViewById(R.id.etitemonhand);
        etupdate = (EditText) dialog.findViewById(R.id.etlastupdate);

        Button edit = (Button) dialog.findViewById(R.id.btnSubmit);
        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        String[] details = Products.getProducts(this, i);
        //name,price,update,itemonhand,taxrate,reorder,description,sku

            //view
            etqty.setVisibility(View.VISIBLE);
            etupdate.setVisibility(View.VISIBLE);
            lastup.setVisibility(View.VISIBLE);
            qtyinstock.setVisibility(View.VISIBLE  );
            etqty.setFocusable(false);
            etqty.setFocusableInTouchMode(false);
            etupdate.setFocusable(false);
            etupdate.setFocusableInTouchMode(false);
            etprodname.setFocusable(false);
            etprodname.setFocusableInTouchMode(false);
            etprice.setFocusable(false);
            etprice.setFocusableInTouchMode(false);
            ettax.setFocusable(false);
            ettax.setFocusableInTouchMode(false);
            etsku.setFocusable(false);
            etsku.setFocusableInTouchMode(false);
            etdesc.setFocusable(false);
            etdesc.setFocusableInTouchMode(false);
            etreorder.setFocusable(false);
            etreorder.setFocusableInTouchMode(false);

            edit.setVisibility(View.GONE);


            etprodname.setText(details[0]);
            etprice.setText(details[1]);
            ettax.setText(details[4]);
            etsku.setText(details[7]);
            etdesc.setText(details[6]);
            etupdate.setText(details[2]);
            etqty.setText(details[3]);
            etreorder.setText(details[5]);


        dialog.show();
    }


    public void showOrders(final long id, final String businessname) {
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.orderitemslist, null);

        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ListView listView = (ListView) dialog.findViewById(R.id.list);
        ItemListAdapterOrderItems adapterOrderItems = new ItemListAdapterOrderItems(this, getListOrderItem(id));
        listView.setAdapter(adapterOrderItems);
        //
        TextView txt = (TextView) dialog.findViewById(R.id.txttitle);
        txt.setText(businessname);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btn = (Button) dialog.findViewById(R.id.btnprint);
        int printed = Order.getPrintStatus(dialog.getContext(), id);
        if (printed == 0) {
            btn.setVisibility(View.VISIBLE);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<OrderItemList> listarray = OrderItems.getItemsArrayList(dialog.getContext(), id);
                    if (!listarray.isEmpty()) {
                        int print = 0;
                        AppPreference appPreference = new AppPreference(Searchable.this);
                        String address = appPreference.getAddress();
                        if (address.isEmpty()) {
                            Intent selectDevice = new Intent(Searchable.this, PrintUtils.class);
                            Bundle b = new Bundle();
                            b.putSerializable("key", listarray);
                            selectDevice.putExtras(b);
                            selectDevice.putExtra("customer", businessname);
                            startActivityForResult(selectDevice, REQUEST_PRINT);
                            print = 1;
                        } else {
                            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter().getDefaultAdapter();
                            final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
                            final BluetoothSocket socket;
                            try {
                                socket = device.createRfcommSocketToServiceRecord(UniversalBluetoothInstance.SPP_UUID);
                                socket.connect();
                                final InputStream in = socket.getInputStream();
                                final OutputStream out = socket.getOutputStream();
                                FMP10TZA fmp10TZA = new FMP10TZA(in, out);
                                print = nonFiscalReceipt(fmp10TZA, listarray, businessname);
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                        if (print == 1) {
                            int orderid = Integer.parseInt(String.valueOf(id));

                            Order.putPrintStatus(dialog.getContext(), new int[]{orderid, 1});
                        }
                    }
                }
            });
        }
        dialog.show();
    }

    private int nonFiscalReceipt(FMP10TZA mFMP, ArrayList<OrderItemList> itemLists, String custname) {
        try {
            mFMP.openFiscalCheck();

            mFMP.cmd54v0b0(" *   " + custname.toUpperCase() + "   *");
            for (OrderItemList itemList : itemLists) {
                String prodname = itemList.getProductname();
                double price = itemList.getUnitprice();
                int qty = itemList.getQuantity();
                double tax = itemList.getTax();
//                price = price + (price * tax / 100);
                String txtgrp = "";
                if (tax == 0) {
                    txtgrp = "B";
                } else if (tax == 18) {
                    txtgrp = "A";
                } else {
                    txtgrp = "B";
                }

                mFMP.sellThis(prodname, txtgrp, String.valueOf(price), String.valueOf(qty));
            }
            mFMP.totalInCash();
            mFMP.closeFiscalCheck();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public List<ItemListOrderItems> getListOrderItem(long i) {
        ContentResolver cr = getContentResolver();
        String where = OrderItems.ORDERID + " = " + i;
        Cursor c = cr.query(OrderItems.BASEURI, null, where, null, null);
        List<ItemListOrderItems> list = new ArrayList<ItemListOrderItems>();
        try {
            if (c.moveToFirst()) {
                do {
                    long itemid = c.getLong(c.getColumnIndexOrThrow(OrderItems.ID));

                    String[] order = OrderItems.getOrderItems(this, itemid);
                    list.add(new ItemListOrderItems(order[0], order[1], order[2], order[3], order[4], itemid));
                } while (c.moveToNext());
            } else {
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }
}