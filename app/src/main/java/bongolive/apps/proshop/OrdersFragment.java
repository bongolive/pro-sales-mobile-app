/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.fiscalprinter.tza.FMP10TZA;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.DatabaseHandler;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.OrderItems;
import bongolive.apps.proshop.db.Stock;

@SuppressLint("DefaultLocale")
public class OrdersFragment extends Fragment implements OnCreateContextMenuListener, AdapterView.OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final int REQUEST_PRINT = 1;
    public static int MAKE_SALE = 1;
    View _rootView;
    DatabaseHandler dh;
    ListView lv;
    ItemListAdapter adapter;
    ItemListAdapterOrderItems adapterOrderItems;
    ArrayList<ItemList> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_items, container, false);
        } else {
            ((ViewGroup) _rootView.getParent()).removeView(_rootView);
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));


        lv = (ListView) getActivity().findViewById(R.id.list);

//        OrderItems.deleteall(getActivity());

        adapter = new ItemListAdapter(getActivity(), getList(), 0);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        lv.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {

            @Override
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenuInfo menuInfo) {
                menu.add("delete").setTitle(getString(R.string.strdelete));
                menu.add("edit").setTitle(getString(R.string.stredit));
            }

        });
        /*CheckBox exportdata = new CheckBox(getActivity());
        ExportDatabase ex = new ExportDatabase(getActivity(), exportdata);
        ex.execute();*/
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.add_sales, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = cr.query(Order.BASEURI, null, null, null, Order.ID + " DESC");
        List<ItemList> list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Order.PAYMENTAMOUNT);
                    int colP = c.getColumnIndex(Order.CUSTOMERID);
                    int colI = c.getColumnIndex(Order.ID);
                    int colD = c.getColumnIndex(Order.ORDERDATE);
                    int colT = c.getColumnIndex(Order.TOTALSALES);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    String d = c.getString(colD);
                    String t = c.getString(colT);
                    String[] bname = Customers.getCustomers(getActivity(), Long.parseLong(p));
                    // i need price here
                    long i = c.getLong(colI);
                    list.add(new ItemList(getString(R.string.strshop).toUpperCase(Locale.getDefault()) +
                            ": " + bname[1].toUpperCase(Locale.getDefault()), getString(R.string.strordercost).toUpperCase(Locale.getDefault()) + ": " + t, getString(R.string.stramountpaid).toUpperCase(Locale.getDefault()) + ": " + n,
                            i,
                            getString(R.string.strtime).toUpperCase(Locale.getDefault()) + ": " + d));
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, "", p, i, d));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object o = lv.getItemAtPosition(position);
        ItemList list1 = (ItemList) o;
        long i = list1.getId();
        String businessname = list1.getitemname();
        Log.v("id", "id is " + i);
        if (!businessname.isEmpty() && i > 0)
            showOrders(i, businessname);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_addsales) {
            int stock = Stock.getStockCount(getActivity());
            AlertDialogManager alertDialogManager = new AlertDialogManager();
            if (stock > 0) {
                Intent intent = new Intent(getActivity(), OrdersActivity.class);
                startActivityForResult(intent, MAKE_SALE);
                return true;
            } else {
                alertDialogManager.showAlertDialog(getActivity(), getString(R.string.strerror), getString(R.string.strnostock), false);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int menuItemIndex = 0;
        if (item.getTitle() == getActivity().getString(R.string.strdelete)) {
            menuItemIndex = 1;
        } else if (item.getTitle() == getActivity().getString(R.string.stredit)) {
            menuItemIndex = 2;
        } else if (item.getTitle() == getActivity().getString(R.string.strpaydebts)) {
            menuItemIndex = 3;
        }
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
        ItemList title = getList().get(menuInfo.position);
        String items = String.valueOf(title.getId());
        String business = title.getitemname();
        String amount = title.getitemname1();
        switch (menuItemIndex) {
            case 1:
                Toast.makeText(getActivity(), "delete business " + business + " amount " + amount, Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(getActivity(), "edit business " + business + " amount " + amount, Toast.LENGTH_LONG).show();
                break;
            case 3:
                break;
        }
        return true;
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        getActivity();
        if (resCode == Activity.RESULT_OK) {
            adapter = new ItemListAdapter(getActivity(), getList(), 0);
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);
            Toast.makeText(getActivity(), getString(R.string.strorderadded), Toast.LENGTH_LONG).show();
        } else {
            getActivity();
            if (resCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), getString(R.string.strordercanceled), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showOrders(final long id, final String businessname) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.orderitemslist, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ListView listView = (ListView) dialog.findViewById(R.id.list);
        adapterOrderItems = new ItemListAdapterOrderItems(getActivity(), getListOrderItem(id));
        listView.setAdapter(adapterOrderItems);
        //
        TextView txt = (TextView) dialog.findViewById(R.id.txttitle);
        txt.setText(businessname);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btn = (Button) dialog.findViewById(R.id.btnprint);
        int printed = Order.getPrintStatus(dialog.getContext(), id);
        if (printed == 0) {
            btn.setVisibility(View.VISIBLE);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<OrderItemList> listarray = OrderItems.getItemsArrayList(dialog.getContext(), id);
                    if (!listarray.isEmpty()) {
                        int print = 0;
                        AppPreference appPreference = new AppPreference(getActivity());
                        String address = appPreference.getAddress();
                        if (address.isEmpty()) {
                            Intent selectDevice = new Intent(getActivity(), PrintUtils.class);
                            Bundle b = new Bundle();
                            b.putSerializable("key", listarray);
                            selectDevice.putExtras(b);
                            selectDevice.putExtra("customer", businessname);
                            startActivityForResult(selectDevice, REQUEST_PRINT);
                            print = 1;
                        } else {
                            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter().getDefaultAdapter();
                            final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
                            final BluetoothSocket socket;
                            try {
                                socket = device.createRfcommSocketToServiceRecord(UniversalBluetoothInstance.SPP_UUID);
                                socket.connect();
                                final InputStream in = socket.getInputStream();
                                final OutputStream out = socket.getOutputStream();
                                FMP10TZA fmp10TZA = new FMP10TZA(in, out);
                                print = nonFiscalReceipt(fmp10TZA, listarray, businessname);
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                        if (print == 1) {
                            int orderid = Integer.parseInt(String.valueOf(id));

                            Order.putPrintStatus(dialog.getContext(), new int[]{orderid, 1});
                        }
                    }
                }
            });
        }
        dialog.show();
    }

    private int nonFiscalReceipt(FMP10TZA mFMP, ArrayList<OrderItemList> itemLists, String custname) {
        try {
            mFMP.openFiscalCheck();

            mFMP.cmd54v0b0(" *   " + custname.toUpperCase() + "   *");
            for (OrderItemList itemList : itemLists) {
                String prodname = itemList.getProductname();
                double price = itemList.getUnitprice();
                int qty = itemList.getQuantity();
                double tax = itemList.getTax();
//                price = price + (price * tax / 100);
                String txtgrp = "";
                if (tax == 0) {
                    txtgrp = "B";
                } else if (tax == 18) {
                    txtgrp = "A";
                } else {
                    txtgrp = "B";
                }

                mFMP.sellThis(prodname, txtgrp, String.valueOf(price), String.valueOf(qty));
            }
            mFMP.totalInCash();
            mFMP.closeFiscalCheck();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public List<ItemListOrderItems> getListOrderItem(long i) {
        ContentResolver cr = getActivity().getContentResolver();
        String where = OrderItems.ORDERID + " = " + i;
        Cursor c = cr.query(OrderItems.BASEURI, null, where, null, null);
        List<ItemListOrderItems> list = new ArrayList<ItemListOrderItems>();
        try {
            if (c.moveToFirst()) {
                do {
                    long itemid = c.getLong(c.getColumnIndexOrThrow(OrderItems.ID));

                    String[] order = OrderItems.getOrderItems(getActivity(), itemid);
                    list.add(new ItemListOrderItems(order[0], order[1], order[2], order[3], order[4], itemid));
                } while (c.moveToNext());
            } else {
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }
}
