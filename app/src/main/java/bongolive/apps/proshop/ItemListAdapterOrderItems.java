/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.db.Stock;

public class ItemListAdapterOrderItems extends BaseAdapter {
private LayoutInflater mInflater ;
Context _activity ;
AppPreference appPreference;
private List<ItemListOrderItems> mItems = new ArrayList<ItemListOrderItems>() ;
public ItemListAdapterOrderItems(Context context, List<ItemListOrderItems> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
    _activity = context;
}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.orderitem, null);
			holder = new ItemViewHolder() ;
			holder.txtName = (TextView)convertView.findViewById(R.id.txtProdNo);
			holder.txtName1 = (TextView)convertView.findViewById(R.id.txtProQty);
			holder.txtName2 = (TextView)convertView.findViewById(R.id.txtSalePrice);
            holder.txtName3 = (TextView)convertView.findViewById(R.id.txtTaxRate);
            holder.txtName4 = (TextView)convertView.findViewById(R.id.txtSyncStatus);
            convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
        ItemListOrderItems cl = mItems.get(position);
        String prodname = Stock.getProdName(_activity, cl.getitemname());
		holder.txtName.setText(prodname);//prodname
		holder.txtName1.setText(_activity.getString(R.string.stritemssold)+" "+cl.getitemname1()); //prod qty
		holder.txtName2.setText(_activity.getString(R.string.strperitem)+" "+cl.getitemname2()); // sale price
        holder.txtName3.setText(_activity.getString(R.string.strtaxrate)+" "+cl.getitemname3()); // tax rate
        appPreference = new AppPreference(_activity.getApplicationContext());

        //prodno/prodqty/saleprice/taxrate/syncstatus
        String sst = cl.getitemname4(); // sync status
        int syncstat = 0;
        if(!sst.isEmpty())
            syncstat = Integer.parseInt(sst) ;
        if(syncstat == 1){
            holder.txtName4.setText(_activity.getString(R.string.stritemsynced));
        } else {
            holder.txtName4.setText(_activity.getString(R.string.stritemunsynced));

        }
        long id = cl.getId();


		return convertView ;
	} 
	static class ItemViewHolder {
		TextView txtName ;
		TextView txtName1 ; 
		TextView txtName2 ;
        TextView txtName3 ;
        TextView txtName4 ;
    }
  
}
