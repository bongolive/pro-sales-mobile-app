/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import bongolive.apps.proshop.networking.Constants;

/**
 * Created by nasznjoka on 1/2/2015.
 */
public class AppPreference {
	private SharedPreferences _sharedPrefs,_settingsPrefs;
	private SharedPreferences.Editor _prefsEditor;

    public AppPreference(Context context) {
        this._sharedPrefs = context.getSharedPreferences(Constants.CLIENT_DATA, Activity.MODE_MULTI_PROCESS);
        PreferenceManager.setDefaultValues(context, R.xml.pref_general, false);
        this._settingsPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this._prefsEditor = _sharedPrefs.edit();
	}

	public int getReceiptStatu() {
		return _sharedPrefs.getInt(Constants.PREFRECEIPT, 0);
	}
	public int getVatStatus() {
		return _sharedPrefs.getInt(Constants.PREFTAX, 0);
	}
    public int getDiscountStatus(){
        return  _sharedPrefs.getInt(Constants.PREFDISCOUNT, 0);
    }
    public String getDefaultLanguage()
    {
        return _settingsPrefs.getString(Constants.PREFLANG, "");
    }
    public String getDefaultSync()
    {
        return _settingsPrefs.getString(Constants.PREFSYNC, "");
    }
    public String getDefaultCurrency()
    {
        return _settingsPrefs.getString(Constants.PREFCURRENCY, "");
    }

    public int getPrintWithNoPayment()
    {
        return _sharedPrefs.getInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, 0);
    }

    public boolean check_firstrun()
    {
        return _sharedPrefs.getBoolean(Constants.FIRSTRUN, true);
    }
    public int get_accounttype()
    {
        return _sharedPrefs.getInt(Constants.DEVICE_ACCOUNTTYPE, 0);
    }

    public String getAddress()
    {
        return _sharedPrefs.getString("address", "");
    }


    public String getLastSync()
    {
        return _sharedPrefs.getString(Constants.LASTSYNC, "");
    }

    public String getAuthkey()
    {
        return _sharedPrefs.getString(Constants.AUTHTOKEN, "");
    }

	public void saveVatChoice(int id) {
		_prefsEditor.putInt(Constants.PREFTAX, id);
		_prefsEditor.commit();
	}
    public void saveDiscountStatus(int id) {
        _prefsEditor.putInt(Constants.PREFDISCOUNT, id);
        _prefsEditor.commit();
    }
    public void savePrintWithNoPaymentStatus(int id) {
        _prefsEditor.putInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, id);
        _prefsEditor.commit();
    }
    public void setDefaultCurrency(String id){
        _prefsEditor.putString(Constants.PREFCURRENCY, id);
        _prefsEditor.commit();
    }
    public void clearAddress(){
        _prefsEditor.remove(Constants.PREFADDRESS);
        _prefsEditor.commit();
    }
    public void saveAddress(String address){
        _prefsEditor.putString(Constants.PREFADDRESS, address);
        _prefsEditor.commit();
    }

    public void savePrintReceiptChoice(int id) {
        _prefsEditor.putInt(Constants.PREFRECEIPT, id);
        _prefsEditor.commit();
    }

    public void save_auth(String vals){
        _prefsEditor.putString(Constants.AUTHTOKEN, vals);
        _prefsEditor.commit();
    }

    public void save_accounttype(int vals){
        _prefsEditor.putInt(Constants.DEVICE_ACCOUNTTYPE, vals);
        _prefsEditor.commit();
    }

    public void clear_authentications(){
        _prefsEditor.remove(Constants.DEVICE_ACCOUNTTYPE);
        _prefsEditor.remove(Constants.AUTHTOKEN);
        _prefsEditor.commit();
    }

    public void set_firstrun(){
        _prefsEditor.putBoolean(Constants.FIRSTRUN, false);
        _prefsEditor.commit();
    }
    public void save_last_sync(String date){
        _prefsEditor.putString(Constants.LASTSYNC, date);
        _prefsEditor.commit();
    }


}
