/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.Locale;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.MultimediaContents;
import bongolive.apps.proshop.db.Stock;
import bongolive.apps.proshop.networking.Constants;


public class AuditingFrag extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    ListView lv;
    private ArrayList<StockItemList> marraylist = new ArrayList<StockItemList>();
    ArrayList<String> arraylist;
    ArrayAdapter<String> adapters, proadapter, paymodeadapter, paystatusadapter;
    StockItemListAdapter adapter;
    String[] businesslist, productlist, paymode, paystatus;
    TextView txtreport;
    Button addp,addc,adds;
    static int __stockquantity;
    AutoCompleteTextView prods, clients;
    EditText etqty;
    static String customeridLocal, customerid, _prodname;
    private static final int TAKE_PICTURE = 3;
    private static final int TAKE_CLIP = 4;
    private static final int TAKE_VOICE = 5;
    private static Bitmap bitmap_ = null;
    private static MediaStore.Video vid_ = null;
    private static Uri fileuri,fileurivideo,fileurisound;
    Uri selectedImage,selectedVideo,selectedAudio;
    static int lastitem = 0;
    private static String imagpath , videopath, soundrecpath;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.addstock, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
        setHasOptionsMenu(true);

        prods = (AutoCompleteTextView) getActivity().findViewById(R.id.autoprods);
        clients = (AutoCompleteTextView) getActivity().findViewById(R.id.autclnts);
        etqty = (EditText) getActivity().findViewById(R.id.etquantity);
        lv = (ListView) getActivity().findViewById(R.id.stocklist);
        addp = (Button)getActivity().findViewById(R.id.takephoto);
        addc = (Button)getActivity().findViewById(R.id.takevideo);
        adds = (Button)getActivity().findViewById(R.id.takeaudio);

        adapter = new StockItemListAdapter(getActivity(), marraylist);


        businesslist = Customers.getAllCustomers(getActivity());
        productlist = Stock.getAllStock(getActivity());
        adapters = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, businesslist);
        proadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, productlist);

        /*
        * set customers list*/
        clients.setAdapter(adapters);
        clients.setOnItemClickListener(this);
        clients.setThreshold(1);

        /*set products list*/
        prods.setAdapter(proadapter);
        prods.setOnItemClickListener(this);
        prods.setThreshold(1);

        addp.setOnClickListener(this);
        addc.setOnClickListener(this);
        adds.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.addstock, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.autclnts: {
                String name = (String) parent.getItemAtPosition(position);
                clients.setText(name);
                customerid = String.valueOf(Customers.getCustomerid(getActivity(), name));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(getActivity(), name));
                clients.clearFocus();
            }
            case R.id.autoprods: {
                String pro = (String) parent.getItemAtPosition(position);
                prods.setText(pro);
                _prodname = pro;
                __stockquantity = Stock.getAllStockQuantity(getActivity(), pro);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addstock:
                add_item();
            break;
            case R.id.action_save_stock:
            break;
        }
        return true;
    }

    private void show_image() {

        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.photo, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        VideoView videoView = (VideoView) dialog.findViewById(R.id.videoview);
        ImageView photo = (ImageView) dialog.findViewById(R.id.imageView);
        photo.setVisibility(View.VISIBLE);
        videoView.setVisibility(View.GONE);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        String img = MultimediaContents.getLastImage(dialog.getContext());

        Bitmap bm = BitmapFactory.decodeFile(img);
//        Bitmap bitmap = Constants.getImage(img);

        photo.setImageBitmap(bm);
        dialog.show();

    }

private void show_clip() {

        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.photo, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        VideoView videoView = (VideoView) dialog.findViewById(R.id.videoview);
        ImageView photo = (ImageView) dialog.findViewById(R.id.imageView);
        videoView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//        Bitmap bitmap = Constants.getImage(img);
        photo.setVisibility(View.GONE);

    Log.v("uri", " " + selectedVideo);
            videoView.setVideoURI(selectedVideo);
            videoView.requestFocus();
            videoView.start();

        dialog.show();

    }
private void play_sound() {

        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.photo, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        VideoView videoView = (VideoView) dialog.findViewById(R.id.videoview);
        ImageView photo = (ImageView) dialog.findViewById(R.id.imageView);
        videoView.setVisibility(View.GONE);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//        Bitmap bitmap = Constants.getImage(img);
        photo.setVisibility(View.GONE);

    Log.v("uri", " " + selectedAudio);

        dialog.show();

    }


    private void add_item() {

        String p = prods.getText().toString();
        String suserq = etqty.getText().toString();

        String[] va = new String[]{p, suserq};
        if (Validating.areSet(va) && !suserq.equals("0")) {


            long i = 0;
            if (!marraylist.isEmpty() && marraylist != null) {
                i = adapter.getCount() + 1;
            } else {
                i = 1;
            }

//            marraylist.add(new StockItemList(p, Integer.parseInt(suserq), getString(R.string.strlabelindex) + " " + i));
            //product/quantity/index
            adapter = new StockItemListAdapter(getActivity(), marraylist);
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);
            Toast.makeText(getActivity(), p.toUpperCase(Locale.getDefault()) + " " + getString(R.string.straddedtocart), Toast.LENGTH_LONG).show();
            prods.setText("");
            etqty.setText("");
        } else {
            Toast.makeText(getActivity(), getString(R.string.strzeroqty), Toast.LENGTH_LONG).show();
        }
    }

    private void submit_details(){

        if(marraylist.size() > 0){


            selectedAudio = null;
            selectedVideo = null;
            selectedImage = null;
            customerid = null;
            customeridLocal = null;
        }
    }

    public void click_pick(){
        if(getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //open camera
            Log.v("picture", "capturing picture");
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileuri);
            startActivityForResult(intent, TAKE_PICTURE);
        } else {
            Toast.makeText(getActivity(),"this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }
    public void click_clip(){
        if(getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //open camera
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileurivideo);
            startActivityForResult(intent, TAKE_CLIP);
        } else {
            Toast.makeText(getActivity(),"this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }
    public void click_sound(){
            Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileurisound);
            startActivityForResult(intent, TAKE_VOICE);
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data){
//        super.onActivityResult(reqCode,resCode,data);
        Log.v("reqcode","req code is "+reqCode);
        switch (reqCode){
            case TAKE_PICTURE:
                Log.v("reqcode","req code is "+reqCode);
                if(resCode == getActivity().RESULT_OK){
                    selectedImage = data.getData();
                    bitmap_ = (Bitmap)data.getExtras().get("data");

                    //cursor to get the image
                    String[] filepath = {MediaStore.Images.Media.DATA};
                    ContentResolver cr = getActivity().getContentResolver();
                    Cursor c = cr.query(selectedImage,null,null,null,null);
                    c.moveToFirst();
                    imagpath = c.getString(c.getColumnIndex(filepath[0]));
                    c.close();

                    String[] p = imagpath.split("/");
                    int n = p.length;
                    String[] vals = {imagpath,p[n-1],Constants.MULT_IMAGE};
                    Log.v("imagepath", imagpath + " imagename "+p[n-1]);
                    int store = MultimediaContents.storeMultimedia(getActivity(),vals);
                    if(store == 1){
                        Toast.makeText(getActivity(),"image successfully stored ",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case TAKE_CLIP:
                Log.v("reqcode","req code is "+reqCode);
                if(resCode == getActivity().RESULT_OK){
                    selectedVideo = data.getData();
                    Log.v("selectedvideo","video uri " +selectedVideo);
//                    bitmap_ = (Bitmap)data.getExtras().get("data");


                    String[] filepath = {MediaStore.Video.Media.DATA};
                    ContentResolver cr = getActivity().getContentResolver();
                    Cursor c = cr.query(selectedVideo,null,null,null,null);
                    c.moveToFirst();
                    videopath = c.getString(c.getColumnIndex(filepath[0]));
                    c.close();

                    String[] p = videopath.split("/");
                    int n = p.length;
                    String[] vals = {videopath,p[n-1],Constants.MULT_VIDEO};
                    Log.v("videopath", videopath + " videoname "+p[n-1]);
                    int store = MultimediaContents.storeMultimedia(getActivity(),vals);
                    if(store == 1){
                        Toast.makeText(getActivity(),"video clip successfully stored ",Toast.LENGTH_SHORT).show();
                    }
//                    show_clip(selectedVideo);
                }
                break;
            case TAKE_VOICE:
                Log.v("reqcode","req code is "+reqCode);
                if(resCode == getActivity().RESULT_OK){
                    selectedAudio = data.getData();
                    Log.v("selectedvideo","audio uri " +selectedAudio);

                    String[] filepath = {MediaStore.Video.Media.DATA};
                    ContentResolver cr = getActivity().getContentResolver();
                    Cursor c = cr.query(selectedAudio,null,null,null,null);
                    c.moveToFirst();
                    soundrecpath = c.getString(c.getColumnIndex(filepath[0]));
                    c.close();

                    String[] p = soundrecpath.split("/");
                    int n = p.length;
                    String audioname = p[n-1];

                    Log.v("videopath", " recordname "+audioname);
                    String[] vals = {soundrecpath,audioname,Constants.MULT_SOUND};
                    Log.v("videopath", soundrecpath + " recordname "+p[n-1]);
                    int store = MultimediaContents.storeMultimedia(getActivity(),vals);
                    if(store == 1){
                        Toast.makeText(getActivity(),"voice record successfully stored ",Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(),"voice record is not stored (don't play it after record)",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.takephoto:
                click_pick();
                break;
            case  R.id.takevideo:
                click_clip();
                break;
            case R.id.takeaudio:
                click_sound();
                break;
        }
    }
}
