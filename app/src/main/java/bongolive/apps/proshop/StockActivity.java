/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import bongolive.apps.proshop.db.Products;
import bongolive.apps.proshop.db.StockFull;
import bongolive.apps.proshop.networking.Constants;

public class StockActivity extends ActionBarActivity implements  OnItemClickListener,
        TextWatcher,OnCreateContextMenuListener
{

	Button btnSave,btnSavePrint;

    ListView  lvitems ;
    AutoCompleteTextView edclientname,etproduct ;
    EditText etquantity,etprice,etexpirydate ;

    TextView txtsku,txtprice  ;
     AppPreference appPreference;

    ArrayAdapter<String> proadapter ;
    StockItemListAdapter adapter;
	String[] businesslist,productlist,paymode,paystatus;

	static int __stockquantity ;
    int receipt = 0, payinfull = 0, quantitytype = 0;
	String orderstatus = null;
	static String _prodname ;
    boolean tabletSize = false;
    Button btnsaveorder,btnadditem;
	private ArrayList<StockItemList> marraylist = new ArrayList<StockItemList>();
   @Override
   public void onCreate(Bundle savedInstanceState)
   {
	   super.onCreate(savedInstanceState);

       tabletSize = getResources().getBoolean(R.bool.isTablet);
       if(!tabletSize){
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
       }
	   setContentView(R.layout.addstock_full);

       appPreference = new AppPreference(this.getApplicationContext());

       createSalesForm();

	   ActionBar ab = getSupportActionBar();
	   ab.setDisplayShowTitleEnabled(false);
	   ab.setIcon(R.drawable.ic_cancel_black_36dp_sale);
	   ab.setDisplayUseLogoEnabled(true); 
	   ab.setDisplayHomeAsUpEnabled(true);
       ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
   }

   private void createSalesForm() { 
       lvitems = (ListView)findViewById(R.id.stocklist);
         adapter = new StockItemListAdapter(this,  marraylist);
         lvitems.setAdapter(adapter) ; 
         lvitems.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
			
			@Override
			public void onCreateContextMenu(ContextMenu menu, View view,
					ContextMenuInfo menuInfo) {  
			    getMenuInflater().inflate(R.menu.removesale, menu);  
			    menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
	             menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp); 
	             AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
	             int position = info.position;
	             StockItemList list = marraylist.get(position);
	             Toast.makeText(StockActivity.this, "ok "+list.getPrice(), Toast.LENGTH_LONG).show();
	 	        	double purchaseprice = list.getPrice();

	 	    	 marraylist.remove(position);
	 	    	 adapter.notifyDataSetChanged();

			}
		}) ;
         orderstatus = " ("+ marraylist.size()+")";

		productlist = Products.getAllProducts(this);

		proadapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
                productlist);
		
		    etproduct = (AutoCompleteTextView)findViewById(R.id.etstockprodname);
		    etproduct.addTextChangedListener(this);
		    etproduct.setOnItemClickListener(this);
//            etproduct.setOnFocusChangeListener(this);
		    etproduct.setAdapter(proadapter); 
		    etproduct.setThreshold(1);
		    
		    etquantity = (EditText)findViewById(R.id.etstockqty);
		    etexpirydate = (EditText)findViewById(R.id.etstockexpirydate);

             etexpirydate.setOnClickListener(new OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Constants.setDateTimeField(StockActivity.this,
                             etexpirydate);
                 }
             });

             etexpirydate.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {
               String minvalue = Constants.getYearDateMonth();
               String maxvalue = "2030-06-30";
               String chked = etexpirydate.getText().toString();

               SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
               try {
                   Date entered = df.parse(chked);
                   Date mindate = df.parse(minvalue);
                   Date maxdate = df.parse(maxvalue);
                   Date common = df.parse("0000-00-00");
                   Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
                           maxdate + " entered value is " + entered);
                   if (entered.compareTo(mindate) <= 0) {
                       Toast.makeText(StockActivity.this, getString(R.string.strmin)
                                       + " " + minvalue,
                               Toast.LENGTH_LONG).show();
                       etexpirydate.setText("");
                       chked = "";
                       etexpirydate.clearFocus();
                   }
                   if (entered.compareTo(common) != 0) {
                       if (entered.compareTo(maxdate) > 0) {
                           Toast.makeText(StockActivity.this, getString(R.string.strmax)
                                           + " " + maxvalue,
                                   Toast.LENGTH_LONG).show();
                           etexpirydate.setText("");
                           chked = "";
                       }
                   }
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           }
       });

		    etquantity.setOnFocusChangeListener(new OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {

                        etprice.setEnabled(true);
                    }
                }
            }) ;
		    etprice = (EditText)findViewById(R.id.etpurchaseprice);
            String lang = appPreference.getDefaultCurrency();
            if(lang.equals("Tshs") || lang.isEmpty() || lang.equals("-1")){
//                etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dptz,0,0,0);
            } else{
//                etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dp,0,0,0);
            }


       if(tabletSize) {
           btnsaveorder = (Button) findViewById(R.id.btnsaveorder);
           btnadditem = (Button) findViewById(R.id.btnadditem);

           btnadditem.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
                   add_item();
               }
           });

           btnsaveorder.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
                   saveOrder();
               }
           });
       }
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
	 
		 if(view.getId() == R.id.autotxtsaleclientname){
			String name = (String) parent.getItemAtPosition(position); 
			edclientname.setText(name);

			edclientname.clearFocus();
			etproduct.requestFocus();
		 } 
		 else if(view.getId() == etproduct.getId()){
			String pro = (String)parent.getItemAtPosition(position);
			etproduct.setText(pro); 
			_prodname = pro ;
			edclientname.requestFocus();
		 }
	} 
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {  
	} 
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {
        String productname = etproduct.getText().toString();
        if (TextUtils.isEmpty(productname)) {
            etproduct.requestFocus();
            Toast.makeText(this, getResources().getString(R.string.strfilldata), Toast.LENGTH_LONG).show();
        }

    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_sales_buttons, menu);

        MenuItem additem,saveitem;
        additem = (MenuItem)menu.findItem(R.id.action_add_item);
        saveitem = (MenuItem)menu.findItem(R.id.action_save);

        if(tabletSize){
            additem.setVisible(false);
            saveitem.setVisible(false);
        }
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case android.R.id.home:
		    marraylist.clear();
		    adapter.notifyDataSetChanged(); 
		    Intent intent = new Intent();
             setResult(RESULT_CANCELED, intent) ;
             finish();
		case R.id.action_save:
			saveOrder();
			break;
		case R.id.action_add_item:
			add_item();
			break;
		}
		
		return false;
		
	}

	private void add_item() {
		    String p = etproduct.getText().toString();
			String suserq = etquantity.getText().toString();
			String price = etprice.getText().toString();
            String expiryd = etexpirydate.getText().toString();
			String[] va = new String[]{p,suserq,price,expiryd} ;
			if(Validating.areSet(va)){
                if(Integer.parseInt(suserq) > 0 && Double.parseDouble(price) > 0) {

                    double grandtotal = Double.parseDouble(va[2]);
                    int proid = Products.getLocalProdid(this, va[0]);
                    String exdate = va[3];

                    long i = 0;
                    if (!marraylist.isEmpty() && marraylist != null) {
                        i = adapter.getCount() + 1;
                    } else {
                        i = 1;
                    }

                    if (!va[1].isEmpty() && grandtotal > 0 ) {
                        marraylist.add(new StockItemList(p, Integer.parseInt(va[1]), getString(R
                                .string.strlabelindex) + " " + i, grandtotal, proid, exdate));
                    }

                    adapter = new StockItemListAdapter(this, marraylist);
                    adapter.notifyDataSetChanged();

                    lvitems.setAdapter(adapter);
                    Toast.makeText(this, p.toUpperCase(Locale.getDefault()) + " " + getString(R.string.straddedtocart), Toast.LENGTH_LONG).show();
                    etproduct.setText("");
                    etquantity.setText("");
                    etprice.setText("");
                    etexpirydate.setText("");
                } else {
                    Toast.makeText(this, getString(R.string.strzeroqty), Toast.LENGTH_LONG).show();
                }
			}  
	}

	private void saveOrder(){
		if(marraylist.size() != 0){
				StockItemList list = null ;

                        int insert = 0;
                          String batch = Constants.generateBatch();
                        for (int i = 0; i < marraylist.size(); i++) {
                            list = marraylist.get(i);

                            String productid = String.valueOf(list.getProdid());
                            String quantity = String.valueOf(list.getQuantity());
                            String sprice = String.valueOf(list.getPrice());
                            String productname = String.valueOf(list.getProductname());
                            String expirydate = list.getExpirydate();
                            String[] stockitem = new String[5];
                            //prodid,stockqt,batch,price,expirydate

                            stockitem = new String[]{productid, quantity,batch,sprice,expirydate};


                            insert = StockFull.insertStockLocal(this, stockitem);
                            if (insert == 1) {
//                                Stock.updateProductDeminish(this, productname, Integer.parseInt(quantity), true);
                                Products.updateProductQty(this, new int[]{list.getProdid(),list
                                        .getQuantity(), 0});
                            }
                        }
                        if (insert == 1) {
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();

                        }
			}
	}


}
