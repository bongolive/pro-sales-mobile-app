/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import bongolive.apps.proshop.networking.Constants;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences
.OnSharedPreferenceChangeListener {
        ListPreference listsync,listlang,listcurrency;
        CheckBoxPreference vatpref,printrecptpref,discountpref,printwithnopaymentpref;
        AppPreference appPreference;
    boolean tabletSize =false;
@Override
public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);

    tabletSize = getResources().getBoolean(R.bool.isTablet);
    if(!tabletSize){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
    addPreferencesFromResource(R.xml.pref_general);
    appPreference = new AppPreference(getApplicationContext());
    listlang = (ListPreference)findPreference(Constants.PREFLANG);
    listsync = (ListPreference)findPreference(Constants.PREFSYNC);
    listcurrency = (ListPreference)findPreference(Constants.PREFCURRENCY);
    vatpref = (CheckBoxPreference)findPreference(Constants.PREFTAX);
    printrecptpref = (CheckBoxPreference)findPreference(Constants.PREFRECEIPT);
    discountpref = (CheckBoxPreference)findPreference(Constants.PREFDISCOUNT);
    printwithnopaymentpref =(CheckBoxPreference)findPreference(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT);

    if(printwithnopaymentpref.isChecked())
        appPreference.savePrintWithNoPaymentStatus(1);
    if(vatpref.isChecked())
        appPreference.saveVatChoice(1);
    vatpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            if(vatpref.isChecked())
                appPreference.saveVatChoice(1);
            if(!vatpref.isChecked())
                appPreference.saveVatChoice(0);
            return false;
        }
    });
    printrecptpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            if(printrecptpref.isChecked())
                appPreference.savePrintReceiptChoice(1);
            if(!printrecptpref.isChecked())
                appPreference.savePrintReceiptChoice(0);
            return false;
        }
    });
    discountpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            if(discountpref.isChecked())
                appPreference.saveDiscountStatus(1);
            if(!discountpref.isChecked())
                appPreference.saveDiscountStatus(0);
            return false;
        }
    });
    printwithnopaymentpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            if(printwithnopaymentpref.isChecked())
                appPreference.savePrintWithNoPaymentStatus(1);
            if(!printwithnopaymentpref.isChecked())
                appPreference.savePrintWithNoPaymentStatus(0);
            return false;
        }
    });

     setListPreferenceData(listlang);
    setListPreferenceDataCurrency(listcurrency);
    setListPreferenceDataSync(listsync);
    listlang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
    @Override
    public boolean onPreferenceClick(Preference preference) {
        setListPreferenceData(listlang);
        return false;
        }
        });
    listcurrency.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            setListPreferenceDataCurrency(listcurrency);
            return false;
        }
    });
    listsync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            setListPreferenceDataSync(listsync);
            return false;
        }
    });
        }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(Constants.PREFLANG))
        {
            Preference langpref = findPreference(key);
            langpref.setSummary(sharedPreferences.getString(key,""));
        }
        if(key.equals(Constants.PREFSYNC))
        {
            Preference syncpref = findPreference(key);
            syncpref.setSummary(sharedPreferences.getString(key,""));
        }
        if(key.equals(Constants.PREFCURRENCY))
        {
            Preference currepref = findPreference(key);
            currepref.setSummary(sharedPreferences.getString(key,""));
        }
    }

protected static void setListPreferenceData(ListPreference lp) {

        lp.setEntries(R.array.pref_default_language_titles);
        lp.setEntryValues(R.array.pref_default_language_values);
        lp.setDefaultValue(0);
        }
    protected static void setListPreferenceDataCurrency(ListPreference lp) {

        lp.setEntries(R.array.pref_default_currency_titles);
        lp.setEntryValues(R.array.pref_default_currency_values);
        lp.setDefaultValue(1);
    }
    protected static void setListPreferenceDataSync(ListPreference lp) {

        lp.setEntries(R.array.pref_sync_frequency_titles);
        lp.setEntryValues(R.array.pref_sync_frequency_values);
        lp.setDefaultValue(2);
    }
  }