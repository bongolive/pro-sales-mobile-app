/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.DatabaseHandler;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.Products;

public class ProductsFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert;
    View _rootView;
    DatabaseHandler dh;
    ListView lv;
    Button sub;
    Dialog dialog;
    ItemListAdapter adapter;
    List<ItemList> list;
    ArrayList<String> deletearray;
    MenuItem deleteitem,addproductitem;
    public static int ADDSTOCK = 1;
    SearchManager searchManager;
    SearchView searchView;
    boolean tableSize = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_items, container, false);
        } else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

      
        lv = (ListView) getActivity().findViewById(R.id.list);

        adapter = new ItemListAdapter(getActivity(), getList(), 2);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        setHasOptionsMenu(true);
        deletearray = new ArrayList<>();
    }

    private void showProduct(final long i, String prodname, int action) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.addproduct, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final EditText etprodname, etreorder, etprice, ettax, etsku, etdesc, etqty, etupdate;
        TextView txttile, lastup, qtyinstock;

        etprodname = (EditText) dialog.findViewById(R.id.etprodname);
        etreorder = (EditText) dialog.findViewById(R.id.etreorder);
        etprice = (EditText) dialog.findViewById(R.id.etunitprice);
        ettax = (EditText) dialog.findViewById(R.id.ettax);
        etsku = (EditText) dialog.findViewById(R.id.etsku);
        etdesc = (EditText) dialog.findViewById(R.id.etdescr);
        txttile = (TextView) dialog.findViewById(R.id.txttitle);
        lastup = (TextView) dialog.findViewById(R.id.txtlastsell);
        qtyinstock = (TextView) dialog.findViewById(R.id.txtitemonhand);
        txttile.setText(prodname);
        etqty = (EditText) dialog.findViewById(R.id.etitemonhand);
        etupdate = (EditText) dialog.findViewById(R.id.etlastupdate);

        Button edit = (Button) dialog.findViewById(R.id.btnSubmit);
        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        String[] details = Products.getProducts(getActivity(), i);
        //name,price,update,itemonhand,taxrate,reorder,description,sku
        if (action == 0) {
            //view
            etqty.setVisibility(View.VISIBLE);
            etupdate.setVisibility(View.VISIBLE);
            lastup.setVisibility(View.VISIBLE);
            qtyinstock.setVisibility(View.VISIBLE  );
            etqty.setFocusable(false);
            etqty.setFocusableInTouchMode(false);
            etupdate.setFocusable(false);
            etupdate.setFocusableInTouchMode(false);
            etprodname.setFocusable(false);
            etprodname.setFocusableInTouchMode(false);
            etprice.setFocusable(false);
            etprice.setFocusableInTouchMode(false);
            ettax.setFocusable(false);
            ettax.setFocusableInTouchMode(false);
            etsku.setFocusable(false);
            etsku.setFocusableInTouchMode(false);
            etdesc.setFocusable(false);
            etdesc.setFocusableInTouchMode(false);
            etreorder.setFocusable(false);
            etreorder.setFocusableInTouchMode(false);

            edit.setVisibility(View.GONE);


            etprodname.setText(details[0]);
            etprice.setText(details[1]);
            ettax.setText(details[4]);
            etsku.setText(details[7]);
            etdesc.setText(details[6]);
            etupdate.setText(details[2]);
            etqty.setText(details[3]);
            etreorder.setText(details[5]);
        } else if (action == 1) {

            etprodname.setText(details[0]);
            etprice.setText(details[1]);
            ettax.setText(details[4]);
            etsku.setText(details[7]);
            etdesc.setText(details[6]);
//            etupdate.setText(dialog.getContext().getString(R.string.strsku)+" : "+details[2]);
//            etqty.setText(dialog.getContext().getString(R.string.strsku)+" : "+details[3]);
            etreorder.setText(details[5]);

        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setprodname, setreorder, setprice, settax, setsku, setdesc;
                setprodname = etprodname.getText().toString();
                setreorder = etreorder.getText().toString();
                setprice = etprice.getText().toString();
                settax = ettax.getText().toString();
                setsku = etsku.getText().toString();
                setdesc = etdesc.getText().toString();
                String[] check = {setprodname, setreorder, setprice, settax};
                if (setsku.isEmpty())
                    setsku = "0000";
                if (setdesc.isEmpty())
                    setdesc = "No description";
                String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc, String
                        .valueOf(i)};
                if (Validating.areSet(check)) {
                    int prodexist = Products.isProductPresent(dialog.getContext(), setprodname);
                    int prodexistid = Products.isProductPresentUpdating(dialog.getContext(), setprodname);

                    if (prodexist == 0 || (prodexist == 1 && prodexistid == i)) {
                        int insert = Products.updateProduct(dialog.getContext(), vals);
                        if (insert == 1) {
                            adapter = new ItemListAdapter(getActivity(), getList(), 2);
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            Toast.makeText(dialog.getContext(), getString(R.string
                                    .strproductadd), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(dialog.getContext(), getString(R.string
                                .strprodexists), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        dialog.show();
    }


    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Products.SYNCSTATUS + " != 3";
        Cursor c = cr.query(Products.BASEURI, null, where, null, Products.ID + " DESC");
        list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Products.PRODUCTNAME);
                    int colP = c.getColumnIndex(Products.ITEMS_ON_HAND);
                    int colI = c.getColumnIndex(Products.ID);
                    int colD = c.getColumnIndex(Products.TAXRATE);
                    int colPr = c.getColumnIndex(Products.UNITPRICE);
                    int colUp = c.getColumnIndex(Products.UPDATE);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    long i = c.getLong(colI);
                    String d = c.getString(colD);
                    String pr = c.getString(colPr);
                    String upd = c.getString(colUp);
                    if (!Validating.areSet(upd))
                        upd = getString(R.string.strnever);
                    list.add(new ItemList(getString(R.string.strproduct).toUpperCase(Locale.getDefault()) + ": " +
                            n.toUpperCase(Locale.getDefault()),
                            getString(R.string.strprice).toUpperCase(Locale.getDefault()) + ": " +
                                    pr.toUpperCase(Locale.getDefault()),
                            getString(R.string.strtotalqty).toUpperCase(Locale.getDefault()) + ": " + p, i,
                            getString(R.string.strsku).toUpperCase(Locale.getDefault()) + ": " + d,
                            getString(R.string.strlastsell).toUpperCase(Locale.getDefault()) + ": " + upd));
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, "", p, i, d));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    public ArrayList<ItemList> getListOrderItem(long i) {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Products.ID + " = " + i;
        Cursor c = cr.query(Order.BASEURI, null, where, null, null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if (c.moveToFirst()) {
            do {
                int colN = c.getColumnIndex(Order.TOTALSALES);
                int colP = c.getColumnIndex(Order.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Order.ID);
                int colD = c.getColumnIndex(Order.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI);
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault()) + ": " +
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault()) + ": " + p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault()) + ": " + d));
            } while (c.moveToNext());
        }
        ArrayList<ItemList> itemlist = new ArrayList<>();
        return itemlist;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.add_product, menu);
        deleteitem = menu.findItem(R.id.action_delete);
        addproductitem = menu.findItem(R.id.action_addproduct);

        searchManager =  (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_addproduct:
                LayoutInflater infl = getActivity().getLayoutInflater();
                View view = infl.inflate(R.layout.addproduct, null);
                dialog = new Dialog(getActivity(), R.style.CustomDialog);
                dialog.setCancelable(false);
                dialog.setContentView(view);
                dialog.show();//prodname,reorder,price,tax,desc,sku
                final EditText etprodname, etreorder, etprice, ettax, etsku, etdesc;
                final TextView txttile;
                etprodname = (EditText) dialog.findViewById(R.id.etprodname);
                etreorder = (EditText) dialog.findViewById(R.id.etreorder);
                etprice = (EditText) dialog.findViewById(R.id.etunitprice);
                ettax = (EditText) dialog.findViewById(R.id.ettax);
                etsku = (EditText) dialog.findViewById(R.id.etsku);
                etdesc = (EditText) dialog.findViewById(R.id.etdescr);
                txttile = (TextView) dialog.findViewById(R.id.txttitle);
                txttile.setText(getString(R.string.straddproduct));
                txttile.setAllCaps(true);
                ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
                goback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                sub = (Button) dialog.findViewById(R.id.btnSubmit);
                sub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String setprodname, setreorder, setprice, settax, setsku, setdesc;
                        setprodname = etprodname.getText().toString();
                        setreorder = etreorder.getText().toString();
                        setprice = etprice.getText().toString();
                        settax = ettax.getText().toString();
                        setsku = etsku.getText().toString();
                        setdesc = etdesc.getText().toString();
                        String[] check = {setprodname, setreorder, setprice, settax};
                        if (setsku.isEmpty())
                            setsku = "0000";
                        if (setdesc.isEmpty())
                            setdesc = "No description";
                        String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc};
                        if (Validating.areSet(check)) {
                            int prodexist = Products.isProductPresent(dialog.getContext(), setprodname);
                            if (prodexist == 0) {
                                int insert = Products.insertProduct(dialog.getContext(), vals);
                                if (insert == 1) {
                                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                                    adapter.notifyDataSetChanged();
                                    lv.setAdapter(adapter);
                                    Toast.makeText(dialog.getContext(), getString(R.string
                                            .strproductadd), Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            } else {
                                Toast.makeText(dialog.getContext(), getString(R.string
                                        .strprodexists), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                return true;

            case R.id.action_delete:
                if (deletearray.size() > 0) {
                    for (int i = 0; i < deletearray.size(); i++) {
                        Products.deleteProduct(getActivity(), deletearray.get(i));
                    }

                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                }
                break;
            case R.id.action_addstock_full:
                int prod = Products.getCount(getActivity());
                AlertDialogManager alertDialogManager = new AlertDialogManager();
                if (prod > 0) {
                    Intent intent = new Intent(getActivity(), StockActivity.class);
                    startActivityForResult(intent, ADDSTOCK);
                    return true;
                } else {
                    alertDialogManager.showAlertDialog(getActivity(), getString(R.string.strerror),
                            getString(R.string.strnoprod), false);
                }
                break;
            case R.id.action_search:
                getActivity().onSearchRequested();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
        LinearLayout ll = ((LinearLayout) view.findViewById(R.id.itemlayout));
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemList _list = list.get(position);
                String name = _list.getitemname();
                long i = _list.getId();
                if (i > 0)
                    showProduct(i, name, 0);
            }
        });
        ImageView imgedit = ((ImageView) view.findViewById(R.id.imgedit));
        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemList _list = list.get(position);
                String name = _list.getitemname();
                long i = _list.getId();

                Uri uri = Uri.withAppendedPath(Customers.BASEURI, String.valueOf(id));
                if (i > 0)
                    showProduct(i, name, 1);
            }
        });
        final CheckBox chkdrop = ((CheckBox) view.findViewById(R.id.chkdrop));

        ItemList _list = list.get(position);
        final long i = _list.getId();

        chkdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i > 0)
                    if (chkdrop.isChecked()) {
                        deleteitem.setVisible(true);
                        addproductitem.setVisible(false);
                        deletearray.add(String.valueOf(i));
                    } else {
                        deletearray.remove(String.valueOf(i));
                    }
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        getActivity();
        switch (reqCode) {
            case 1:
                if (resCode == Activity.RESULT_OK) {
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                    Toast.makeText(getActivity(), getString(R.string.strstockadded), Toast
                            .LENGTH_LONG)
                            .show();
                } else if (resCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(getActivity(), getString(R.string.strstockcancl), Toast
                            .LENGTH_LONG).show();
                }
                break;
        }
    }

   /* @Override
    public void startActivity(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            intent.putExtra(Constants.SEARCHSOURCE, "data");
        }

        super.startActivity(intent);
    }*/
}
