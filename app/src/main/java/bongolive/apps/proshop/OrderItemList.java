/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.net.Uri;

import java.io.Serializable;

/**
 * @author nasznjoka
 * 
 * Oct 9, 2014
 *
 */
public class OrderItemList implements Serializable{
	String productname ;
	int quantity ;  
	double price ; 
	String id ;
	double unitprice;
	double grandtotal;
	double tax;
	int prodid;
    double unittax ;
	Uri image,clip,audio;

	public Uri getImage() {
		return image;
	}

	public Uri getClip() {
		return clip;
	}

	public Uri getAudio() {
		return audio;
	}

	public double getUnitprice() {
		return unitprice;
	}


	public double getGrandtotal() {
		return grandtotal;
	}


	public double getTax() {
		return tax;
	}


	public int getProdid() {
		return prodid;
	}


	public String getProductname() {
		return productname;
	}


	public int getQuantity() {
		return quantity;
	}


	public double getPrice() {
		return price;
	}

    public double getUnittax(){
        return unittax;
    }

	public String getId(){
		return id ;
	}

	public OrderItemList(String p, int q, String i){
		productname = p;
		quantity = q;
		id = i;
	}
	public OrderItemList(String p, int q, String i, Uri img){
		productname = p;
		quantity = q;
		id = i;
		image = img;
	}
	public OrderItemList(String p, int q, double pr, String i,double up,int prid,double gt, double tx, double taxes){
		productname = p ;
		quantity = q;
		price = pr ; //grand total pr*qtyand tax
		id = i;
		unitprice = up;//unit price
		prodid = prid; // prodid
		grandtotal = gt; //grandtotalnotax
		tax = tx ; //taxrate
        unittax = taxes;
	} 

}
