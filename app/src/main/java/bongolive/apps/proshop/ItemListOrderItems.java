/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

/**
 * @author nasznjoka
 * 
 * Oct 9, 2014
 *
 */
public class ItemListOrderItems {
	String itemname ;
	String itemname1 ;
	String itemname2 ;
    String itemname3;
    String itemname4;

	long id ;

	public String getitemname(){
		return itemname ;
	}
	public String getitemname1(){
		return itemname1 ;
	}
    public String getitemname3(){
        return itemname3 ;
    }
    public String getitemname4(){
        return itemname4 ;
    }

	public long getId(){
		return id ;
	}

	public String getitemname2() {
		return itemname2;
	}
	public ItemListOrderItems(String it1,String it2,String it3,String it4,String it5,long idd){
		itemname = it1;
		itemname1  = it2 ;
		itemname2 = it3 ;
        itemname3 =it4;
        itemname4 = it5;
		id = idd ;
	} 

}
