/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.Validating;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.GpsTracker;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class Customers {
    public static final String TABLENAME = "customers";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderEngine.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String CUSTOMERNAME = "customer_name";
    public static final String BUZINESSNAME = "business_name";
    public static final String REFERENCE = "system_customer_id";
    public static final String AREA = "area";
    public static final String CITY = "city";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
    public static final String SYNCSTATUS = "is_synced";//0 new //1 synced //2 update //3 delete
    public static final String STREET = "street";
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prostock.customers";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prostock.customers";

    public static final String GETARRAYNAME = "customerList";
    public static final String SENDARRAYNAME = "customer";
    public static final String SENDARRAYNAME2 = "customerSentList";
    private static final String TAG = "CUSTOMERS";

    int id, reference, sync;
    String cname, bname, area, city, mob, email, lat, lng, street;

    public Customers(int id, int ref, int sy, String cn, String bn
            , String ar, String ci, String mo, String em, String lt, String lng, String srt) {
        this.id = id;
        this.reference = ref;
        this.sync = sy;
        this.cname = cn;
        this.bname = bn;
        this.area = ar;
        this.city = ci;
        this.mob = mo;
        this.email = em;
        this.lat = lt;
        this.lng = lng;
        this.street = srt;
    }

    public Customers() {

    }

    // for checking if the customer(sy_cust_id) is present in the database locally
    private static int isCustomerPresent(Context context, int custid) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, null, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                    if (i == custid) {
                        return 1;
                    }
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String getCustomerBusinessName(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getCustomerBusinessNameLoca(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    //help to know if the customer is updating or creating
    public static int isCustomerNew(Context context, String buziness, String mobile) {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " LIKE \'%" + buziness + "%\'" + " AND " + MOBILE + " = " + mobile;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.getCount() > 0) {
                return 1;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int insertCustomer(Context context, String[] string) {
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(REFERENCE, string[2]);
        values.put(AREA, string[3]);
        values.put(CITY, string[4]);
        values.put(MOBILE, string[5]);
        values.put(EMAIL, string[6]);
        values.put(LAT, string[7]);
        values.put(LONGTUDE, string[8]);
        values.put(SYNCSTATUS, 0);
        values.put(STREET, string[10]);
        cr.insert(BASEURI, values);
        if (getCustomerCount(context) == customercount + 1) {
            return 1;
        }
        return customercount;
    }

    public static int updateCustomerDetails(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
//		String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\'"  ;
        String where = ID + " = " + string[5];

        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(AREA, "");
        values.put(CITY, "");
        values.put(MOBILE, string[2]);
        values.put(EMAIL, string[3]);
        values.put(LAT, 0);
        values.put(LONGTUDE, 0);
        values.put(SYNCSTATUS, 2);
        values.put(STREET, string[4]);

        int customercount = cr.update(BASEURI, values, where, null);

        return customercount;
    }

    public static int deleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, "3");
        String where = ID + " = " + string;
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static int confirmdeleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + string;
        if (cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }

    public static int updateCustomer(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(REFERENCE, string[0]);
        String where = ID + " = " + string[1];
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static int insertCustomerLocal(Context context, String[] string) {
        GpsTracker gpsTracker = new GpsTracker(context.getApplicationContext());
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(REFERENCE, 0);
        values.put(AREA, "");
        values.put(CITY, "");
        values.put(MOBILE, string[2]);
        values.put(EMAIL, string[3]);
        values.put(LAT, 0);
        values.put(LONGTUDE, 0);
        values.put(SYNCSTATUS, 0);
        values.put(STREET, string[4]);
        values.put(LAT, gpsTracker.getLatitude());
        values.put(LONGTUDE, gpsTracker.getLongitude());
        cr.insert(BASEURI, values);

        if (getCustomerCount(context) == customercount + 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static int getCustomerCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static JSONObject getCustomerJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETCUSTOMER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        String job = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject prepareCustomerToSend(Context context) throws JSONException {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = 0";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSENDCUSTOMER);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array;
            if (c.moveToFirst()) {
                JSONArray jarr = new JSONArray();
                do {
                    JSONObject j = new JSONObject();
                    j.put(CUSTOMERNAME, c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME)));
                    j.put(BUZINESSNAME, c.getString(c.getColumnIndexOrThrow(BUZINESSNAME)));
                    j.put(AREA, c.getString(c.getColumnIndexOrThrow(AREA)));
                    j.put(CITY, c.getString(c.getColumnIndexOrThrow(CITY)));
                    j.put(MOBILE, c.getString(c.getColumnIndexOrThrow(MOBILE)));
                    j.put(EMAIL, c.getString(c.getColumnIndexOrThrow(EMAIL)));
                    j.put(STREET, c.getString(c.getColumnIndexOrThrow(STREET)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    jarr.put(j);
                } while (c.moveToNext());
                jsonObject.put("incommingcustomers", (Object) jarr);
                array = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jsonObject);
                JSONObject job = new JSONObject(array);
                return job;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    /*
     * Getting A new Customer or Updating an existing one
     */
    public static void getCustomersFromServer(Context context) throws JSONException {
        JSONObject json = getCustomerJson(context);
        try {
            if (json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {

                    if (json.has(GETARRAYNAME)) {
                        JSONArray custarray = json.getJSONArray(GETARRAYNAME);
                        for (int i = 0; i < custarray.length(); i++) {
                            JSONObject js = custarray.getJSONObject(i);
                            String pty = js.getString(CUSTOMERNAME);
                            String put = js.getString(BUZINESSNAME);
                            String pdn = js.getString(REFERENCE);
                            String ref = js.getString(AREA);
                            String stc = js.getString(CITY);
                            String txr = js.getString(MOBILE);
                            String upc = js.getString(EMAIL);
                            String lud = js.getString(LAT);
                            String lng = js.getString(LONGTUDE);
                            String syn = "0";
                            String str = js.getString(STREET);
                            if (stc.isEmpty())
                                stc = "Dar es salaam";
                            if (upc.isEmpty())
                                upc = "example@company.com";
                            if (ref.isEmpty())
                                ref = "Posta";
                            String[] stockvalue = new String[]{pty, put, pdn, ref, stc, txr, upc, lud, lng, syn, str};
                            int custidpresent = isCustomerPresent(context, Integer.parseInt(pdn));
                            if (Validating.areSet(stockvalue)) {// is the customer system id present locally?
                                switch (custidpresent) {
                                    case 0:
                                        // it not present update or create new
                                        int createupdate = isCustomerNew(context, put, txr);
                                        switch (createupdate) {
                                            case 0:
                                                // this is a new customer create
                                                if (insertCustomer(context, stockvalue) == 1) {
                                                    if (updateSync(context, Integer.parseInt(pdn)) == 1)
                                                        sendAck(context, new String[]{pdn});
                                                }
                                                break;
                                            case 1:
                                                // the customer is present just update
                                                if (updateCustomer(context, stockvalue) == 1) {
                                                    if (updateSync(context, Integer.parseInt(pdn)) == 1)
                                                        sendAck(context, new String[]{pdn});
                                                }
                                                break;
                                        }
                                        break;
                                    case 1:
                                        //the customer system id is present ignore the request
                                        sendAck(context, new String[]{pdn});
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCustomersToServer(Context context) throws JSONException {
        JSONObject json = prepareCustomerToSend(context);
        try {
            if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {
                    if (json.has(SENDARRAYNAME)) {
                        JSONObject jsonObject = json.getJSONObject(SENDARRAYNAME);
                        if (jsonObject.has(SENDARRAYNAME2)) {
                            JSONArray jsonArray = jsonObject.getJSONArray(SENDARRAYNAME2);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject js = jsonArray.getJSONObject(i);
                                String pdn = js.getString(REFERENCE);
                                String customerid = js.getString(ID);
                                String[] stockvalue = new String[]{pdn, customerid};
                                int custidpresent = isCustomerPresent(context, Integer.parseInt(pdn));
                                if (Validating.areSet(stockvalue)) {// is the customer system id present locally?
                                    switch (custidpresent) {
                                        case 0:
                                            if (updateCustomer(context, stockvalue) == 1) {
                                                if (updateSync(context, Integer.parseInt(pdn)) == 1)
                                                    sendAck(context, new String[]{pdn});
                                                Order.updateSyncFromCustomer(context, stockvalue);
                                            }
                                        case 1:
                                            //the customer system id is present ignore the request
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static int updateSync(Context context, int customerid) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, 1);
        String where = REFERENCE + " = " + customerid;
        if (cr.update(BASEURI, values, where, null) == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void sendAck(Context context, String[] requiredid) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKKEY, REFERENCE);
        jobs.put(REFERENCE, requiredid[0]);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
    }

    public static String[] getAllCustomers(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null);
        try {
            if (cursor.getCount() > 0) {
                String[] str = new String[cursor.getCount()];
                int i = 0;

                while (cursor.moveToNext()) {
                    str[i] = cursor.getString(cursor.getColumnIndex(BUZINESSNAME));
                    i++;
                }
                cursor.close();
                return str;
            } else {
                cursor.close();
                return new String[]{};
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int getCustomerid(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\'";

        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int getCustomerlocalId(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\'";

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static ArrayList<String> getSearchedCustomer(Context context, String businessnm) {
        ArrayList<String> custlist= new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\' OR "+ CUSTOMERNAME + " LIKE \'%" + businessnm + "%'" ;

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getString(c.getColumnIndexOrThrow(ID)));

                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static int isCustomerSynced(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm;

        Cursor c = cr.query(BASEURI, new String[]{SYNCSTATUS}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(SYNCSTATUS));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String[] getCustomer(Context context, long id) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + id;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String[] custdetails;
        try {
            if (c.moveToFirst()) {
                String n = c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME));
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                String e = c.getString(c.getColumnIndexOrThrow(EMAIL));
                String p = c.getString(c.getColumnIndexOrThrow(MOBILE));
                String a = c.getString(c.getColumnIndexOrThrow(STREET));
                String ar = c.getString(c.getColumnIndexOrThrow(AREA));
                String ct = c.getString(c.getColumnIndexOrThrow(CITY));
                String g = a + " " + ar + " " + ct;
                custdetails = new String[]{n, b, e, p, g};
                return custdetails;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static String[] getCustomers(Context context, long reference) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + reference;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String[] custdetails;
        try {
            if (c.moveToFirst()) {
                String n = c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME));
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                String e = c.getString(c.getColumnIndexOrThrow(EMAIL));
                String p = c.getString(c.getColumnIndexOrThrow(MOBILE));
                String a = c.getString(c.getColumnIndexOrThrow(STREET));
                String ar = c.getString(c.getColumnIndexOrThrow(AREA));
                String ct = c.getString(c.getColumnIndexOrThrow(CITY));
                String g = a + " " + ar + " " + ct;
                custdetails = new String[]{n, b, e, p, g};
                return custdetails;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static String getCustomerName(Context context, long reference) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + reference;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String custdetails = "";
        try {
            if (c.moveToFirst()) {
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                custdetails = b;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custdetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
