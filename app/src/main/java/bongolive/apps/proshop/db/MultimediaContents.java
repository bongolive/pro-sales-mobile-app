/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

/**
 * Created by nasznjoka on 3/26/15.
 */
public class MultimediaContents {
    public static final String TABLENAME = "multimedia";
    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String SIZE = "size";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String DATE = "date";
    public static final String ISSYNCED = "issynced";

    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.bongolive.apps.multimedia" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.bongolive.apps.multimedia" ;
    private static final String TAG = MultimediaContents.class.getName();
    private static final String ARRAYNAME2 = "datapointLists";
    static JsonObjectJsonArray js ;
    public static String ARRAYNAME = "datapointList" ;

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    public static int storeMultimedia(Context context,String[] vals_){
        int loc = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(CONTENT,vals_[0]);
        cv.put(NAME, vals_[1]);
        cv.put(TYPE, vals_[2]);
        cv.put(SIZE, 0);
        cv.put(DATE, Constants.getDate());
       cr.insert(BASEURI, cv);
        if(loc + 1 == getCount(context)){
            Log.v("DATASTORAGE", "DATA IS STORED");
            purgeData(context);
            return 1;
        }
      return 0;
    }

    public static JSONObject sendMultimedia(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = ISSYNCED + " = 0" ;/*
        ContentValues cv = new ContentValues(1);
        cv.put(ISSYNCED,"0");
        cr.update(BASEURI,cv,where,null);*/
        Cursor c = cr.query(BASEURI, null, where, null, DATE +" ASC LIMIT 1");
        js = new JsonObjectJsonArray();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGMULTIMEDIA);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(NAME, c.getString(c.getColumnIndexOrThrow(NAME)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    String ty = c.getString(c.getColumnIndex(TYPE));
                    j.put(TYPE, ty);
                    if(ty.equals(Constants.MULT_IMAGE)){
                        j.put(CONTENT, Constants.getbase64photo(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                    } else if(ty.equals(Constants.MULT_VIDEO) || ty.equals(Constants.MULT_SOUND)){
                        j.put(CONTENT, Constants.getBase64Vid(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                    }
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put("multimedia_array", (Object) jarr);
//                Log.v("json",jsonObject.toString());
                array = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static void processInformation(Context context) throws JSONException{
        JSONObject json = sendMultimedia(context);
        if(json != null)
        try {
            if(json.has(Constants.SUCCESS)){
                String res = json.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(json.has(ARRAYNAME)){
                        JSONObject jsonObject = json.getJSONObject(ARRAYNAME);
                        if(jsonObject.has(ARRAYNAME2)){
                            JSONArray jsonArray = jsonObject.getJSONArray(ARRAYNAME2);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                int locid = jsonObject1.getInt(ID);
                                boolean OK = updateSync(context, locid, 1);
                                if (OK == true) {
                                    Log.v("updated", "the media is synced " + locid);
                                }
                            }
                        }
                    }

                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static boolean updateSync(Context context, int id, int flag)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ISSYNCED, flag);
        String where =  ID+ " = "+ id ;
        if(cr.update(BASEURI, values, where, null) == 1)
        {
            return true ;
        }   else {
            return false ;
        }
    }

    public static int purgeData(Context context){
        ContentResolver cr = context.getContentResolver();
        int data = getCount(context);
        if(data > 50){
            Cursor c = cr.query(BASEURI, new String[]{ID},null,null,null);
            if(c.moveToLast()){
                    String locid = c.getString(c.getColumnIndexOrThrow(ID));
                    deleteContent(context,locid);
            }
        }
        return 0;
    }

    public static int deleteContent(Context context, String data){
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+ data +
                " AND " + ISSYNCED + " = 1";
        if(cr.delete(BASEURI, where, null) > 0){
            Log.v("DELETING DATA", "LOC  ID "+ data);
            return 1;
        }
        return 0;
    }

    public static String getLastImage(Context context)
    {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = "+ DatabaseUtils.sqlEscapeString(Constants.MULT_IMAGE);
        Cursor c = cr.query(BASEURI, null, where,null,ID + " DESC LIMIT 1") ;
        try {
            if(c.moveToFirst())
            {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return bz ;
    }
    public static String getLastClip(Context context)
    {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = "+ DatabaseUtils.sqlEscapeString(Constants.MULT_VIDEO);
        Cursor c = cr.query(BASEURI, null, where,null,ID + " DESC LIMIT 1") ;
        try {
            if(c.moveToFirst())
            {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return bz ;
    }

}
