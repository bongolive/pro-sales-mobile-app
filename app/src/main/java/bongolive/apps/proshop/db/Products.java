/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.Validating;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class Products
{
//product_id, product_name, description, items_on_hand,  category, product_status,
// re_order_value, container_type, unit_size, unit_measure, package_type, package_units,
// unit_sale_price, sku_product_no, tax_rate, created_on, created_by, account_id,  updated_on,
// updated_by
	public static final String TABLENAME = "products";
	public static final String ID = "_id";
	public static final String PRODUCTNAME = "product_name";
	public static final String PACKAGETYPE = "package_type";
	public static final String PACKAGEUNITS = "package_units";
	public static final String CONTINER = "container_type";
	public static final String ITEMS_ON_HAND = "items_on_hand" ;
	public static final String REODER = "re_order_value" ;
	public static final String PRODUCT_TYPE = "product_type" ; //this is for those products who
	// don't run out of stock 0 for normal 1 for the one that never end
	public static final String UNITPRICE = "unit_sale_price" ;
	public static final String CATEGORY = "category";
	public static final String PRODSTATUS = "product_status";
	public static final String TAXRATE = "tax_rate";
	public static final String UPDATE = "updated_on" ;
	public static final String REFERENCE = "prod_system_id" ;//"system_prod_id" ;
	public static final String DESCRIPTION = "description" ;//"system_prod_id" ;
    public static final String SKU = "sku_product_no" ;//"Sys_stock_id" ;
    public static final String UNITSIZE = "unit_size" ;//"Sys_stock_id" ;
    public static final String CREATED = "created_on" ;//"Sys_stock_id" ;
    public static final String SYNCSTATUS = "is_synced" ;
    public static final String UNITMEASURE = "unit_measure" ;//"Sys_stock_id" ;
	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.bongolive" +
            ".apps.products" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.bongolive" +
            ".apps.products" ;
	private static final String TAG = Products.class.getName();
	static JsonObjectJsonArray js ;
	public static String ARRAYNAME = "productsList" ;

	private static JSONArray productsarray ;

	public Products() {
		
	}
	
	 
	 public static int isProductPresent(Context context, String productname)
	    {

            String where = PRODUCTNAME  + " LIKE \'%" + productname + "%\'"  ;
            Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null);
	    	 try {
	    	if(c.moveToFirst())
	    	{
                return 1;//product exists
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }

	 public static int isProductPresentUpdating(Context context, String productname)
	    {

            String where = PRODUCTNAME  + " LIKE \'%" + productname + "%\'"  ;
            Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null);
	    	 try {
	    	if(c.moveToFirst())
	    	{
                return c.getInt(c.getColumnIndex(ID));//product exists
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }
	public static int getItemOnHand(Context context, String stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{ITEMS_ON_HAND}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}
	public static int checkItemOnHand(Context context) {
		ContentResolver cr = context.getContentResolver();

        int qty= 0 ;
        String where = SYNCSTATUS + " != 3";
    	Cursor c = cr.query(BASEURI, new String[]{ITEMS_ON_HAND}, where, null, null) ;
    	try {

    	if(c.moveToFirst())
    	{
    		do {
                int q;
    			q = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
    			 if(q > 0)
                     qty += 1;
    		}while(c.moveToNext());
    	}
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return qty ;
	}
	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static int updateProduct(Context context, String[] string)
	{
		ContentResolver cr = context.getContentResolver(); 
		ContentValues values = new ContentValues() ;

        values.put(PRODUCTNAME, string[0].toUpperCase());//prodname,reorder,price,tax,desc,sku
        values.put(PACKAGETYPE, context.getString(R.string.strno));
        values.put(PACKAGEUNITS, 0) ;
        values.put(CONTINER, context.getString(R.string.strno));
        values.put(REODER, string[1]);
        values.put(UNITPRICE, Double.parseDouble(string[2])) ;
        values.put(CATEGORY, context.getString(R.string.strno));
        values.put(TAXRATE, string[3]);
        values.put(DESCRIPTION, string[5]);
        values.put(SKU, string[4]);
        values.put(UNITSIZE, 0);
        values.put(SYNCSTATUS, 2);
        values.put(UNITMEASURE, context.getString(R.string.strno));
        values.put(UPDATE, Constants.getDate());

		String where = ID + " = "+ string[6] ;
		if(cr.update(BASEURI, values, where, null) > 0) {
			return 1 ; 
		} 
		return 0;
	}
	/*
	 * Save a new product in the stock table
	 */
	public static int insertProduct(Context context, String[] string)
	{
		int productcount = getCount(context) ;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(PRODUCTNAME, string[0].toUpperCase());//prodname,reorder,price,tax,desc,sku
		values.put(PACKAGETYPE, context.getString(R.string.strno));
		values.put(PACKAGEUNITS, 0) ;
		values.put(CONTINER, context.getString(R.string.strno));
		values.put(REODER, Integer.parseInt(string[1]));
		values.put(UNITPRICE, Double.parseDouble(string[2])) ;
		values.put(CATEGORY, context.getString(R.string.strno));
		values.put(TAXRATE, string[3]);
        values.put(DESCRIPTION, string[5]);
        values.put(SKU, string[4]);
        values.put(UNITSIZE, 0);
        values.put(CREATED, Constants.getDate());
        values.put(UNITMEASURE, context.getString(R.string.strno));
		cr.insert(BASEURI, values);  
		if(getCount(context) == productcount+1){
			return 1 ;
		} else {
			return 0 ;
		}  
	}
	/*
	 * How many items are in the table ?
	 */
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
        String where = SYNCSTATUS + " != 3";
		Cursor _c = cr.query(BASEURI, null, where, null, null);
        int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}


    public static int deleteProduct(Context context, String string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYNCSTATUS, "3") ;
        values.put(UPDATE, Constants.getDate());
        String where =  ID + " = " + string ;
        if( cr.update(BASEURI, values, where, null) > 0) {
            Log.v("updated","updated");
            return 1 ;
        }
        return 0;
    }


    public static int updateProductQty(Context context, int[] string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        int itemsonhand = getProductQuantity(context, string[0]);
        Log.v("itemsonhand"," are "+itemsonhand);

        int newitemonhand = 0;

        if(string[2] == 0)// add stock
        newitemonhand = new BigDecimal(itemsonhand).add(new BigDecimal(string[1])).intValue();

        if(string[2] == 1)//deduct products sold
            newitemonhand = new BigDecimal(itemsonhand).subtract(new BigDecimal(string[1])).intValue();

        Log.v("newitemsonhand"," are "+newitemonhand);
        values.put(ITEMS_ON_HAND, newitemonhand);
        values.put(UPDATE, Constants.getDate());

        String where = ID + " = "+ string[0] ;
        if(cr.update(BASEURI, values, where, null) > 0) {
            return 1 ;
        }
        return 0;
    }


    public static int getProductQuantity(Context context, int stockid) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + stockid ;
        Cursor c = cr.query(BASEURI, new String[] {ITEMS_ON_HAND}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
                    return i ;
                }while(c.moveToNext());
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static int confirmdeleteProduct(Context context, String string)
    {
        ContentResolver cr = context.getContentResolver();
        String where =  REFERENCE + " = " + string ;
        if( cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }
	
	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
	public static JSONObject getProductJson(Context context) throws JSONException
	{
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSPRODUCT);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        String job =  js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
	}

	/*
	 * Acknowledgement to the server to tick item as a go
	 */
	public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, REFERENCE);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
	}


	public static String[] getAllProducts(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTNAME));
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
	
	public static int getAllProductQuantity(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
        Cursor cursor = cr.query(BASEURI, new String[] {ITEMS_ON_HAND}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ITEMS_ON_HAND));
        		return quantity;
        	} while (cursor.moveToNext());
        } 
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
	
	public static int getTax(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
//        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {TAXRATE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXRATE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
	
	public static int getProdid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
//        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }
	public static int getLocalProdid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
//        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }
	
	public static int getUnitPrice(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
//        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(UNITPRICE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }

    public static String getSku(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
            String quantity = cursor.getString(cursor.getColumnIndexOrThrow(SKU));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return "";
    }
	
	public static String[] getSoldOut(Context context)
    {
        ContentResolver cr = context.getContentResolver();

        String where = SYNCSTATUS + " != 3";
        where = ITEMS_ON_HAND +" < 1" ;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTNAME));
                 
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
    public static String getProdName(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + prodid ;
        Cursor cursor = cr.query(BASEURI, new String[] {PRODUCTNAME}, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTNAME));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public static String[] getProducts(Context context, long i) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {//name,qty,reorder,unitprice,tax,lastupdate,
                // description,
                String n = c.getString(c.getColumnIndexOrThrow(PRODUCTNAME));
                String b = c.getString(c.getColumnIndexOrThrow(UNITPRICE));
                String e = c.getString(c.getColumnIndexOrThrow(UPDATE));
                String p = c.getString(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
                String a = c.getString(c.getColumnIndexOrThrow(TAXRATE));
                String ar = c.getString(c.getColumnIndexOrThrow(REODER));
                String sk = c.getString(c.getColumnIndexOrThrow(SKU));
                String ct = c.getString(c.getColumnIndexOrThrow(DESCRIPTION));
                if(!Validating.areSet(e))
                    e = "never";
                custdetails = new String[]{n,b,e,p,a,ar,ct,sk};
                //name,price,update,itemonhand,taxrate,reorder,description,sku
                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }
}