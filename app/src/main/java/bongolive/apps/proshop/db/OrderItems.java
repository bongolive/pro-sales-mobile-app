/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import bongolive.apps.proshop.OrderItemList;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class OrderItems {
	public static final String TABLENAME = "order_items";
	public static final String ID = "_id";
	public static final String PRODUCTNO = "prod_system_id" ;
	public static final String PRODUCTQTY = "prod_order_qty" ;
	public static final String SALEPRICE = "sale_price" ;
	public static final String TAXRATE = "tax_rate";
	public static final String ORDERID = "order_id";
	public static final String SYNCSTATUS = "is_synced" ;
	public static final Uri BASEURI = Uri.parse("content://"+ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prostock.order_items" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prostock.order_items" ;
    public static final String ROW = ID+PRODUCTNO+PRODUCTQTY+SALEPRICE+TAXRATE+ORDERID+SYNCSTATUS ;
    
    public static String ARRAYNAME= "orderList" ;
    public static String ARRAYNAME2 = "orderitemsList";
    int id,syncstatus,productno,orderid,prodquantity ;
    double saleprice,taxrate ;
    
    public OrderItems(){
    	
    }
    public OrderItems(int id, int sync, int prod, int order, int proqty
    		,double price, double tax){
    	this.id = id ;
    	this.syncstatus = sync ;
    	this.productno = prod ;
    	this.orderid = order;
    	this.prodquantity = proqty ;
    	this.saleprice = price;
    	this.taxrate = tax ;
    }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSyncstatus() {
		return syncstatus;
	}
	public void setSyncstatus(int syncstatus) {
		this.syncstatus = syncstatus;
	}
	public int getProductno() {
		return productno;
	}
	public void setProductno(int productno) {
		this.productno = productno;
	}
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	public int getProdquantity() {
		return prodquantity;
	}
	public void setProdquantity(int prodquantity) {
		this.prodquantity = prodquantity;
	}
	public double getSaleprice() {
		return saleprice;
	}
	public void setSaleprice(double saleprice) {
		this.saleprice = saleprice;
	}
	public double getTaxrate() {
		return taxrate;
	}
	public void setTaxrate(double taxrate) {
		this.taxrate = taxrate;
	}
    
	public static int getItemsCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}
	public static int insertItems(Context context, String[] items){
		int itemcount = getItemsCount(context) ;

		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(PRODUCTNO, items[0]);
		cv.put(PRODUCTQTY, items[1]);
		cv.put(SALEPRICE, items[2]);
		cv.put(TAXRATE, items[3]);
		cv.put(ORDERID, items[4]);
		cv.put(SYNCSTATUS, items[5]);
		cr.insert(BASEURI, cv);
		if(getItemsCount(context) == itemcount + 1) 
		{
			return 1 ;
		} else {
			return 0 ;
		}
	}
    public static String[] getOrderItems(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {
                String n = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                String b = c.getString(c.getColumnIndexOrThrow(PRODUCTQTY));
                String e = c.getString(c.getColumnIndexOrThrow(SALEPRICE));
                String p = c.getString(c.getColumnIndexOrThrow(TAXRATE));
                String ar = c.getString(c.getColumnIndexOrThrow(SYNCSTATUS));
                String id = c.getString(c.getColumnIndexOrThrow(ID));
                custdetails = new String[]{n,b,e,p,ar,id};
                //prodno/prodqty/saleprice/taxrate/syncstatus
                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }
    public static ArrayList<OrderItemList> getItemsArrayList(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        ArrayList<OrderItemList> custdetails = new ArrayList<OrderItemList>();
        try {
            if(c.moveToFirst())
            {
                int count = 1;
                do {
                    count +=1 ;
                    int productid = c.getInt(c.getColumnIndexOrThrow(PRODUCTNO));
                    int quantity = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    double sprice = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double taxrate = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    String prodname = Stock.getProdName(context,String.valueOf(productid));
                    double grandtotal = Order.getGrandTotal(context, idd);
                    double taxamount = Order.getTaxAmount(context, idd);
                    double grandtotalwithnotax = new BigDecimal(grandtotal).subtract(new BigDecimal(taxamount)).doubleValue();

                    custdetails.add(new OrderItemList(prodname,quantity,grandtotal,
                            String.valueOf(count),sprice,productid,grandtotalwithnotax,taxrate,taxamount));

                } while (c.moveToNext());
                return custdetails;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }

   public static void deleteall(Context context){
       ContentResolver cr = context.getContentResolver();
       String where = ORDERID + " > 0";
       cr.delete(BASEURI, where, null);
       cr.delete(Order.BASEURI, where, null);
   }
   public static void processInformation(Context context) throws JSONException{
	   JSONObject json = sendOrderitemsJson(context);
	   try {
		if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
		   String res = json.getString(Constants.SUCCESS) ;
		   if(Integer.parseInt(res) == 1)
			{
                if(json.has(ARRAYNAME)){
                    JSONObject jsonObject = json.getJSONObject(ARRAYNAME);
                    if(jsonObject.has(ARRAYNAME2)){
                        JSONArray jsonArray = jsonObject.getJSONArray(ARRAYNAME2);
                        for(int i = 0; i< jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String ref = String.valueOf(jsonObject1.get(ORDERID));
                            boolean OK = updateSync(context, Integer.parseInt(ref),1);
                            if (OK == true) {
                            }
                        }
                    }
                }

			}
		}
	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }

    public static JSONObject sendOrderitemsJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = SYNCSTATUS + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSITEM);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(PRODUCTNO, c.getString(c.getColumnIndexOrThrow(PRODUCTNO)));
                    j.put(PRODUCTQTY, c.getString(c.getColumnIndexOrThrow(PRODUCTQTY)));
                    j.put(SALEPRICE, c.getString(c.getColumnIndexOrThrow(SALEPRICE)));
                    j.put(TAXRATE, c.getString(c.getColumnIndexOrThrow(TAXRATE)));
                    j.put(ORDERID, c.getString(c.getColumnIndexOrThrow(ORDERID))) ;
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put("orderitems",(Object)jarr);
                    array = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jsonObject);
                    JSONObject job = new JSONObject(array);
                    return job;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static boolean updateSync(Context context, int orderid, int flag)
   {
	   ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ; 
		values.put(SYNCSTATUS, flag);
		String where = ORDERID + " = "+ orderid ;
		if(cr.update(BASEURI, values, where, null) > 0)
		{
			return true ;
		}   else {
			return false ;
		}
   }


}
