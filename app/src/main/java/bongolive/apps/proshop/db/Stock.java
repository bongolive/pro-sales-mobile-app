/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.Validating;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class Stock 
{
	public static final String TABLENAME = "stock";
	public static final String ID = "prod_id";
	public static final String PRODUCTNAME = "product_name";
	public static final String STOCK = "stock_qty" ;//current_stock";
	public static final String PACKAGETYPE = "packaging_type";
	public static final String PACKAGEUNIT = "packaging_units";
	public static final String UNITPRICE = "unit_sale_price" ;
	public static final String TAXRATE = "tax_rate";
	public static final String UPDATE = "last_update" ;
	public static final String REFERENCE = "prod_system_id" ;//"system_prod_id" ;
	public static final String STOCKID = "sys_stock_id" ;//"Sys_stock_id" ;
    public static final String SKU = "sku" ;//"Sys_stock_id" ;
    public static final String UNITSIZE = "size" ;//"Sys_stock_id" ;
    public static final String UNITMEASURE = "measure" ;//"Sys_stock_id" ;
	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.bongolive.apps.stock" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.bongolive.apps.stock" ;
	private static final String TAG = "STOCK";
	static JsonObjectJsonArray js ;
	public static String ARRAYNAME = "productsList" ;
 
	private static JSONArray stockarray ;
	
	public Stock() {
		
	}
	
	 
	 private static int isStockPresent(Context context, int custid)
	    {
		 
	    	Cursor c = context.getContentResolver().query(BASEURI, new String[] {REFERENCE}, null,null,null) ;
	    	 try {
	    	int i= 0 ;
	    	if(c.moveToFirst())
	    	{
	    		do {
	    			i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
	    			if(i == custid) {
	    				return 1 ;
	    			}
	    		}while(c.moveToNext());
	    	} 
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    } 
	public static int getStockQuantity(Context context, int stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = REFERENCE + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[] {STOCK}, where,null,null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(STOCK));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}
	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static int updateProduct(Context context, String[] string, int ref)
	{
		ContentResolver cr = context.getContentResolver(); 
		ContentValues values = new ContentValues() ;/*
        int currentstock = getStockQuantity(context,ref);
//		int stockqt = Integer.parseInt(string[4]) + currentstock ;
        //packagetype,punit,prodname,reference,stock,taxrate,unitprice,stockid*/
		
		values.put(PACKAGETYPE, string[0]);
		values.put(PACKAGEUNIT, string[1]);
		values.put(PRODUCTNAME, string[2]) ;
		values.put(STOCK,  string[4] );
		values.put(TAXRATE, string[5]) ;
		values.put(UNITPRICE, string[6]) ;
		values.put(UPDATE, Constants.getDate());
        values.put(SKU, string[8]);
        values.put(UNITMEASURE, string[9]);
        values.put(UNITSIZE, string[10]);
		
		String where = REFERENCE + " = "+ ref ; 
		if(cr.update(BASEURI, values, where, null) > 0) {
			return 1 ; 
		} 
		return 0;
	}

    public static String getPackageUnits(Context context, String pn){
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " = " + DatabaseUtils.sqlEscapeString(pn) ;
        Cursor c = cr.query(BASEURI, new String[] {PACKAGEUNIT}, where,null,null) ;
        try {
            String i= "" ;
            if(c.moveToFirst())
            {
                    i = c.getString(c.getColumnIndexOrThrow(PACKAGEUNIT));
                    return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "" ;
    }
	public static int updateProductDeminish(Context context, String productname, int userqty, boolean action)
	{
		ContentResolver cr = context.getContentResolver(); 
		ContentValues values = new ContentValues() ;
		int stockqt = 0;
		int remainder = 0 ; 
		stockqt = getAllStockQuantity(context, productname); 
		remainder = stockqt - userqty ; 
		values.put(STOCK,  remainder ); 
		values.put(UPDATE, Constants.getDate()); 
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;   
		if(cr.update(BASEURI, values, where, null) > 0) {
			return 1 ; 
		}  
		return 0;
	}
	
	/*
	 * Save a new product in the stock table
	 */
	public static int insertStock(Context context, String[] string)
	{
		int productcount = getStockCount(context) ;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(PACKAGETYPE, string[0]);
		values.put(PACKAGEUNIT, string[1]);
		values.put(PRODUCTNAME, string[2]) ; 
		values.put(REFERENCE, Integer.parseInt(string[3]));
		values.put(STOCK, Integer.parseInt(string[4]));
		values.put(TAXRATE, Double.parseDouble(string[5])) ;
		values.put(UNITPRICE, Double.parseDouble(string[6])) ;
		values.put(UPDATE, Constants.getDate()); 
		values.put(STOCKID, string[7]);
        values.put(SKU, string[8]);
        values.put(UNITMEASURE, string[9]);
        values.put(UNITSIZE, string[10]);
        //package type/packageunit/productname/reference/stock/taxrate/unitprice/stockid
        //packagetype,punit,prodname,reference,stock,taxrate,unitprice,stockid
		cr.insert(BASEURI, values);  
		if(getStockCount(context) == productcount+1){
			return 1 ;
		} else {
			return 0 ;
		}  
	}
	/*
	 * How many items are in the table ?
	 */
	public static int getStockCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}
	
	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
	public static JSONObject getStockJson(Context context) throws JSONException
	{
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSTOCKONCE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        String job =  js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
	}

	/*
	 * Acknowledgement to the server to tick item as a go
	 */
	public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, REFERENCE);
        jobs.put(STOCKID, requiredid[1]);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
	}

	public static void saveStock(Context context) throws JSONException
	{
		JSONObject json = getStockJson(context) ;
		try { 
			if(json.has(Constants.SUCCESS)){
			 String res = json.getString(Constants.SUCCESS) ;
			 if(Integer.parseInt(res) == 1)
			   {
                   if(json.has(ARRAYNAME)) {
                       stockarray = json.getJSONArray(ARRAYNAME) ;
                       for (int i = 0; i < stockarray.length(); i++) {
                           JSONObject js = stockarray.getJSONObject(i);
                           String pty = js.getString(PACKAGETYPE);
                           String put = js.getString(PACKAGEUNIT);
                           String pdn = js.getString(PRODUCTNAME);
                           String ref = js.getString(REFERENCE);
                           String stc = js.getString(STOCK);
                           String txr = js.getString(TAXRATE);
                           String upc = js.getString(UNITPRICE);
                           String stk = js.getString(STOCKID);
                           String sku = js.getString(SKU);
                           String mea = js.getString(UNITMEASURE);
                           String siz = js.getString(UNITSIZE);
                           String[] stockvalue = new String[]{pty, put, pdn, ref, stc, txr, upc, stk,sku,mea,siz};
                           //package type/packageunit/productname/reference/stock/taxrate/unitprice/stockid
                           //packagetype,punit,prodname,reference,stock,taxrate,unitprice,stockid
                           if (Validating.areSet(stockvalue)) {
                               String[] reqired = new String[]{ref, stk};
                               switch (isStockPresent(context, Integer.parseInt(ref))) {
                                   case 0:
                                       //insert a new one
                                       if (insertStock(context, stockvalue) == 1) ;
                                       sendAck(context, reqired);
                                       break;
                                   case 1:
                                       //update the stock is present
                                       if (updateProduct(context, stockvalue, Integer.parseInt(ref)) == 1);
                                       sendAck(context, reqired);
                                       break;
                               }
                           }
                       }
                   }
		       }
			 }
				 
		 } catch (JSONException e) { 
						e.printStackTrace();
	 }
	} 
	
	public static String[] getAllStock(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTNAME));
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
	
	public static int getAllStockQuantity(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
        Cursor cursor = cr.query(BASEURI, new String[] {STOCK}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(STOCK));
        		return quantity;
        	} while (cursor.moveToNext());
        } 
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
	
	public static int getTax(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {TAXRATE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXRATE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
	
	public static int getStockid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {REFERENCE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(REFERENCE));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }
	
	public static int getUnitPrice(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(UNITPRICE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }public static String getSku(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTNAME +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
            String quantity = cursor.getString(cursor.getColumnIndexOrThrow(SKU));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return "";
    }
	
	public static String[] getSoldOut(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = null;
        where = STOCK +" < 1" ;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTNAME));
                 
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
    public static String getProdName(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + prodid ;
        Cursor cursor = cr.query(BASEURI, new String[] {PRODUCTNAME}, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                do{
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTNAME));
                    return quantity;
                } while (cursor.moveToNext());
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public static String[] getProducts(Context context, long i) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {
                String n = c.getString(c.getColumnIndexOrThrow(PRODUCTNAME));
                String b = c.getString(c.getColumnIndexOrThrow(UNITPRICE));
                String e = c.getString(c.getColumnIndexOrThrow(UNITMEASURE));
                String p = c.getString(c.getColumnIndexOrThrow(UNITSIZE));
                String a = c.getString(c.getColumnIndexOrThrow(TAXRATE));
                String ar = c.getString(c.getColumnIndexOrThrow(SKU));
                String ct = c.getString(c.getColumnIndexOrThrow(STOCK));

                custdetails = new String[]{n,b,e,p,a,ar,ct};
                //proname,price,measu,size,tax,sku,stoc
                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }
}