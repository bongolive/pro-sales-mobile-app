/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ContentProviderEngine  extends ContentProvider
{
	public static final String AUTHORITY = "bongolive.apps.proshop.db.ContentProviderEngine" ;
	Context _context;
	//static final HashMap<String, String> hashmap ;
	private static UriMatcher _uriMatcher  ; 
	private static final int GENERAL_ORDERS = 3;
	private static final int SPECIFIC_ORDERS = 4 ;
	private static final int GENERAL_ORDERITEMS = 5 ;
	private static final int SPECIFIC_ORDERITEMS = 6 ;
	private static final int GENERAL_CUSTOMER = 7 ;
	private static final int SPECIFIC_CUSTOMER = 8 ;
	private static final int GENERAL_PAYMODE = 9 ;
	private static final int SPECIFIC_PAYMODE = 10;
	private static final int GENERAL_SETTINGS = 11 ;
	private static final int SPECIFIC_SETTINGS = 12 ;
	private static final int GENERAL_STOCK = 13;
	private static final int SPECIFIC_STOCK = 14 ;
    private static final int GENERAL_LOC = 15;
    private static final int SPECIFIC_LOC = 16;
    private static final int GENERAL_MULT = 17;
    private static final int SPECIFIC_MULT = 18;
    private static final int GENERAL_PRODUCT = 19;
    private static final int SPECIFIC_PRODUCT = 20;
    private static final int GENERAL_STOCKFULL = 21;
    private static final int SPECIFIC_STOCKFULL = 22;
	static {
	    _uriMatcher  = new UriMatcher(UriMatcher.NO_MATCH) ; 
		_uriMatcher.addURI(AUTHORITY, Order.TABLENAME, GENERAL_ORDERS) ;
		_uriMatcher.addURI(AUTHORITY, Order.TABLENAME+ "/#", SPECIFIC_ORDERS) ;
		_uriMatcher.addURI(AUTHORITY, Customers.TABLENAME, GENERAL_CUSTOMER) ;
		_uriMatcher.addURI(AUTHORITY, Customers.TABLENAME + "/#", SPECIFIC_CUSTOMER) ;
		_uriMatcher.addURI(AUTHORITY, OrderItems.TABLENAME, GENERAL_ORDERITEMS) ;
		_uriMatcher.addURI(AUTHORITY, OrderItems.TABLENAME + "/#", SPECIFIC_ORDERITEMS) ;
		_uriMatcher.addURI(AUTHORITY, PaymentMode.TABLENAME, GENERAL_PAYMODE) ;
		_uriMatcher.addURI(AUTHORITY, PaymentMode.TABLENAME + "/#", SPECIFIC_PAYMODE);
		_uriMatcher.addURI(AUTHORITY, Settings.TABLENAME, GENERAL_SETTINGS) ;
		_uriMatcher.addURI(AUTHORITY, Settings.TABLENAME + "/#", SPECIFIC_SETTINGS) ;
		_uriMatcher.addURI(AUTHORITY, Stock.TABLENAME, GENERAL_STOCK) ;
		_uriMatcher.addURI(AUTHORITY, Stock.TABLENAME + "/#", SPECIFIC_STOCK) ;
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME, GENERAL_LOC) ;
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME + "/#", SPECIFIC_LOC) ;
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME, GENERAL_MULT) ;
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME + "/#", SPECIFIC_MULT) ;
        _uriMatcher.addURI(AUTHORITY, Products.TABLENAME, GENERAL_PRODUCT) ;
        _uriMatcher.addURI(AUTHORITY, Products.TABLENAME + "/#", SPECIFIC_PRODUCT) ;
        _uriMatcher.addURI(AUTHORITY, StockFull.TABLENAME, GENERAL_STOCKFULL) ;
        _uriMatcher.addURI(AUTHORITY, StockFull.TABLENAME + "/#", SPECIFIC_STOCKFULL) ;
	}
	
	@Override
	public boolean onCreate() { 
		try{
		DatabaseHandler.getInstance(getContext());
		} catch (SQLiteException ex)
		{
			throw new Error(ex.toString());
		}
		return true ;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getReadableDatabase() ;
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			qb.setTables(Customers.TABLENAME) ;
			break ;
		case SPECIFIC_CUSTOMER:
			qb.setTables(Customers.TABLENAME);
			qb.appendWhere(Customers.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_ORDERITEMS:
			qb.setTables(OrderItems.TABLENAME) ;
			break ;
		case SPECIFIC_ORDERITEMS:
			qb.setTables(OrderItems.TABLENAME);
			qb.appendWhere(OrderItems.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_ORDERS:
			qb.setTables(Order.TABLENAME);
			break ;
		case SPECIFIC_ORDERS:
			qb.setTables(Order.TABLENAME);
			qb.appendWhere(Order.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PAYMODE:
			qb.setTables(PaymentMode.TABLENAME);
			break ;
		case SPECIFIC_PAYMODE:
			qb.setTables(PaymentMode.TABLENAME);
			qb.appendWhere(PaymentMode.ID + " =  " + uri.getLastPathSegment()) ;
			break ; 
		case GENERAL_SETTINGS:
			qb.setTables(Settings.TABLENAME);
			break ;
		case SPECIFIC_SETTINGS:
			qb.setTables(Settings.TABLENAME);
			qb.appendWhere(Settings.ID + " =  " + uri.getLastPathSegment()) ;
		case GENERAL_STOCK:
			qb.setTables(Stock.TABLENAME);
			break ;
		case SPECIFIC_STOCK:
			qb.setTables(Stock.TABLENAME);
			qb.appendWhere(Stock.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_LOC:
			qb.setTables(Tracking.TABLENAME);
			break ;
		case SPECIFIC_LOC:
			qb.setTables(Stock.TABLENAME);
			qb.appendWhere(Tracking.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_MULT:
			qb.setTables(MultimediaContents.TABLENAME);
			break ;
		case SPECIFIC_MULT:
			qb.setTables(Stock.TABLENAME);
			qb.appendWhere(MultimediaContents.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PRODUCT:
			qb.setTables(Products.TABLENAME);
			break ;
		case SPECIFIC_PRODUCT:
			qb.setTables(Stock.TABLENAME);
			qb.appendWhere(Products.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_STOCKFULL:
			qb.setTables(StockFull.TABLENAME);
			break ;
		case SPECIFIC_STOCKFULL:
			qb.setTables(Stock.TABLENAME);
			qb.appendWhere(StockFull.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		default:
			throw new IllegalArgumentException(" UNKOWN URI :" + uri);
		}  
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder) ;
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c ;
	}

	@Override
	public String getType(Uri uri) { 
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			return Customers.GENERAL_CONTENT_TYPE ;
		case SPECIFIC_CUSTOMER:
			return Customers.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_ORDERITEMS:
			return OrderItems.GENERAL_CONTENT_TYPE ;
		case SPECIFIC_ORDERITEMS:
			return OrderItems.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_ORDERS:
			return Order.GENERAL_CONTENT_TYPE;
		case SPECIFIC_ORDERS:
			return Order.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PAYMODE:
			return PaymentMode.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PAYMODE:
			return PaymentMode.SPECIFIC_CONTENT_TYPE; 
		case GENERAL_SETTINGS:
			return Settings.GENERAL_CONTENT_TYPE;
		case SPECIFIC_SETTINGS:
			return Settings.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_STOCK:
			return Stock.GENERAL_CONTENT_TYPE;
		case SPECIFIC_STOCK:
			return Stock.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_LOC:
			return Tracking.GENERAL_CONTENT_TYPE;
		case SPECIFIC_LOC:
			return Tracking.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_MULT:
			return MultimediaContents.GENERAL_CONTENT_TYPE;
		case SPECIFIC_MULT:
			return MultimediaContents.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PRODUCT:
			return Products.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PRODUCT:
			return Products.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_STOCKFULL:
			return StockFull.GENERAL_CONTENT_TYPE;
		case SPECIFIC_STOCKFULL:
			return StockFull.SPECIFIC_CONTENT_TYPE ;
		default:
			throw new IllegalArgumentException(" Unkown TYPE " + uri) ;
		}  
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		long id ;
		switch(_uriMatcher.match(uri))
		{
		case GENERAL_CUSTOMER:
			id = db.insertOrThrow(Customers.TABLENAME, null, values);
			break;
		case GENERAL_ORDERITEMS:
			id = db.insertOrThrow(OrderItems.TABLENAME, null, values);
			break;
		case GENERAL_ORDERS:
			id = db.insertOrThrow(Order.TABLENAME, null, values);
			break;
		case GENERAL_PAYMODE:
			id = db.insertOrThrow(PaymentMode.TABLENAME, null, values);
			break; 
		case GENERAL_SETTINGS:
			id = db.insertOrThrow(Settings.TABLENAME, null, values) ;
			break;
		case GENERAL_STOCK:
			id = db.insertOrThrow(Stock.TABLENAME, null, values) ;
            break;
		case GENERAL_LOC:
			id = db.insertOrThrow(Tracking.TABLENAME, null, values) ;
			break;
		case GENERAL_MULT:
			id = db.insertOrThrow(MultimediaContents.TABLENAME, null, values) ;
			break;
		case GENERAL_PRODUCT:
			id = db.insertOrThrow(Products.TABLENAME, null, values) ;
			break;
		case GENERAL_STOCKFULL:
			id = db.insertOrThrow(StockFull.TABLENAME, null, values) ;
			break;
		default:
			throw new IllegalArgumentException(" error invalid uri " + uri);
		}
		Uri inserturi = ContentUris.withAppendedId(uri, id);
		getContext().getContentResolver().notifyChange(inserturi, null);
		return inserturi ;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
		int count = 0 ;
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			count = db.delete(Customers.TABLENAME, selection, selectionArgs);
			break;
		case SPECIFIC_CUSTOMER:
			count = db.delete(Customers.TABLENAME, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERITEMS:
			count = db.delete(OrderItems.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_ORDERITEMS:
			count = db.delete(OrderItems.TABLENAME, OrderItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERS:
			count = db.delete(Order.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_ORDERS:
			count = db.delete(Order.TABLENAME, Order.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMODE:
			count = db.delete(PaymentMode.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PAYMODE:
			count = db.delete(PaymentMode.TABLENAME,PaymentMode.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;  
		case GENERAL_SETTINGS:
			count = db.delete(Settings.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_SETTINGS:
			count = db.delete(Settings.TABLENAME,  Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STOCK:
			count = db.delete(Stock.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_STOCK:
			count = db.delete(Stock.TABLENAME,  Stock.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_LOC:
			count = db.delete(Tracking.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_LOC:
			count = db.delete(Tracking.TABLENAME,  Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_MULT:
			count = db.delete(MultimediaContents.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_MULT:
			count = db.delete(MultimediaContents.TABLENAME,  MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PRODUCT:
			count = db.delete(Products.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PRODUCT:
			count = db.delete(Products.TABLENAME,  Products.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STOCKFULL:
			count = db.delete(StockFull.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_STOCKFULL:
			count = db.delete(StockFull.TABLENAME,  StockFull.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
			getContext().getContentResolver().notifyChange(uri, null);
			return count ;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		int count ;
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			count = db.update(Customers.TABLENAME, values, selection, selectionArgs);
			break;
		case SPECIFIC_CUSTOMER:
			count = db.update(Customers.TABLENAME, values, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERITEMS:
			count = db.update(OrderItems.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_ORDERITEMS:
			count = db.update(OrderItems.TABLENAME, values, OrderItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERS:
			count = db.update(Order.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_ORDERS:
			count = db.update(Order.TABLENAME, values, Order.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMODE:
			count = db.update(PaymentMode.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PAYMODE:
			count = db.update(PaymentMode.TABLENAME, values, PaymentMode.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break; 
		case GENERAL_SETTINGS:
			count = db.update(Settings.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_SETTINGS:
			count = db.update(Settings.TABLENAME, values, Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STOCK:
			count = db.update(Stock.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_STOCK:
			count = db.update(Stock.TABLENAME, values, Stock.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_LOC:
			count = db.update(Tracking.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_LOC:
			count = db.update(Tracking.TABLENAME, values, Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_MULT:
			count = db.update(MultimediaContents.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_MULT:
			count = db.update(MultimediaContents.TABLENAME, values, MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PRODUCT:
			count = db.update(Products.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PRODUCT:
			count = db.update(Products.TABLENAME, values, Products.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STOCKFULL:
			count = db.update(StockFull.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_STOCKFULL:
			count = db.update(StockFull.TABLENAME, values, StockFull.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count ;
	}

}
