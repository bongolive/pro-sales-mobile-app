/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.AppPreference;
import bongolive.apps.proshop.Validating;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class Settings {
	public static final String TABLENAME = "settings" ;
	public static final String ID = "id" ;
	public static final String PRINTRECEIPT = "print_receipt" ;
	public static final String EDITSALES = "alter_sales_price" ;
	public static final String TAXINCLUSIVE = "include_tax" ;
	public static final String VAT = "vat" ;
	public static final String DISCOUNT = "enable_discounts" ;
    public static final String TRACKTIME = "tracktime";
	public static final String FIXEDPRICE = "use_fixed_prices" ;
	public static final String DISCOUNT_LIMIT = "discount_limit" ;
	public static final String CURRENCY = "default_currency" ;
	public static final Uri BASEURI = Uri.parse("content://" + ContentProviderEngine.AUTHORITY + "/" + TABLENAME) ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prostock.settings" ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prostock.settings" ;
    public static final String ARRAYNAME = "settingList";
    public static final String SETTINGSOBJECT = "settings";
    public static final String PAYMODESOBJECT = "pay_modes";
	private static final String TAG = "SETTINGSPAYMENTMODE";
    private static  JSONArray settingsarray;

	int id,printrece,editsale,taxinc ;
	public Settings(int i,int pr, int ed, int tx)
	{
		this.id = i ;
		this.printrece = pr ;
		this.editsale = ed ;
		this.taxinc = tx ;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrintrece() {
		return printrece;
	}
	public void setPrintrece(int printrece) {
		this.printrece = printrece;
	}
	public int getEditsale() {
		return editsale;
	}
	public void setEditsale(int editsale) {
		this.editsale = editsale;
	}
	public int getTaxinc() {
		return taxinc;
	}
	public void setTaxinc(int taxinc) {
		this.taxinc = taxinc;
	}
	
	public static int getSettingsCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;  
	} 
	
	
	
	public static int insertSettings(Context context,String[] settings)
	{
		int settingsCount = getSettingsCount(context) ;

		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(PRINTRECEIPT, settings[0]) ;
		cv.put(EDITSALES, settings[1]);
		cv.put(TAXINCLUSIVE, settings[2]);
		cv.put(DISCOUNT, settings[3]);
		cv.put(DISCOUNT_LIMIT, settings[4]);
		cv.put(VAT, settings[5]);
		cv.put(FIXEDPRICE, settings[6]);
		cv.put(CURRENCY, settings[7]);
        cv.put(TRACKTIME,settings[8]);
		cr.insert(BASEURI, cv);
		if(getSettingsCount(context) == settingsCount +1)
		{
			return 1 ;
		} else {
			return 0 ;
		}
	}
	  
	public static void saveSettingsAndMode(Context context) throws JSONException
	{
		JSONObject json = getSettingsJson(context) ; 
		 settingsarray = json.getJSONArray(ARRAYNAME) ;
		try { 
			if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
			 String res = json.getString(Constants.SUCCESS) ;
			 if(Integer.parseInt(res) == 1)
			   {
				 for(int i = 0; i < settingsarray.length(); i ++) {
				      JSONObject js = settingsarray.getJSONObject(i) ;   
				      
				    	  if(!TextUtils.isEmpty(js.getString(SETTINGSOBJECT)))
					      { 
					    	  JSONArray ja = js.getJSONArray(SETTINGSOBJECT);
					    	  for(int k = 0; k< ja.length(); k++) { 
					    		  JSONObject j =  ja.getJSONObject(k);
					    		  String print = j.getString(PRINTRECEIPT);
		                             String editsale = j.getString(EDITSALES);
		                             String tax = j.getString(TAXINCLUSIVE);
		                             String dic = j.getString(DISCOUNT);
		                             String diclimit = j.getString(DISCOUNT_LIMIT);
		                             String vat = j.getString(VAT);
		                             String fixedp = j.getString(FIXEDPRICE);
		                             String curr = j.getString(CURRENCY);
                                     String trk = j.getString(TRACKTIME);
									 String[] settings = new String[] {print,editsale,tax,dic,diclimit,vat,fixedp,curr,trk} ;
						             if(Validating.areSet(settings))
						             {
						            	 int flag = getSettingsCount(context);
						            	 if(flag == 0){
						            		 Log.e(TAG, "SETTINGS IS NULL AND INSERTED");
							            	 insertSettings(context, settings); 
						            	 }else{
						            		 Log.e(TAG, "SETTINGS IS THERE ALREADY");
						            	 }
						             }
					    	  } 
					      }  
				    	  if(!TextUtils.isEmpty(js.getString(PAYMODESOBJECT)))
					      { 
					    	  JSONArray m = js.getJSONArray(PAYMODESOBJECT);
					    	  Log.e(TAG, m.toString());
					    	  for(int j= 0; j < m.length(); j++){
					    		  JSONObject job = m.getJSONObject(j);
					    		  String modetittle = job.getString(PaymentMode.MODETITLE);
						    	  String sysmode = job.getString(PaymentMode.SYSTEMMODE);
						    	  String account = job.getString(PaymentMode.SYMBOL);
									 String[] paymodes = new String[] {modetittle,sysmode,account} ;
									 if(Validating.areSet(paymodes))
						             {
										 int flags = PaymentMode.getModeCount(context);
						            	 if(flags <= 5){
						            		 Log.e(TAG, "PAYMENT MODE IS NULL AND INSERTED");
							            	 if( PaymentMode.insertPaymentMode(context, paymodes) == 1){
                                                 PaymentMode.sendAck(context, sysmode);
                                             }

						            	 }else{
						            		 Log.e(TAG, "PAYMENT MODE IS THERE ALREADY"); 
						             }
						          }
					    	  } 
					      }   
			       }
				 }
			   } 
		 } catch (JSONException e) { 
						e.printStackTrace();
	 }
	}
	private static JSONObject getSettingsJson(Context context) throws JSONException { 
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSETTINGS);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        String job =  js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
                Log.v("ERROR JOB", " JSONOBJECT IS NOT VALID OR NULL");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;

	}
	/*
	*  register device
	* */
	public static JSONObject registerDevice(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGREGISTRATION);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));

		JsonObjectJsonArray js = new JsonObjectJsonArray();
		String ret = js.getJsonArray(Constants.SERVERAUTHENTICITY, JsonObjectJsonArray.POST, jobs);
		JSONObject json = new JSONObject(ret);
		return json;
	}
	/*
	* validate account
	* */
	public static JSONObject validateToken(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
		AppPreference appPreference = new AppPreference(context);
        jobs.put("tag", Constants.AUTHENTICATETAG);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.AUTHTOKENSERVER, appPreference.getAuthkey());

		JsonObjectJsonArray js = new JsonObjectJsonArray();
		String ret = js.getJsonArray(Constants.SERVERAUTHENTICITY, JsonObjectJsonArray.POST, jobs);
		JSONObject json = new JSONObject(ret);
		return json;
	}

	public static void perform_account_validation(Context context){
		AppPreference appPreference = new AppPreference(context);
		try {
			JSONObject jsonObject = Settings.validateToken(context);
			if (jsonObject != null) {
				if (!jsonObject.has(Constants.SUCCESS)) {
					appPreference.clear_authentications();
				} else {
					appPreference.save_last_sync(Constants.getDateOnly2());
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

    public static int getTrackTime(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(TRACKTIME));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

	public static String[] getSettings(Context context) { 
        ContentResolver cr = context.getContentResolver(); 
        Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
        try {
        if(cursor.getCount() >0)
        {
            String[] str = new String[cursor.getCount()]; 
 
            while (cursor.moveToNext())
            { 
                 String print =  cursor.getString(cursor.getColumnIndex(PRINTRECEIPT));
                 String edit =  cursor.getString(cursor.getColumnIndex(EDITSALES));
                 String taxinc =  cursor.getString(cursor.getColumnIndex(TAXINCLUSIVE));
                 String vat = cursor.getString(cursor.getColumnIndex(VAT));
                 String discount =  cursor.getString(cursor.getColumnIndex(DISCOUNT));
                 String fixed =  cursor.getString(cursor.getColumnIndex(FIXEDPRICE));
                 String dlimit =  cursor.getString(cursor.getColumnIndex(DISCOUNT_LIMIT));
                 String currency =  cursor.getString(cursor.getColumnIndex(CURRENCY));

                 str = new String[]{print,edit,taxinc,vat,discount,fixed,dlimit,currency};
             }
            cursor.close();
            return str;
        }
        else
        {
        	cursor.close();
            return new String[] {};
        }
        }finally {
			 if(cursor != null) {
		            cursor.close();
		        }
		 }  
}


}
