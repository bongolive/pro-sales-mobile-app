/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class PaymentMode {
	public static final String TABLENAME = "paymentmode" ;
	public static final String ID = "mode_id" ;
	public static final String MODETITLE = "mode_title" ;
	public static final String SYSTEMMODE = "system_mode_id" ;
	public static final String SYMBOL = "mode_symbol" ;
	public static final Uri BASEURI = Uri.parse("content://"+ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prostock.paymentmode" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prostock.paymentmode" ;

	
	int id,account ;
	String modetitle,systmode ;

	public PaymentMode(int i,int ac, String mod,String sys)
	{
		this.id = i;
		this.account = ac ;
		this.modetitle = mod ;
		this.systmode = sys ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAccount() {
		return account;
	}

	public void setAccount(int account) {
		this.account = account;
	}

	public String getModetitle() {
		return modetitle;
	}

	public void setModetitle(String modetitle) {
		this.modetitle = modetitle;
	}

	public String getSystmode() {
		return systmode;
	}

	public void setSystmode(String systmode) {
		this.systmode = systmode;
	}
	
	public static int insertPaymentMode(Context context, String[] mode)
	{
		int modecount = getModeCount(context) ;

		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(MODETITLE, mode[0]);
		cv.put(SYSTEMMODE, mode[1]) ;
		cv.put(SYMBOL, mode[2]);
		cr.insert(BASEURI, cv);
		if(getModeCount(context) == modecount +1)
		{
			return 1;
		} else {
			return 0;
		}
	}

    public static void sendAck(Context context, String requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(SYSTEMMODE, requiredid);
        jobs.put(Constants.ACKKEY, SYSTEMMODE);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
    }
	
	public static int getModeCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}

	public static String[] getPaymod(Context context) {
	        ContentResolver cr = context.getContentResolver();
	        Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
	        try {
	        if(cursor.getCount() >0)
	        {
	            String[] str = new String[cursor.getCount()];
	            int i = 0;
	 
	            while (cursor.moveToNext())
	            {
	                 str[i] = cursor.getString(cursor.getColumnIndex(MODETITLE));
	                 i++;
	             }
	            cursor.close();
	            return str;
	        }
	        else
	        {
	        	cursor.close();
	            return new String[] {};
	        }
	        }finally {
				 if(cursor != null) {
			            cursor.close();
			        }
			 }  
	}

    public static String getPaymodId(Context context, String pname) {
        ContentResolver cr = context.getContentResolver();
        String where = MODETITLE + " = "+ DatabaseUtils.sqlEscapeString(pname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try {
            if(cursor.getCount() >0)
            {
                cursor.moveToFirst();
                    return cursor.getString(cursor.getColumnIndex(SYSTEMMODE));
            }
            else
            {
                cursor.close();
                return "";
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
    }

}
