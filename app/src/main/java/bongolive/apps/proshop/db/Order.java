/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.GpsTracker;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;


public class Order {
	public static final String TABLENAME = "orders" ;
	public static final String ID = "_id";
	public static final String CUSTOMERID = "customer_id";// required to send system_customer_id
	public static final String ORDERDATE = "order_date";
	public static final String PAYMENTSTATUS ="payment_status" ;
	public static final String PAYMENTMOD = "payment_mode";
    public static final String PAYMENTAMOUNT ="payment_amount" ;
	public static final String RECEIPT = "receipt_no" ;
	public static final String TOTALSALES = "total_sales" ;
	public static final String TOTALTAX = "total_tax";
	public static final String SYNCSTATUS = "is_synced" ;
    public static final String ISPRINTED = "is_printed" ;
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
	public static final Uri BASEURI = Uri.parse("content://"+ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prostock.order" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prostock.order" ;
	private static final String ARRAYNAME = "orderList";
    private static final String ARRAYNAME2 = "ordersList";
    public static final String LOCALCUSOMER = "local_cust_id";
    
	int id,customer,sync,paymntmod ;
	double pay,totalsales,totaltax ;
	String date,paystatus,receipt ;
	
	public Order() {
		
	}
	public Order(int id, int cus, int sy, int pym, double paynt, double sales, double tax
			,String dt, String status, String receipt){
		this.id =id ;
		this.customer = cus ;
		this.sync = sy ;
		this.paymntmod = pym ;
		this.pay = paymntmod ;
		this.totalsales = sales ;
		this.totaltax = tax ;
		this.date = dt ;
		this.paystatus = status ;
		this.receipt = receipt ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomer() {
		return customer;
	}

	public void setCustomer(int customer) {
		this.customer = customer;
	}

	public int getSync() {
		return sync;
	}

	public void setSync(int sync) {
		this.sync = sync;
	}

	public int getPaymntmod() {
		return paymntmod;
	}

	public void setPaymntmod(int paymntmod) {
		this.paymntmod = paymntmod;
	}

	public double getPay() {
		return pay;
	}

	public void setPay(double pay) {
		this.pay = pay;
	}

	public double getTotalsales() {
		return totalsales;
	}

	public void setTotalsales(double totalsales) {
		this.totalsales = totalsales;
	}

	public double getTotaltax() {
		return totaltax;
	}

	public void setTotaltax(double totaltax) {
		this.totaltax = totaltax;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPaystatus() {
		return paystatus;
	}

	public void setPaystatus(String paystatus) {
		this.paystatus = paystatus;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	
	public static int insertOrders(Context context, String[] orders){
        GpsTracker gpsTracker = new GpsTracker(context.getApplicationContext());
		int ordercount = getOrdersCount(context) ;
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(CUSTOMERID, orders[0]) ;
		cv.put(ORDERDATE, Constants.getDate());
		cv.put(PAYMENTSTATUS, orders[1]);
		cv.put(PAYMENTMOD, orders[2]);
		cv.put(PAYMENTAMOUNT, orders[3]);
		cv.put(RECEIPT, 0);
		cv.put(TOTALSALES, orders[4]);
		cv.put(TOTALTAX, orders[5]);
		cv.put(SYNCSTATUS, orders[6]);
        cv.put(LOCALCUSOMER, orders[7]);
        cv.put(LAT, gpsTracker.getLatitude());
        cv.put(LONGTUDE, gpsTracker.getLongitude());
		cr.insert(BASEURI, cv);
		if(getOrdersCount(context) == ordercount + 1)
		{
			return 1 ;
		} else {
			return 0 ;
		}
	}
    //gettotaltax,getpayamount
	public static int getOrdersCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;  
	}
    public static JSONObject sendOrderitemsJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = SYNCSTATUS + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGORDERS);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(CUSTOMERID, c.getString(c.getColumnIndexOrThrow(CUSTOMERID)));
                    j.put(ORDERDATE, c.getString(c.getColumnIndexOrThrow(ORDERDATE)));
                    j.put(PAYMENTSTATUS, c.getString(c.getColumnIndexOrThrow(PAYMENTSTATUS)));
                    j.put(PAYMENTMOD, c.getString(c.getColumnIndexOrThrow(PAYMENTMOD)));
                    j.put(PAYMENTAMOUNT, c.getString(c.getColumnIndexOrThrow(PAYMENTAMOUNT)));
                    j.put(PAYMENTMOD, c.getString(c.getColumnIndexOrThrow(PAYMENTMOD)));
                    j.put(RECEIPT, c.getString(c.getColumnIndexOrThrow(RECEIPT)));
                    j.put(TOTALSALES, c.getString(c.getColumnIndexOrThrow(TOTALSALES)));
                    j.put(TOTALTAX, c.getString(c.getColumnIndexOrThrow(TOTALTAX)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put("orders",(Object)jarr);
                array = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }
	 
	public static void processInformation(Context context) throws JSONException{
		   JSONObject json = sendOrderitemsJson(context);
		   try {
			if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
			   String res = json.getString(Constants.SUCCESS) ;
			   if(Integer.parseInt(res) == 1)
				{
                    if(json.has(ARRAYNAME)){
                        JSONObject jsonObject = json.getJSONObject(ARRAYNAME);
                        if(jsonObject.has(ARRAYNAME2)){
                            JSONArray jsonArray = jsonObject.getJSONArray(ARRAYNAME2);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                int orderid = jsonObject1.getInt(ID);
                                boolean OK = updateSync(context, orderid, 1);
                                if (OK == true) {
                                }
                            }
                        }
                    }

				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
	 public static boolean updateSync(Context context, int id, int flag)
	   {
		   ContentResolver cr = context.getContentResolver();
			ContentValues values = new ContentValues() ; 
			values.put(SYNCSTATUS, flag);
			// read more on contenturis and update 
			String where =  ID+ " = "+ id ;
			if(cr.update(BASEURI, values, where, null) == 1)
			{
				return true ;
			}   else {
				return false ;
			}
	   }
    public static int updateSyncFromCustomer(Context context, String[] vals)
	   {
           ContentResolver cr = context.getContentResolver();
           String where = LOCALCUSOMER + " = " + vals[1] ;

           Cursor c = cr.query(BASEURI, null, where,null,null) ;
           try {
               int i= 0 ; int j=0;
               if(c.moveToFirst())
               {
                   i = c.getInt(c.getColumnIndexOrThrow(SYNCSTATUS));
                   j = c.getInt(c.getColumnIndexOrThrow(ID));
                   if(i == 2){
                       ContentValues values = new ContentValues() ;
                       String whereas = ID + " = "+ j;
                       values.put(SYNCSTATUS, 0);
                       values.put(CUSTOMERID, vals[0]);
                       int update = cr.update(BASEURI,values,whereas,null);
                       if(update > 0)
                       {
                           OrderItems.updateSync(context,j,0);//update orderitem sync status to 0 to enable it to sync
                       }
                   }
               }
           } finally {
               if(c != null) {
                   c.close();
               }
           }


			return 0;
	   }

    public static int isOrderSynced(Context context, String businessnm)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm ;

        Cursor c = cr.query(BASEURI, new String[] {SYNCSTATUS}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(SYNCSTATUS));
                return i ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int getPaymodId(Context context, String orderid) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = "+ orderid;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try {
            if(cursor.getCount() >0)
            {
                cursor.moveToFirst();
                return cursor.getInt(cursor.getColumnIndex(ISPRINTED));

            }
            else
            {
                cursor.close();
                return 0;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
    }
	 
	 public static int getId(Context context) {
			ContentResolver cr = context.getContentResolver(); 
	    	Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
	    	try {
	    	int i= 0 ;
	    	if(c.moveToLast())
	    	{
	    		i = c.getInt(c.getColumnIndexOrThrow(ID));
	    		 return i ; 
	    	}
	    	}finally {
				 if(c != null) {
			            c.close();
			        }
			 }
			return 0 ;
		}
	 public static double getDailySummary(Context context, int flag){
		 ContentResolver cr = context.getContentResolver() ;
         String where = null ; 
		 switch(flag)
		 {
		 case 0:
			  where = ORDERDATE + " >=  date('now')"; 
			  
				 break;
		 case 1:  
			  where = ORDERDATE + " >= date('now','-7 day')"; 
			  //date('now','start of month','+0 month','0 day') AND "+ ORDERDATE + " <= date('now','start of month','+0 month','-1 day')"; 
				 break;
		 case 2:
			  where = ORDERDATE + " >=  date('now','start of month','-1 month') AND "+
		 ORDERDATE + " < date('now','start of month','-1 day')"; 
			  //date('now','start of month','+1 month','-1 day')
				 break;
		 }
	        Cursor c = cr.query(BASEURI, null, where, null, null);
			double total = 0;
			try{
				if(c.moveToFirst()){
					do{
						total += c.getDouble(c.getColumnIndexOrThrow(PAYMENTAMOUNT));
					} while (c.moveToNext());
				    return total ;
				}
			}finally{
				if(c != null) {
		            c.close();
		        }
			}
			return 0;  
	 }
    public static int putPrintStatus(Context context, int[] data)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ISPRINTED, data[1]);

        String where =  ID+ " = "+ data[0] ;
        if(cr.update(BASEURI, values, where, null) > 0)
        {
            return 1 ;
        }
        return 0;
    }

    public static double getGrandTotal(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {TOTALSALES}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(TOTALSALES));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }
    public static double getTaxAmount(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {TOTALTAX}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(TOTALTAX));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static int getPrintStatus(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {ISPRINTED}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ISPRINTED));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

   /* public static ArrayList<OrderItemList> getItemsArrayList(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        ArrayList<OrderItemList> custdetails = new ArrayList<OrderItemList>();
        try {
            if(c.moveToFirst())
            {
                int count = 1;
                do {
                    count +=1 ;
                    int productid = c.getInt(c.getColumnIndexOrThrow(PRODUCTNO));
                    int quantity = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    double sprice = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double taxrate = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    String prodname = Stock.getProdName(context,String.valueOf(productid));
                    double grandtotal = Order.getGrandTotal(context, idd);
                    double taxamount = Order.getTaxAmount(context, idd);
                    double grandtotalwithnotax = new BigDecimal(grandtotal).subtract(new BigDecimal(taxamount)).doubleValue();

                    custdetails.add(new OrderItemList(prodname,quantity,grandtotal,
                            String.valueOf(count),sprice,productid,grandtotalwithnotax,taxrate,taxamount));

                } while (c.moveToNext());
                return custdetails;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }*/
	
}
