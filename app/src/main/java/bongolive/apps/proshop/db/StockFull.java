/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import bongolive.apps.proshop.Validating;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.JsonObjectJsonArray;

public class StockFull
{
	public static final String TABLENAME = "stock_full";
	public static final String ID = "_id";
	public static final String PRODUCTID = "product_id";
	public static final String PURCHASEPRICE = "purchase_price" ;//current_stock";
	public static final String STOCKQTY = "stock_qty" ;//current_stock";
	public static final String EXPIRYDATE = "expiry_date" ;//current_stock";
	public static final String BATCH = "batch";
	public static final String CREATED = "create_on" ;
	public static final String UPDATE = "last_update" ;
	public static final String STOCKID = "sys_stock_id" ;//"Sys_stock_id" ;
	public static final String SYNCSTATUS = "sync_status" ;//"Sys_stock_id" ;

	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderEngine.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.bongolive.apps.stock_full" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.bongolive.apps.stock_full" ;
	private static final String TAG = StockFull.class.getName();
	static JsonObjectJsonArray js ;
	public static String ARRAYNAME = "stocksList" ;

	private static JSONArray stockarray ;

	public StockFull() {
		
	}

	public static int deleteStock(Context context, String string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(SYNCSTATUS, "3") ;
		values.put(UPDATE, Constants.getDate());
		String where =  ID + " = " + string ;
		if( cr.update(BASEURI, values, where, null) > 0) {
			Log.v("updated","updated");
			return 1 ;
		}
		return 0;
	}


	private static int isStockPresent(Context context, int custid)
	    {
		 
	    	Cursor c = context.getContentResolver().query(BASEURI, new String[]{STOCKID}, null, null,
                    null) ;
	    	 try {
	    	int i= 0 ;
	    	if(c.moveToFirst())
	    	{
	    		do {
	    			i = c.getInt(c.getColumnIndexOrThrow(STOCKID));
	    			if(i == custid) {
	    				return 1 ;
	    			}
	    		}while(c.moveToNext());
	    	} 
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }

	 private static int isStockPresentLocal(Context context, String custid)
	    {

	    	Cursor c = context.getContentResolver().query(BASEURI, null, null, null, null) ;
	    	 try {

	    	if(c.moveToFirst())
	    	{
	    		return 1;
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }

	public static int getQuantity(Context context, int stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{STOCKQTY}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(STOCKQTY));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}

	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static int updateProduct(Context context, String[] string, int ref)
	{
		ContentResolver cr = context.getContentResolver(); 
		ContentValues values = new ContentValues() ;
		
		values.put(PRODUCTID, string[0]);
		values.put(STOCKQTY, string[1]);
		values.put(BATCH, string[2]) ;
		values.put(EXPIRYDATE, string[3]) ;
		values.put(PURCHASEPRICE, string[4]) ;
		values.put(UPDATE, Constants.getDate());
		
		String where = STOCKID + " = "+ ref ;
		if(cr.update(BASEURI, values, where, null) > 0) {
			return 1 ; 
		} 
		return 0;
	}/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static int updateProductLocal(Context context, String[] string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;

		values.put(STOCKQTY, string[0]);
		values.put(EXPIRYDATE, string[2]) ;
		values.put(PURCHASEPRICE, string[1]) ;
		values.put(UPDATE, Constants.getDate());

		String where = ID + " = "+ string[3] ;
		if(cr.update(BASEURI, values, where, null) > 0) {
			int itemonhand = Products.getItemOnHand(context, string[3]);
			int newitenhand = Integer.parseInt(string[0]);
			int updatingqty =  new BigDecimal(newitenhand).subtract(new BigDecimal(itemonhand))
					.intValue();
			Log.v("itemonhandupdating"," itemonhand "+itemonhand + " newitemonhand "+newitenhand+
					" updatingitemonhand "+updatingqty);
			//prodid,qty,action(0 add 1 deduct

				int[] vals = {Integer.parseInt(string[3]),updatingqty,0};
				Products.updateProductQty(context,vals);

			return 1 ;
		}
		return 0;
	}
	
	/*
	 * Save a new product in the stock table
	 */
	public static int insertStock(Context context, String[] string)
	{
		int productcount = getStockCount(context) ;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;

        values.put(PRODUCTID, string[0]);//prodid,stockqt,batch,stockid
        values.put(STOCKQTY, string[1]);
        values.put(BATCH, string[2]) ;
        values.put(STOCKID, string[3]);
		values.put(EXPIRYDATE, string[4]);
        values.put(CREATED, Constants.getDate());

		cr.insert(BASEURI, values);  
		if(getStockCount(context) == productcount+1){
			return 1 ;
		} else {
			return 0 ;
		}  
	}

	/*
	 * Save a new product in the stock table
	 */
	public static int insertStockLocal(Context context, String[] string)
	{
		int productcount = getStockCount(context) ;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;

        values.put(PRODUCTID, string[0]);
        values.put(STOCKQTY, string[1]);
        values.put(BATCH, string[2]) ;
		values.put(PURCHASEPRICE, string[3]);//prodid,stockqt,batch,price,expiry
		values.put(EXPIRYDATE, string[4]);
        values.put(CREATED, Constants.getDate());

		cr.insert(BASEURI, values);
		if(getStockCount(context) == productcount+1){
			return 1 ;
		} else {
			return 0 ;
		}
	}
	/*
	 * How many items are in the table ?
	 */
	public static int getStockCount(Context context){
		ContentResolver cr = context.getContentResolver();
		String where = SYNCSTATUS + " != 3";
		Cursor _c = cr.query(BASEURI, null, where, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}
	
	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
	public static JSONObject getStockJson(Context context) throws JSONException
	{
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSTOCKONCE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        JsonObjectJsonArray js = new JsonObjectJsonArray();
		String job = js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
	}

	/*
	 * Acknowledgement to the server to tick item as a go
	 */
	public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, STOCKID);
        jobs.put(STOCKID, requiredid[1]);
        JsonObjectJsonArray js = new JsonObjectJsonArray();
        js.getJsonArray(Constants.SERVER, JsonObjectJsonArray.POST, jobs);
	}

	public static void saveStock(Context context) throws JSONException
	{
		JSONObject json = getStockJson(context) ;
		try { 
			if(json.has(Constants.SUCCESS)){
			 String res = json.getString(Constants.SUCCESS) ;
			 if(Integer.parseInt(res) == 1)
			   {
                   if(json.has(ARRAYNAME)) {
                       stockarray = json.getJSONArray(ARRAYNAME) ;
                       for (int i = 0; i < stockarray.length(); i++) {
                           JSONObject js = stockarray.getJSONObject(i);
                           String put = js.getString(BATCH);
                           String pdn = js.getString(STOCKQTY);
                           String stk = js.getString(STOCKID);
                           String ref = js.getString(PRODUCTID);
                           String[] stockvalue = new String[]{ put, pdn, stk, ref};

                           if (Validating.areSet(stockvalue)) {
                               String[] reqired = new String[]{stk, stk};
                               switch (isStockPresent(context, Integer.parseInt(stk))) {
                                   case 0:
                                       //insert a new one
                                       if (insertStock(context, stockvalue) == 1) ;
                                       sendAck(context, reqired);
                                       break;
                                   case 1:
                                       //update the stock is present
                                       if (updateProduct(context, stockvalue, Integer.parseInt(stk)
                                       ) == 1);
                                       sendAck(context, reqired);
                                       break;
                               }
                           }
                       }
                   }
		       }
			 }
				 
		 } catch (JSONException e) { 
						e.printStackTrace();
	 }
	} 
	
	public static String[] getAllStock(Context context)
    {
        ContentResolver cr = context.getContentResolver();

		String where = SYNCSTATUS + " != 3";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTID));
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
	
	public static int getAllStockQuantity(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTID + " = " + prodid  ;
        Cursor cursor = cr.query(BASEURI, new String[] {STOCKQTY}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(STOCKQTY));
        		return quantity;
        	} while (cursor.moveToNext());
        } 
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
	

	
	public static int getStockid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {STOCKID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(STOCKID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }
	public static int getStockLocalid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }

	
	public static String[] getExpiry(Context context)
    {
        ContentResolver cr = context.getContentResolver();

		String where = SYNCSTATUS + " != 3";
        where = STOCKQTY +" < 1" ;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTID));
                 
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }

    public static String[] getStockItem(Context context, long i) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {
                String n = c.getString(c.getColumnIndexOrThrow(PRODUCTID));
                String b = c.getString(c.getColumnIndexOrThrow(PURCHASEPRICE));
                String e = c.getString(c.getColumnIndexOrThrow(STOCKQTY));
                String p = c.getString(c.getColumnIndexOrThrow(EXPIRYDATE));

				String prodname = Products.getProdName(context,n);
                custdetails = new String[]{prodname,b,e,p};

                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }
}