/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

@SuppressWarnings("unused")
public class DatabaseHandler extends SQLiteOpenHelper
{
	private static final String DBNAME = "prosales.db" ;
	private static final int DBVERSION =  1  ;
	private static DatabaseHandler mInstance = null;
	Context _context ;
	
	public static DatabaseHandler getInstance(Context ctx) { 
	    if (mInstance == null) {
	      mInstance = new DatabaseHandler(ctx.getApplicationContext());
	    }
	    return mInstance;
	  }  
	
	private DatabaseHandler(Context context) {  
		super(context, DBNAME, null, DBVERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	 String CUSTOMERS = "CREATE TABLE "+Customers.TABLENAME + " ("+
			 Customers.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
			 Customers.CUSTOMERNAME + " VARCHAR(50) ," +
			 Customers.BUZINESSNAME + " VARCHAR(50) ," +
			 Customers.REFERENCE + " INTEGER ," + 
			 Customers.AREA + " VARCHAR(50) ," +
			 Customers.CITY + " VARCHAR(50) ,"+
			 Customers.MOBILE +" VARCHAR(20) ,"+
			 Customers.EMAIL + " VARCHAR(50) ,"+
			 Customers.LAT + " DOUBLE ,"+
			 Customers.LONGTUDE + " DOUBLE ,"+
			 Customers.SYNCSTATUS + " INTEGER DEFAULT 0,"+
			 Customers.STREET + " VARCHAR(50) )"; 

     String STOCKS = "CREATE TABLE "+Stock.TABLENAME + " ("+
			 Stock.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		 Stock.PRODUCTNAME + " VARCHAR(50) ,"+
			 Stock.STOCK + " INTEGER ,"+
    		 Stock.PACKAGETYPE + " VARCHAR(50) ,"+
			 Stock.PACKAGEUNIT + " INTEGER ,"+
    		 Stock.UNITPRICE + " DOUBLE ," +
			 Stock.TAXRATE + " DOUBLE ," +
    		 Stock.UPDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
			 Stock.STOCKID + " INTEGER ," +
             Stock.UNITMEASURE +" VARCHAR(50) , "+
             Stock.UNITSIZE + " DOUBLE, "+
             Stock.SKU + " VARCHAR(100), "+
			 Stock.REFERENCE + " INTEGER )";

     String _PRODUCT = "CREATE TABLE "+Products.TABLENAME + " ("+
			 Products.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		 Products.PRODUCTNAME + " VARCHAR(50) NOT NULL UNIQUE ,"+
    		 Products.PACKAGETYPE + " VARCHAR(50) NULL ,"+
			 Products.PACKAGEUNITS + " INTEGER NULL, "+
			 Products.CONTINER + " VARCHAR(30) NULL ,"+
			 Products.ITEMS_ON_HAND + " INTEGER(11) NOT NULL DEFAULT 0,"+
			 Products.REODER + " INTEGER(11) NOT NULL DEFAULT 0,"+
			 Products.PRODUCT_TYPE + " INTEGER(11) NOT NULL DEFAULT 0,"+
    		 Products.UNITPRICE + " DOUBLE NOT NULL," +
			 Products.TAXRATE + " DOUBLE NOT NULL DEFAULT 0.18, " +
    		 Products.UPDATE + " DATETIME NULL ,"+
    		 Products.CREATED + " DATETIME NOT NULL ,"+
			 Products.REFERENCE + " INTEGER  ,"+
             Products.CATEGORY +" VARCHAR(50) , "+
			 Products.SYNCSTATUS + " INTEGER DEFAULT 0,"+
             Products.DESCRIPTION +" VARCHAR(50) , "+
             Products.SKU +" VARCHAR(50) , "+
             Products.UNITMEASURE +" VARCHAR(50) , "+
             Products.UNITSIZE + " DOUBLE, "+
			 Products.PRODSTATUS + " INTEGER NOT NULL DEFAULT 1 )";
     
     String ORDERS = "CREATE TABLE "+ Order.TABLENAME+" ("+
		     Order.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		 Order.CUSTOMERID + " INTEGER ," +
		     Order.ORDERDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
    		 Order.PAYMENTSTATUS + " VARCHAR(20) ,"+
		     Order.PAYMENTMOD + " VARCHAR(20) ," +
    		 Order.PAYMENTAMOUNT + " DOUBLE ," +
		     Order.TOTALSALES + " DOUBLE ," +
    		 Order.TOTALTAX + " DOUBLE ," +
		     Order.RECEIPT + " INTEGER ," +
             Order.LAT + " DOUBLE ,"+
             Order.LONGTUDE + " DOUBLE ,"+
             Order.ISPRINTED + " INTEGER DEFAULT 0 ," +
             Order.LOCALCUSOMER + " INTEGER ," +
    		 Order.SYNCSTATUS + " INTEGER DEFAULT 0 )";
     
     String ORDERITEM = "CREATE TABLE "+OrderItems.TABLENAME  + " ("+
				OrderItems.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                OrderItems.PRODUCTNO + " INTEGER ," +
				OrderItems.PRODUCTQTY + " INTEGER ," +
                OrderItems.SALEPRICE + " DOUBLE ," +
				OrderItems.TAXRATE + " DOUBLE ," +
                OrderItems.ORDERID + " INTEGER ," +
				OrderItems.SYNCSTATUS + " INTEGER DEFAULT 0 )";
     
     String PAYMENTMODE = "CREATE TABLE "+PaymentMode.TABLENAME + " (" +
				PaymentMode.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		    PaymentMode.MODETITLE + " VARCHAR ," +
				PaymentMode.SYSTEMMODE + " INTEGER ," +
    		    PaymentMode.SYMBOL + " VARCHAR )";

     String TRACKING = "CREATE TABLE "+Tracking.TABLENAME + " (" +
             Tracking.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Tracking.LAT + " DOUBLE ," +
             Tracking.LNG + " DOUBLE ," +
             Tracking.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 , "+
             Tracking.DATE + " VARCHAR )";

     String _STOCKFULL = "CREATE TABLE "+StockFull.TABLENAME + " (" +
			 StockFull.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			 StockFull.PRODUCTID + " INT ," +
			 StockFull.STOCKID + " INT ," +
			 StockFull.PURCHASEPRICE + " DOUBLE NOT NULL DEFAULT 0 , "+
			 StockFull.STOCKQTY + " INTEGER NOT NULL DEFAULT 0 , "+
			 StockFull.CREATED + " DATETIME NOT NULL , "+
			 StockFull.EXPIRYDATE + " DATETIME , "+
			 StockFull.UPDATE + " DATETIME , "+
			 StockFull.SYNCSTATUS + " INTEGER NOT NULL DEFAULT 0 , "+
			 StockFull.BATCH + " VARCHAR(70) )";

     String SETTINGS = "CREATE TABLE " + Settings.TABLENAME + " (" +
    		  Settings.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  Settings.PRINTRECEIPT + " INTEGER DEFAULT 0 ," +
    		  Settings.EDITSALES + " INTEGER DEFAULT 0 ," +
    		  Settings.DISCOUNT + " INTEGER DEFAULT 0 ," +
    		  Settings.DISCOUNT_LIMIT + " INTEGER DEFAULT 0 ," +
    		  Settings.VAT + " INTEGER DEFAULT 0 ," +  
    		  Settings.FIXEDPRICE + " INTEGER DEFAULT 0 ," +  
    		  Settings.CURRENCY + " INTEGER DEFAULT 0 ," +
              Settings.TRACKTIME + " INTEGER NOT NULL DEFAULT 0, "+
    		  Settings.TAXINCLUSIVE + " INTEGER DEFAULT 0 )";

     String MULTM = "CREATE TABLE " + MultimediaContents.TABLENAME + " (" +
    		  MultimediaContents.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  MultimediaContents.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 ," +
    		  MultimediaContents.NAME + " VARCHAR(100) NOT NULL ," +
    		  MultimediaContents.SIZE + " VARCHAR(30) NOT NULL ," +
    		  MultimediaContents.TYPE + " VARCHAR(25) NOT NULL ," +
    		  MultimediaContents.CONTENT + " VARCHAR(150) NOT NULL," +
    		  MultimediaContents.DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";

     db.execSQL(CUSTOMERS);
     db.execSQL(ORDERS);
     db.execSQL(ORDERITEM);
     db.execSQL(STOCKS); 
     db.execSQL(PAYMENTMODE);
     db.execSQL(SETTINGS);
     db.execSQL(TRACKING);
     db.execSQL(MULTM);
     db.execSQL(_PRODUCT);
     db.execSQL(_STOCKFULL);

     System.out.println("database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("LOG_TAG", "upgrading database from version " 
	+oldVersion + "to version "+newVersion +"all data will be dropped") ; 
		 
			String ORDER = " DROP TABLE IF EXISTS "+Order.TABLENAME ;
			String ORDERITEM = " DROP TABLE IF EXISTS "+ OrderItems.TABLENAME ;
			String CUSTOMER = " DROP TABLE IF EXISTS " + Customers.TABLENAME ;
			String SETTING = " DROP TABLE IF EXISTS " + Settings.TABLENAME ;
			String STOCK = " DROP TABLE IF EXISTS " + PaymentMode.TABLENAME ;
			String PAYMEODES = " DROP TABLE IF EXISTS " + PaymentMode.TABLENAME ; 
			String M = " DROP TABLE IF EXISTS " + MultimediaContents.TABLENAME ;
			String TR = " DROP TABLE IF EXISTS " + Tracking.TABLENAME ;
			db.execSQL(ORDERITEM);
			db.execSQL(ORDER);
			db.execSQL(CUSTOMER);
			db.execSQL(STOCK);
			db.execSQL(SETTING);
			db.execSQL(PAYMEODES);
			db.execSQL(M);
			db.execSQL(TR);
			onCreate(db);
	} 
	
 }
