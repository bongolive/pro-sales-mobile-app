/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package bongolive.apps.proshop.db;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nasznjoka
 *
 * Oct 16, 2014
 */
public class Lists {
	private static Context mcontext ;
	   public Lists(Context context) {
		Lists.mcontext = context ;
	   }
   
   // get unsynced orders
   public  List<Order> getAllUnsyncedOrders()  {
		List<Order> orderlist = new ArrayList<Order>() ;
		ContentResolver cr = mcontext.getContentResolver();
		String where = Order.SYNCSTATUS + " = 0" ;
		Cursor c = cr.query(Order.BASEURI, null, where, null, null);
		if(c.moveToFirst())
		{
			do {
				Order o = new Order();
				o.setCustomer(c.getInt(c.getColumnIndex(Order.CUSTOMERID))) ;
				o.setDate(c.getString(c.getColumnIndex(Order.ORDERDATE))) ;
				o.setPaystatus(c.getString(c.getColumnIndex(Order.PAYMENTSTATUS)));
				o.setPaymntmod(c.getInt(c.getColumnIndex(Order.PAYMENTMOD)));
				o.setPay(c.getDouble(c.getColumnIndex(Order.PAYMENTAMOUNT))) ;
				o.setReceipt(c.getString(c.getColumnIndex(Order.RECEIPT)));
				o.setTotalsales(c.getDouble(c.getColumnIndex(Order.TOTALSALES))) ;
				o.setTotaltax(c.getDouble(c.getColumnIndex(Order.TOTALTAX))) ;
				o.setSync(c.getInt(c.getColumnIndex(Order.SYNCSTATUS)));
				orderlist.add(o);
			} while (c.moveToNext()) ;
		}
		c.close();
		return orderlist;
		
	} ;
	
	 // get unsynced orders
	   public  List<Order> getOrdersList()  {
			List<Order> orderlist = new ArrayList<Order>() ;
			ContentResolver cr = mcontext.getContentResolver(); 
			Cursor c = cr.query(Order.BASEURI, null, null, null, null);
			if(c.moveToFirst())
			{
				do {
					Order o = new Order();
					o.setCustomer(c.getInt(c.getColumnIndex(Order.CUSTOMERID))) ;
					o.setDate(c.getString(c.getColumnIndex(Order.ORDERDATE))) ;
					o.setPaystatus(c.getString(c.getColumnIndex(Order.PAYMENTSTATUS)));
					o.setPaymntmod(c.getInt(c.getColumnIndex(Order.PAYMENTMOD)));
					o.setPay(c.getDouble(c.getColumnIndex(Order.PAYMENTAMOUNT))) ;
					o.setReceipt(c.getString(c.getColumnIndex(Order.RECEIPT)));
					o.setTotalsales(c.getDouble(c.getColumnIndex(Order.TOTALSALES))) ;
					o.setTotaltax(c.getDouble(c.getColumnIndex(Order.TOTALTAX))) ;
					o.setSync(c.getInt(c.getColumnIndex(Order.SYNCSTATUS)));
					orderlist.add(o);
				} while (c.moveToNext()) ;
			}
			c.close();
			return orderlist;
			
		} ;
	
	//get unsynced order items
	public List<OrderItems> getUnsyncedItems(){
		List<OrderItems> listItems = new ArrayList<OrderItems>() ;
		ContentResolver cr = mcontext.getContentResolver();
		String where = OrderItems.SYNCSTATUS + " = 0" ;
		Cursor c = cr.query(OrderItems.BASEURI, null, where, null, null);
		if(c.moveToFirst())
		{
			do {
				OrderItems i = new OrderItems();
				i.setProductno(c.getInt(c.getColumnIndexOrThrow(OrderItems.PRODUCTNO))) ;
				i.setProdquantity(c.getInt(c.getColumnIndexOrThrow(OrderItems.PRODUCTQTY))) ;
				i.setSaleprice(c.getDouble(c.getColumnIndexOrThrow(OrderItems.SALEPRICE))) ;
				i.setTaxrate(c.getDouble(c.getColumnIndexOrThrow(OrderItems.TAXRATE)));
				i.setOrderid(c.getInt(c.getColumnIndexOrThrow(OrderItems.ORDERID))) ;
				i.setSyncstatus(c.getInt(c.getColumnIndexOrThrow(OrderItems.SYNCSTATUS)));
				listItems.add(i);
			} while (c.moveToNext()) ;
		}
		c.close();
		return listItems;
	}
	
	public List<OrderItems> getItemsList(){
		List<OrderItems> listItems = new ArrayList<OrderItems>() ;
		ContentResolver cr = mcontext.getContentResolver(); 
		Cursor c = cr.query(OrderItems.BASEURI, null, null, null, null);
		if(c.moveToFirst())
		{
			do {
				OrderItems i = new OrderItems();
				i.setProductno(c.getInt(c.getColumnIndexOrThrow(OrderItems.PRODUCTNO))) ;
				i.setProdquantity(c.getInt(c.getColumnIndexOrThrow(OrderItems.PRODUCTQTY))) ;
				i.setSaleprice(c.getDouble(c.getColumnIndexOrThrow(OrderItems.SALEPRICE))) ;
				i.setTaxrate(c.getDouble(c.getColumnIndexOrThrow(OrderItems.TAXRATE)));
				i.setOrderid(c.getInt(c.getColumnIndexOrThrow(OrderItems.ORDERID))) ;
				i.setSyncstatus(c.getInt(c.getColumnIndexOrThrow(OrderItems.SYNCSTATUS)));
				listItems.add(i);
			} while (c.moveToNext()) ;
		}
		c.close();
		return listItems;
	}
 

	public List<Customers> getCustomersList(){
		List<Customers> customerList = new ArrayList<Customers>();
		ContentResolver cr = mcontext.getContentResolver();
		Cursor _c = cr.query(Customers.BASEURI, null, null, null, null);
		if(_c.moveToFirst())
		{
			do{
				Customers c = new Customers();
				c.setId(_c.getInt(_c.getColumnIndexOrThrow(Customers.ID)));
				c.setCname(_c.getString(_c.getColumnIndexOrThrow(Customers.CUSTOMERNAME)));
				c.setBname(_c.getString(_c.getColumnIndexOrThrow(Customers.BUZINESSNAME)));
				c.setId(_c.getInt(_c.getColumnIndexOrThrow(Customers.REFERENCE)));
				c.setArea(_c.getString(_c.getColumnIndexOrThrow(Customers.AREA))) ;
				c.setCity(_c.getString(_c.getColumnIndexOrThrow(Customers.CITY))); 
				c.setMob(_c.getString(_c.getColumnIndexOrThrow(Customers.MOBILE)));
				c.setEmail(_c.getString(_c.getColumnIndexOrThrow(Customers.EMAIL)));
				c.setLat(_c.getString(_c.getColumnIndexOrThrow(Customers.LAT)));
				c.setLng(_c.getString(_c.getColumnIndexOrThrow(Customers.LONGTUDE)));
				c.setSync(_c.getInt(_c.getColumnIndexOrThrow(Customers.SYNCSTATUS)));
				c.setStreet(_c.getString(_c.getColumnIndexOrThrow(Customers.STREET)));
			}while(_c.moveToNext());
		}
		_c.close();
		return customerList;
	}
	public List<Customers> getUnsyncedCustomersList(Context context){
		List<Customers> customerList = new ArrayList<Customers>();
		ContentResolver cr = context.getContentResolver();
		String where = Customers.REFERENCE + " = 0";
		Cursor _c = cr.query(Customers.BASEURI, null, where, null, null);
		if(_c.moveToFirst())
		{
			do{
				Customers c = new Customers(); 
				c.setCname(_c.getString(_c.getColumnIndexOrThrow(Customers.CUSTOMERNAME))); 
				c.setBname(_c.getString(_c.getColumnIndexOrThrow(Customers.BUZINESSNAME))); 
				c.setArea(_c.getString(_c.getColumnIndexOrThrow(Customers.AREA))) ;
				c.setCity(_c.getString(_c.getColumnIndexOrThrow(Customers.CITY))); 
				c.setMob(_c.getString(_c.getColumnIndexOrThrow(Customers.MOBILE)));
				c.setEmail(_c.getString(_c.getColumnIndexOrThrow(Customers.EMAIL)));  
				c.setStreet(_c.getString(_c.getColumnIndexOrThrow(Customers.STREET)));
			}while(_c.moveToNext());
		}
		_c.close();
		return customerList;
	}

	public List<Customers> getCustomerDetailsList(Context context, long id){
		List<Customers> customerList = new ArrayList<Customers>();
		ContentResolver cr = context.getContentResolver();
		String where = Customers.ID + " = " + id;
		Cursor _c = cr.query(Customers.BASEURI, null, where, null, null);
		if(_c.moveToFirst())
		{
				Customers c = new Customers(); 
				c.setCname(_c.getString(_c.getColumnIndexOrThrow(Customers.CUSTOMERNAME))); 
				c.setBname(_c.getString(_c.getColumnIndexOrThrow(Customers.BUZINESSNAME))); 
				c.setArea(_c.getString(_c.getColumnIndexOrThrow(Customers.AREA))) ;
				c.setCity(_c.getString(_c.getColumnIndexOrThrow(Customers.CITY))); 
				c.setMob(_c.getString(_c.getColumnIndexOrThrow(Customers.MOBILE)));
				c.setEmail(_c.getString(_c.getColumnIndexOrThrow(Customers.EMAIL)));  
				c.setStreet(_c.getString(_c.getColumnIndexOrThrow(Customers.STREET))); 
		}
		_c.close();
		return customerList;
	}
	
}
