/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class FiscalPrinter extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";
    ListView lv ;
	ArrayAdapter<String> adapter ;
	ArrayList<String> arraylist ;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
       return inflater.inflate(R.layout.paydebts, container, false) ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));    

    }   
}
