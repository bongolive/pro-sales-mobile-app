/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ItemListAdapter extends BaseAdapter  {
    int action;
    private LayoutInflater mInflater;
    private List<ItemList> mItems = new ArrayList<ItemList>();

    public ItemListAdapter() {

    }

    public ItemListAdapter(Context context, List<ItemList> items, int source) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        action = source;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ItemViewHolder();
            holder.txtName = (TextView) convertView.findViewById(R.id.txtitem);
            holder.txtName1 = (TextView) convertView.findViewById(R.id.txtitem1);
            holder.txtName2 = (TextView) convertView.findViewById(R.id.txtitem2);
            holder.txtName3 = (TextView) convertView.findViewById(R.id.txtitemextra);
            holder.txtName4 = (TextView) convertView.findViewById(R.id.txtitem4);
            holder.chkdelete = (CheckBox) convertView.findViewById(R.id.chkdrop);
            holder.imgedit = (ImageView) convertView.findViewById(R.id.imgedit);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        ItemList cl = mItems.get(position);

        holder.txtName.setText(cl.getitemname());
        holder.txtName1.setText(cl.getitemname1());
        holder.txtName2.setText(cl.getitemname2());
        String item3 = cl.getitemname3();
        if (Validating.areSet(item3)) {
            holder.txtName3.setVisibility(View.VISIBLE);
            holder.txtName3.setText(item3);
        }
        if (cl.getId() < 1) {
            holder.chkdelete.setVisibility(View.GONE);
            holder.imgedit.setVisibility(View.GONE);
        }
        if (action == 0) {
            holder.imgedit.setVisibility(View.GONE);
        }
//		if(!cl.getitemname4().isEmpty()){
        holder.txtName4.setVisibility(View.VISIBLE);
        holder.txtName4.setText(cl.getitemname4());
//		}

        long id = cl.getId();


        return convertView;
    }

    static class ItemViewHolder {
        TextView txtName;
        TextView txtName1;
        TextView txtName2;
        TextView txtName3;
        TextView txtName4;
        CheckBox chkdelete;
        ImageView imgedit;
    }

}
