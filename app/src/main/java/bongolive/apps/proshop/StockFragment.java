/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.app.Dialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.DatabaseHandler;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.Stock;

public class StockFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static int ADDSTOCK = 1;
    AlertDialogManager alert;
    View _rootView;
    DatabaseHandler dh;
    ListView lv;
    Dialog dialog;
    ItemListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_items, container, false);
        } else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        lv = (ListView) getActivity().findViewById(R.id.list);

        adapter = new ItemListAdapter(getActivity(), getList(), 4);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object ob = (Object) lv.getItemAtPosition(position);
                ItemList item = (ItemList) ob;
                long i = item.getId();
                String prodname = item.getitemname();
                showProduct(i, prodname);
            }
        });
        setHasOptionsMenu(true);
    }

    private void showProduct(long i, String prodname) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.products, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        TextView tclname, tshname, tphone, temail, taddress, sku;

        tclname = (TextView) dialog.findViewById(R.id.txtclientname);
        tshname = (TextView) dialog.findViewById(R.id.txtshopname);
        temail = (TextView) dialog.findViewById(R.id.txtclientemail);
        tphone = (TextView) dialog.findViewById(R.id.txtclientphone);
        taddress = (TextView) dialog.findViewById(R.id.txtclientshopaddress);
        sku = (TextView) dialog.findViewById(R.id.txtsku);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        String[] details = Stock.getProducts(getActivity(), i);
        //proname,price,measu,size,tax,sku,stoc
        tclname.setText(dialog.getContext().getString(R.string.strproductname) + " : " + details[0]);
        tshname.setText(dialog.getContext().getString(R.string.strprice) + " : " + details[1]);
        temail.setText(dialog.getContext().getString(R.string.strtaxrate) + " : " + details[4]);
        tphone.setText(dialog.getContext().getString(R.string.strunitsize) + " : " + details[3] + " " + details[2]);
        taddress.setText(dialog.getContext().getString(R.string.strquantity) + " : " + details[6]);
        sku.setText(dialog.getContext().getString(R.string.strsku) + " : " + details[5]);

        dialog.show();
    }


    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = cr.query(Stock.BASEURI, null, null, null, Stock.STOCK + " DESC");
        List<ItemList> list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Stock.PRODUCTNAME);
                    int colP = c.getColumnIndex(Stock.STOCK);
                    int colI = c.getColumnIndex(Stock.ID);
                    int colD = c.getColumnIndex(Stock.UPDATE);
                    int colPr = c.getColumnIndex(Stock.UNITPRICE);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    long i = c.getLong(colI);
                    String d = c.getString(colD);
                    String pr = c.getString(colPr);
                    list.add(new ItemList(getString(R.string.strproduct).toUpperCase(Locale.getDefault()) + ": " +
                            n.toUpperCase(Locale.getDefault()),
                            getString(R.string.strprice).toUpperCase(Locale.getDefault()) + ": " +
                                    pr.toUpperCase(Locale.getDefault()),
                            getString(R.string.strquantity).toUpperCase(Locale.getDefault()) + ": " + p, i,
                            getString(R.string.strhistory).toUpperCase(Locale.getDefault()) + ": " + d));
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, "", p, i, d));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    public ArrayList<ItemList> getListOrderItem(long i) {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Stock.ID + " = " + i;
        Cursor c = cr.query(Order.BASEURI, null, where, null, null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if (c.moveToFirst()) {
            do {
                int colN = c.getColumnIndex(Order.TOTALSALES);
                int colP = c.getColumnIndex(Order.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Order.ID);
                int colD = c.getColumnIndex(Order.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI);
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault()) + ": " +
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault()) + ": " + p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault()) + ": " + d));
            } while (c.moveToNext());
        }
        ArrayList<ItemList> itemlist = new ArrayList<>();
        return itemlist;
    }
}
