/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


/**
 * Created by nasznjoka on 2/22/2015.
 */
public class ExportDatabase extends AsyncTask<String, Void, Boolean> {
	Context ctx;
	CheckBox chk;
	public ExportDatabase(Context context, CheckBox checkBoxPreference){
		this.ctx = context;
		this.chk = checkBoxPreference;
	}
//	private final ProgressDialog dialog = new ProgressDialog(ctx);

	// can use UI thread here
	protected void onPreExecute() {/*
		this.dialog.setMessage("Exporting database...");
		this.dialog.show();*/
	}

	// automatically done on worker thread (separate from UI thread)
	protected Boolean doInBackground(final String... args) {

		File dbFile =
				new File(Environment.getDataDirectory() + "/data/com" +
						".bongolive.apps.prostock/databases/prostock.db");

		File sdcard = Environment.getExternalStorageDirectory() ;
		File folder = new File(sdcard.getAbsoluteFile(), "safi_database");

		if (!folder.exists()) {
			folder.mkdirs();
		}

		File file = new File(folder.getAbsoluteFile(), "prostock.db") ;

		try {
			try {

				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.copyFile(dbFile, file);
			return true;
		} catch (IOException e) {
			Log.e("mypck", e.getMessage(), e);
			return false;
		}
	}

	// can use UI thread here
	protected void onPostExecute(final Boolean success) {
		/*if (this.dialog.isShowing()) {
			this.dialog.dismiss();
		}*/
		if (success) {
			AlertDialogManager dialog = new AlertDialogManager();
			dialog.showAlertDialog(ctx, "DATABASE EXPORT","Export successful! \n please locate " +
					"the back up in the memory card within a folder called safi_database " +
					" and a file called "  , false);
			chk.setChecked(false);
		} else {
			Toast.makeText(ctx, "Export failed", Toast.LENGTH_SHORT).show();
			chk.setChecked(false);
		}
	}

	void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

}
