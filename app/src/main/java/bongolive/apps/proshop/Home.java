/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.Stock;


public class Home extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";
	TextView daysummary,weeksummary,monthsummary ;
	SharedPreferences sharedPref;
	ListView lv ;
	ArrayAdapter<String> adapter ;
	ArrayList<String> arraylist ;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
       return inflater.inflate(R.layout.home, container, false) ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));    
        daysummary = (TextView)getActivity().findViewById(R.id.txthome_sales_day);
        weeksummary = (TextView)getActivity().findViewById(R.id.txthome_sales_week);
        monthsummary = (TextView)getActivity().findViewById(R.id.txthome_sales_month);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String currency = sharedPref.getString("default_currency_key", "");
          
        String header = getResources().getString(R.string.strsalestotalday) ;
        String report = String.format("%.02f", Order.getDailySummary(getActivity(), 0)) ;
        daysummary.setText( header+":\n " + report +" "+ currency);
        
        String header1 = getResources().getString(R.string.strsalestotalweek) ;
        String report1 = String.format("%.02f", Order.getDailySummary(getActivity(), 1)) ;
        weeksummary.setText( header1+":\n " + report1 +" "+ currency);
         String header2 = getResources().getString(R.string.strsalestotalmonth) ;
        String report2 = String.format("%.02f", Order.getDailySummary(getActivity(), 2)) ;
        monthsummary.setText( header2+":\n " + report2 +" "+ currency);
        
        lv = (ListView)getActivity().findViewById(R.id.listsoldout);
        String[] array = Stock.getSoldOut(getActivity());
        if(array.length < 1)
        	array = new String[]{getString(R.string.strsoldout)} ;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, array);
        lv.setAdapter(adapter);
    }   
}
