/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeviceActivity extends ActionBarActivity {
	public static final String EXTRA_ADDRESS = "bt_address";

	private ListView mListView;
	private List<Pair<String, String>> mList;
    boolean tabletSize = false;
		
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_device);
        setResult(RESULT_CANCELED);
                
        mList = new ArrayList<Pair<String,String>>();
        mListView = (ListView)findViewById(R.id.listView);        
        mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {
				final Pair<String, String> data = mList.get(position);
				final String bthAddress = data.second;
								
				Intent resultData = new Intent();
				resultData.putExtra(EXTRA_ADDRESS, bthAddress);				
				setResult(RESULT_OK, resultData);
				finish();
			}			
		});
        
        findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				finish();
			}
		});
        
        loadDevices();
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();    	
    }
    
    private void loadDevices() {
    	final BluetoothAdapter bthAdapter = BluetoothAdapter.getDefaultAdapter();
    	if (bthAdapter == null) {
    		return;
    	}

    	final ArrayAdapter<Pair<String, String>> arrayAdapter = new ArrayAdapter<Pair<String,String>>(this, android.R.layout.simple_list_item_2, mList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TwoLineListItem row;
                if(convertView == null){
                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    row = (TwoLineListItem)inflater.inflate(android.R.layout.simple_list_item_2, null);
                } else{
                    row = (TwoLineListItem)convertView;
                }

                final Pair<String, String> data = mList.get(position);
                row.getText1().setText(data.first);
                row.getText2().setText(data.second);
	            return row;
			}
        };
        mListView.setAdapter(arrayAdapter);

    	final Set<BluetoothDevice> paired = bthAdapter.getBondedDevices();
    	for (BluetoothDevice device: paired) {
    		final Pair<String, String> data = new Pair<String, String>(device.getName(), device.getAddress());
    		mList.add(data);    		
    	}
    	
    	arrayAdapter.notifyDataSetChanged();
    }       
}
