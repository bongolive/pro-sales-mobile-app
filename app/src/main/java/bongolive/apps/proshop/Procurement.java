/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.DatabaseHandler;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.Products;
import bongolive.apps.proshop.db.Stock;
import bongolive.apps.proshop.db.StockFull;
import bongolive.apps.proshop.networking.Constants;

public class Procurement extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static int ADDSTOCK = 1;
    AlertDialogManager alert;
    View _rootView;
    DatabaseHandler dh;
    ListView lv;
    Dialog dialog;
    ItemListAdapter adapter;
    List<ItemList> list;
    ArrayList<String> deletearray;
    MenuItem deleteitem,addproductitem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_items, container, false);
        } else {
            ((ViewGroup) _rootView.getParent()).removeView(_rootView);
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        lv = (ListView) getActivity().findViewById(R.id.list);

        adapter = new ItemListAdapter(getActivity(), getList(), 3);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        setHasOptionsMenu(true);
        deletearray = new ArrayList<>();


    }

    private void showProduct(final long i, String prodname, int source) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.view_edit_stock, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final EditText etprodname, etqty, etprice, etexpirydate;
        TextView txttitle;

        etprodname = (EditText) dialog.findViewById(R.id.etstockprodname);
        etqty = (EditText) dialog.findViewById(R.id.etstockqty);
        etprice = (EditText) dialog.findViewById(R.id.etpurchaseprice);
        etexpirydate = (EditText) dialog.findViewById(R.id.etstockexpirydate);
        txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        txttitle.setText(prodname);
        ImageView drop = (ImageView) dialog.findViewById(R.id.btnCancel);
        ImageView edit = (ImageView) dialog.findViewById(R.id.btnSubmit);
        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        String[] v = StockFull.getStockItem(dialog.getContext(), i);
        if (Validating.areSet(v)) {
            etprodname.setText(v[0]);
            etqty.setText(v[2]);
            etprice.setText(v[1]);
            etexpirydate.setText(v[3]);
        }

        if (source == 0) {
            etqty.setFocusableInTouchMode(false);
            etqty.setFocusable(false);
            etprice.setFocusable(false);
            etprice.setFocusableInTouchMode(false);
            etexpirydate.setFocusableInTouchMode(false);
            etexpirydate.setFocusable(false);

            drop.setVisibility(View.GONE);
            edit.setVisibility(View.GONE);

        } else {

            etexpirydate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.setDateTimeField(getActivity(), etexpirydate);
                }
            });

            etexpirydate.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String minvalue = Constants.getYearDateMonth();
                    String maxvalue = "2030-06-30";
                    String chked = etexpirydate.getText().toString();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date entered = df.parse(chked);
                        Date mindate = df.parse(minvalue);
                        Date maxdate = df.parse(maxvalue);
                        Date common = df.parse("0000-00-00");
                        Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
                                maxdate + " entered value is " + entered);
                        if (entered.compareTo(mindate) <= 0) {
                            Toast.makeText(dialog.getContext(), getString(R.string.strmin)
                                            + " " + minvalue,
                                    Toast.LENGTH_LONG).show();
                            etexpirydate.setText("");
                            chked = "";
                            etexpirydate.clearFocus();
                        }
                        if (entered.compareTo(common) != 0) {
                            if (entered.compareTo(maxdate) > 0) {
                                Toast.makeText(dialog.getContext(), getString(R.string.strmax)
                                                + " " + maxvalue,
                                        Toast.LENGTH_LONG).show();
                                etexpirydate.setText("");
                                chked = "";
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = etqty.getText().toString();
                String prc = etprice.getText().toString();
                String expd = etexpirydate.getText().toString();

                String[] vals = {qty, prc, expd, String.valueOf(i)};
                if (Validating.areSet(vals)) {
                    StockFull.updateProductLocal(dialog.getContext(), vals);
                    dialog.dismiss();
                }
            }
        });


        dialog.show();
    }


    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        String where = StockFull.SYNCSTATUS + " != 3";
        Cursor c = cr.query(StockFull.BASEURI, null, where, null, StockFull.CREATED + " DESC");
        list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(StockFull.PRODUCTID);
                    int colP = c.getColumnIndex(StockFull.STOCKQTY);
                    int colI = c.getColumnIndex(StockFull.ID);
                    int colD = c.getColumnIndex(StockFull.BATCH);
                    int colPr = c.getColumnIndex(StockFull.PURCHASEPRICE);
                    int colExp = c.getColumnIndex(StockFull.EXPIRYDATE);
                    String proid = c.getString(colN);
                    String n = Products.getProdName(getActivity(), proid);
                    String p = c.getString(colP);
                    long i = c.getLong(colI);
                    String btch = c.getString(colD);
                    String pr = c.getString(colPr);
                    String expr = c.getString(colExp);
                    list.add(new ItemList(getString(R.string.strproduct).toUpperCase(Locale.getDefault()) + ": " +
                            n.toUpperCase(Locale.getDefault()),
                            getString(R.string.strprice).toUpperCase(Locale.getDefault()) + ": " +
                                    pr.toUpperCase(Locale.getDefault()),
                            getString(R.string.strquantity).toUpperCase(Locale.getDefault()) + ": " + p, i,
                            getString(R.string.strbatch).toUpperCase(Locale.getDefault()) + ": " + btch,
                            getString(R.string.strexpirydt).toUpperCase(Locale.getDefault()) + ": " + expr));
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, "", p, i, d));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    public ArrayList<ItemList> getListOrderItem(long i) {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Stock.ID + " = " + i;
        Cursor c = cr.query(Order.BASEURI, null, where, null, null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if (c.moveToFirst()) {
            do {
                int colN = c.getColumnIndex(Order.TOTALSALES);
                int colP = c.getColumnIndex(Order.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Order.ID);
                int colD = c.getColumnIndex(Order.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI);
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault()) + ": " +
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault()) + ": " + p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault()) + ": " + d));
            } while (c.moveToNext());
        }
        ArrayList<ItemList> itemlist = new ArrayList<>();
        return itemlist;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.addstock_full, menu);
        deleteitem = menu.findItem(R.id.action_delete);
        addproductitem = menu.findItem(R.id.action_addstock_full);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addstock_full:
                int prod = Products.getCount(getActivity());
                AlertDialogManager alertDialogManager = new AlertDialogManager();
                if (prod > 0) {
                    Intent intent = new Intent(getActivity(), StockActivity.class);
                    startActivityForResult(intent, ADDSTOCK);
                    return true;
                } else {
                    alertDialogManager.showAlertDialog(getActivity(), getString(R.string.strerror),
                            getString(R.string.strnoprod), false);
                }
                break;
            case R.id.action_delete:
                if (deletearray.size() > 0) {
                    for (int i = 0; i < deletearray.size(); i++) {
                        StockFull.deleteStock(getActivity(), deletearray.get(i));
                    }

                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                    deleteitem.setVisible(false);
                    addproductitem.setVisible(true);
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
        LinearLayout ll = ((LinearLayout) view.findViewById(R.id.itemlayout));
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemList _list = list.get(position);
                String name = _list.getitemname();
                long i = _list.getId();
                if (i > 0)
                    showProduct(i, name, 0);
            }
        });
        ImageView imgedit = ((ImageView) view.findViewById(R.id.imgedit));
        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemList _list = list.get(position);
                String name = _list.getitemname();
                long i = _list.getId();

                Uri uri = Uri.withAppendedPath(Customers.BASEURI, String.valueOf(id));
                if (i > 0)
                    showProduct(i, name, 1);
            }
        });
        final CheckBox chkdrop = ((CheckBox) view.findViewById(R.id.chkdrop));

        ItemList _list = list.get(position);
        final long i = _list.getId();

        chkdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i > 0)
                    if (chkdrop.isChecked()) {
                        deleteitem.setVisible(true);
                        addproductitem.setVisible(false);
                        deletearray.add(String.valueOf(i));
                    } else {
                        deletearray.remove(String.valueOf(i));
                    }
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        getActivity();
        switch (reqCode) {
            case 1:
                if (resCode == Activity.RESULT_OK) {
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 3);
                    lv.setAdapter(adapter);
                    Toast.makeText(getActivity(), getString(R.string.strstockadded), Toast
                            .LENGTH_LONG)
                            .show();
                } else if (resCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(getActivity(), getString(R.string.strstockcancl), Toast
                            .LENGTH_LONG).show();
                }
                break;
        }
    }

}
