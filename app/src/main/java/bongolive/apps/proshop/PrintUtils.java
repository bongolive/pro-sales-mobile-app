/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.fiscalprinter.tza.FMP10TZA;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bongolive.apps.proshop.networking.Constants;

public class PrintUtils extends Activity {
	private static final int REQUEST_ENABLE_BT = 1;
	private static final int REQUEST_DEVICE = 2;
    private ArrayList<OrderItemList> ordersarraylist ;
	private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private BluetoothAdapter mBtAdapter;
	private BluetoothSocket mBtSocket;
	private FMP10TZA mFMP;
    boolean isConnected = false;
    String customer = "";
    boolean tabletSize = false;

    TextView txtprintreport;
    Button btnprint;
	
	private final Handler mHandler = new Handler();
	
	private interface MethodInvoker {
		public void invoke() throws IOException;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.print);

        Bundle b = getIntent().getExtras();
        Intent i = getIntent();
        ordersarraylist = (ArrayList<OrderItemList>)b.getSerializable("key");

         customer = i.getExtras().getString("customer");


        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBtAdapter != null) {
        	if (mBtAdapter.isEnabled()) {
        		selectDevice();
        	} else {
        		enableBluetooth();
        	}
        } else {
        	Toast.makeText(this, R.string.msg_bluetooth_is_not_supported, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
       /* if(isConnected){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }*/
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();    	    	
    	disconnect();
    }
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case REQUEST_ENABLE_BT: {
				if (resultCode == RESULT_OK) {
					selectDevice();
				} else {
                    finish();
                    //fails return to activity
				}
				break;
			}
			case REQUEST_DEVICE: {
				if (resultCode == RESULT_OK) {
					String address = data.getStringExtra(DeviceActivity.EXTRA_ADDRESS);
				    connect(address);
                    AppPreference appPreference = new AppPreference(this);
                    appPreference.saveAddress(address);
				} else {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
				}
				break;
			}
		}
	}

	private void enableBluetooth() {
    	Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);	    
    }
    
    private void selectDevice() {
    	Intent selectDevice = new Intent(this, DeviceActivity.class);
	    startActivityForResult(selectDevice, REQUEST_DEVICE);	    
    }
    
    private void postToast(final String text) {    	
    	mHandler.post(new Runnable() {			
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();*/
//                txtprintreport.setText(text);
			}
		});
    }
    
    public void connect(final String address) {
    	invokeHelper(new MethodInvoker() {			
			@Override
			public void invoke() throws IOException {
				final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
				final BluetoothSocket socket = device.createRfcommSocketToServiceRecord(SPP_UUID);
				socket.connect();
				
				mBtSocket = socket;
				final InputStream in = socket.getInputStream();
				final OutputStream out = socket.getOutputStream();					
				mFMP = new FMP10TZA(in, out);
				postToast("Connected");

                /* start printing*/
                mFMP.openFiscalCheck();
                mFMP.cmd54v0b0(" *   "+customer.toUpperCase()+"   *");
                for(OrderItemList itemList: ordersarraylist) {
                    String prodname = itemList.getProductname();
                    double price = itemList.getUnitprice();
                    int qty = itemList.getQuantity();
                    double tax = itemList.getTax();
                    price = price+(price * tax/100);
                    String txtgrp = "";
                    if(tax == 0){
                        txtgrp = "B";
                    } else if(tax == 18){
                        txtgrp = "A";
                    } else {
                        txtgrp = "B";
                    }

                    mFMP.sellThis(prodname, txtgrp, String.valueOf(price),String.valueOf(qty));
                }
                mFMP.totalInCash();
                mFMP.closeFiscalCheck();
//                postToast(getString(R.string.strprintsuccess));
                /*finish printing*/

//                Toast.makeText(PrintUtils.this, R.string.strprintsuccess, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
			}
		});
	}

    public synchronized void disconnect() {
    	if (mBtSocket != null) {
    		try {
				mBtSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
    
    private void invokeHelper(final MethodInvoker invoker) {
    	final ProgressDialog dialog = new ProgressDialog(this);
    	dialog.setMessage(getString(R.string.msg_please_wait));
		dialog.setOnKeyListener(new OnKeyListener() {					
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				return true;
			}
		});
		dialog.show();
		
    	final Thread t = new Thread(new Runnable() {			
			@Override
			public void run() {				
				try {
					invoker.invoke();				
		    	} catch (final IOException e) { // Critical exception
		    		e.printStackTrace();		    		
		    		disconnect();   		
		    		selectDevice();
		    	} finally {
		    		dialog.dismiss();
		    	}
			}
		});

    	t.start();
    }
    
    private void panicOperation() {
    	invokeHelper(new MethodInvoker() {
            @Override
            public void invoke() throws IOException {
                mFMP.checkAndResolve();

            }
        });
    }
    
    private void nonFiscalReceipt() {
    	invokeHelper(new MethodInvoker() {			
			@Override
			public void invoke() throws IOException {
				mFMP.cmd38v0b0();
				mFMP.cmd42v0b0("  * SIMPLE NON FISCAL TEXT*");
				mFMP.cmd39v0b0();				
			}
		});
    }
    
    private void fiscalReceipt(final List<OrderItemList> itemLists, final String custname) {
        Constants.writeXml(itemLists);
    	invokeHelper(new MethodInvoker() {		
			@Override
			public void invoke() throws IOException {

				mFMP.openFiscalCheck();
                mFMP.cmd54v0b0(" *    " + custname.toUpperCase() + "    *");
                for(OrderItemList itemList: itemLists) {
                    String prodname = itemList.getProductname();
                    double price = itemList.getPrice();
                    int qty = itemList.getQuantity();
                    mFMP.sellThis(prodname, "B", String.valueOf(price),String.valueOf(qty));
                }
                mFMP.totalInCash();
				mFMP.closeFiscalCheck();
			}
		});
    }
}
