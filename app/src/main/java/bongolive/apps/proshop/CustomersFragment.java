/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

import android.accounts.Account;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.ContentProviderEngine;
import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.networking.Constants;


public class CustomersFragment extends Fragment implements OnItemClickListener,OnClickListener
{
	private static final String ARG_SECTION_NUMBER = "section_number";
	AlertDialogManager alert ;
	View _rootView; 
	List<ItemList> list;
    ListView lv ; 
    Button sub ;
	Dialog dialog ; 
	ItemListAdapter adapter ;
    EditText clname,shname,phone,email,address ;
	ArrayList<String> deletearray;
    SearchManager searchManager;
    SearchView searchView;
    MenuItem  deleteitem, addproductitem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {  
		if(_rootView == null)
		{
			_rootView = inflater.inflate(R.layout.list_items, container, false);
		} else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER)); 
        alert = new AlertDialogManager(); 
        
        lv = (ListView)getActivity().findViewById(R.id.list);
		lv.setItemsCanFocus(false);

        adapter = new ItemListAdapter(getActivity(), getList(), 1);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

		setHasOptionsMenu(true);
		deletearray = new ArrayList<>();
    }


	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {


		LinearLayout ll = ((LinearLayout) view.findViewById(R.id.itemlayout));
		ll.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ItemList _list = list.get(position);
				String name = _list.getitemname();
				long i = _list.getId();

				Uri uri = Uri.withAppendedPath(Customers.BASEURI, String.valueOf(id));
                if (i > 0)
                    showCustomer(i, name, 0);
            }
		});
		ImageView imgedit = ((ImageView)view.findViewById(R.id.imgedit));
		imgedit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ItemList _list = list.get(position);
				String name = _list.getitemname();
				long i = _list.getId();

				Uri uri = Uri.withAppendedPath(Customers.BASEURI, String.valueOf(id));
                if (i > 0)
                    showCustomer(i, name, 1);
            }
		});
		final CheckBox chkdrop = ((CheckBox)view.findViewById(R.id.chkdrop));

		ItemList _list = list.get(position);
		final long i = _list.getId();

        chkdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i > 0)
                    if (chkdrop.isChecked()) {
                        deleteitem.setVisible(true);
                        addproductitem.setVisible(false);
                        deletearray.add(String.valueOf(i));
                    } else {
                        deletearray.remove(String.valueOf(i));
                    }
            }
        });
	}
	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.addcustomer, menu);
        deleteitem = menu.findItem(R.id.action_delete);
        addproductitem = menu.findItem(R.id.action_addcustomer);

         searchManager =  (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.strcustomersearchhint));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));


        super.onCreateOptionsMenu(menu, inflater);
    }

	public List<ItemList> getList(){
		ContentResolver cr = getActivity().getContentResolver() ;
		String where = Customers.SYNCSTATUS + " != 3";
        Cursor c = cr.query(Customers.BASEURI, null, where, null, Customers.ID+ " DESC");
        list = new ArrayList<ItemList>();
        try{
        if(c.getCount() > 0)
        { 
        	c.moveToFirst() ;
        	do{
        	int colN = c.getColumnIndex(Customers.BUZINESSNAME);
        	int colP = c.getColumnIndex(Customers.MOBILE);
        	int colI = c.getColumnIndex(Customers.ID);
        	int colD = c.getColumnIndex(Customers.CITY);
            int colPN = c.getColumnIndex(Customers.CUSTOMERNAME);
        	String n = c.getString(colN);
        	String p = c.getString(colP);
            String pn = c.getString(colPN);
        	long i = c.getLong(colI) ;
        	String d = c.getString(colD);
        	//String[] detailsc = Customers.getCustomer(getActivity(), colI);
        	
        	list.add(new ItemList(getString(R.string.strbusiness).toUpperCase(Locale.getDefault())+ ": "+
        	n.toUpperCase(Locale.getDefault()),
                    getString(R.string.strclientname).toUpperCase(Locale.getDefault())+ ": "+
                            pn.toUpperCase(Locale.getDefault()),
                    getString(R.string.strcontact).toUpperCase(Locale.getDefault())+ ": "+p, i,
        			getString(R.string.straddress).toUpperCase(Locale.getDefault())+ ": "+d));
        	} while (c.moveToNext()) ;  
        } else {
        	String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault()) ;
        	String p = " " ; 
        	long i = 0 ;
        	String d = null ;
        	list.add(new ItemList(n,"", p, i,d));
        	lv.setAdapter(adapter); 
        } 
        }finally{
			if(c != null) {
	            c.close();
	        }
		}
        return list ;
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addcustomer:
                LayoutInflater infl = getActivity().getLayoutInflater();
                View view = infl.inflate(R.layout.addcustomer, null);
                dialog = new Dialog(getActivity(), R.style.CustomDialog);
                dialog.setCancelable(false);
                dialog.setContentView(view);
                dialog.show();
                sub = (Button) dialog.findViewById(R.id.btnSubmit);
                TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
                String title = getResources().getString(R.string.straddcustomer);
                title.toUpperCase(Locale.getDefault());
                txttitle.setText(title);

                ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
                goback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                clname = (EditText) dialog.findViewById(R.id.etclientname);
                shname = (EditText) dialog.findViewById(R.id.etshopname);
                phone = (EditText) dialog.findViewById(R.id.etclientphone);
                email = (EditText) dialog.findViewById(R.id.etclientemail);
                address = (EditText) dialog.findViewById(R.id.etclientshopaddress);

                sub.setOnClickListener(this);
                break;
            case R.id.action_search:
                getActivity().onSearchRequested();
                break;
            case R.id.action_delete:
                if (deletearray.size() > 0) {
                    for (int i = 0; i < deletearray.size(); i++) {
                        Customers.deleteCustomer(getActivity(), deletearray.get(i));
                    }

                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                    deleteitem.setVisible(false);
                    addproductitem.setVisible(true);
                }
                break;
        }

		return super.onOptionsItemSelected(item);
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.btnSubmit:
			String strclname,strshname,strclemail,strclphone,strcladdress ;
			strclname = clname.getText().toString();
			strshname = Validating.stripit(shname.getText().toString());
			strclemail = Validating.stripit(email.getText().toString());
			strclphone = Validating.stripit(phone.getText().toString());
			strcladdress = address.getText().toString();
			EditText[] et = new EditText[]{clname,shname,phone,email,address} ;
			String[] input = new String[]{strclname,strshname,strclphone,strclemail,strcladdress} ;
            String[] inputcheck = new String[]{strclname,strshname,strclphone,strcladdress} ;

            if(Validating.areSet(inputcheck)){
				 if(strclemail.isEmpty()) {
                     int insert = Customers.insertCustomerLocal(getActivity(), input);
                     adapter.notifyDataSetChanged();
                     if (insert == 1) {
                         adapter = new ItemListAdapter(getActivity(), getList(), 1);
                         adapter.notifyDataSetChanged();
                         lv.setAdapter(adapter);
                         Toast.makeText(getActivity(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                         dialog.dismiss();
                     } else {
                         Toast.makeText(getActivity(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                         dialog.dismiss();
                     }
                 } else if (!strclemail.isEmpty() && Validating.isEmailValid(strclemail)){
                     int insert = Customers.insertCustomerLocal(getActivity(), input);
                     if (insert == 1) {
                         Toast.makeText(getActivity(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                         adapter.notifyDataSetChanged();
                         adapter = new ItemListAdapter(getActivity(), getList(), 1);
                         lv.setAdapter(adapter);
                         dialog.dismiss();
                     } else {
                         Toast.makeText(getActivity(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                         dialog.dismiss();
                     }
                 } else {
                         et[3].setHint(getResources().getString(R.string.strinputerror)+
                                 " "+ getResources().getString(R.string.strclientemail));
                         et[3].setHintTextColor(Color.RED) ;
                         et[3].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}

			} else {
				 if(!Validating.areSet(strclname)){
				  et[0].setHint(getResources().getString(R.string.strinputerror) +
						  " "+ getResources().getString(R.string.strclientname));
				  et[0].setHintTextColor(Color.RED) ;
				  et[0].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
				  }
				 if(!Validating.areSet(strshname)){
					  et[1].setHint(getResources().getString(R.string.strinputerror)+
							  " "+ getResources().getString(R.string.strclientshopname));
					  et[1].setHintTextColor(Color.RED) ;

					  et[1].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_account_box_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
				 if(!Validating.areSet(strclphone)){
					  et[2].setHint(getResources().getString(R.string.strinputerror)+
							  " "+ getResources().getString(R.string.strclientphone));
					  et[2].setHintTextColor(Color.RED) ;
					  et[2].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_local_phone_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
				  if(!Validating.areSet(strcladdress)){
					  et[4].setHint(getResources().getString(R.string.strinputerror)+
							  " "+ getResources().getString(R.string.strclientaddress));
					  et[4].setHintTextColor(Color.RED) ;
					  et[4].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pin_drop_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
				return;
			}
			break;

		}
	}
	
	private void showCustomer(final long i, final String businessname, int action){
		LayoutInflater infl = getActivity().getLayoutInflater();
		View view = infl.inflate(R.layout.addcustomer, null) ;
		
		dialog = new Dialog(getActivity(), R.style.CustomDialog	);
		dialog.setCancelable(true);
		dialog.setContentView(view);

		clname = (EditText)dialog.findViewById(R.id.etclientname);
		shname = (EditText)dialog.findViewById(R.id.etshopname);
		phone = (EditText)dialog.findViewById(R.id.etclientphone);
		email = (EditText)dialog.findViewById(R.id.etclientemail);
		address = (EditText)dialog.findViewById(R.id.etclientshopaddress) ;
		TextView txttitle = (TextView)dialog.findViewById(R.id.txttitle);
		txttitle.setText(businessname);


		ImageView goback = (ImageView)dialog.findViewById(R.id.goback);
		goback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		Button edit = (Button)dialog.findViewById(R.id.btnSubmit);

		if(action == 0){
			clname.setFocusable(false);
			shname.setFocusable(false);
			email.setFocusable(false);
			phone.setFocusable(false);
			address.setFocusable(false);
			clname.setFocusableInTouchMode(false);
			shname.setFocusableInTouchMode(false);
			phone.setFocusableInTouchMode(false);
			address.setFocusableInTouchMode(false);
			email.setFocusableInTouchMode(false);
			edit.setVisibility(View.GONE);
		}


		final String[] details = Customers.getCustomer(getActivity(), i);
		 clname.setText(details[0]);
			shname.setText(details[1]);
			email.setText(details[2]);
			phone.setText(details[3]);
			address.setText(details[4]);

		edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String cl = clname.getText().toString();
				String sh  = shname.getText().toString();
				String em = email.getText().toString();
				String ph = phone.getText().toString();
				String add = address.getText().toString();
				int userid = Customers.getCustomerlocalId(dialog.getContext(), details[1]);
				String[] vals = {cl,sh,ph,em,add,String.valueOf(userid)};
				String[] vals2 = {cl,sh,ph,add};
				if(Validating.areSet(vals2)){
					int update = Customers.updateCustomerDetails(dialog.getContext(), vals);
					if(update > 0){
						Toast.makeText(dialog.getContext(), "You have edited customer details",Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					} else {
						Toast.makeText(dialog.getContext(), "No update for customer details",Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				}
			}

		});
		 dialog.show();	
	}


    private List<ItemList> getCustomerHistory(long i) {
        ContentResolver cr = dialog.getContext().getContentResolver();
        String where = Order.LOCALCUSOMER + " = " + i;
        Cursor c = cr.query(Order.BASEURI,null,where,null,null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if(c.moveToFirst()){
            do{
                int colN = c.getColumnIndex(Order.TOTALSALES);
                int colP = c.getColumnIndex(Order.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Order.ID);
                int colD = c.getColumnIndex(Order.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI) ;
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault())+ ": "+
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault())+ ": "+p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault())+ ": "+d));
            } while (c.moveToNext());
        }
        return itemLists;
    }

    public void ondemandsync()
    {
        Account mAccount = mAccount = Constants.createSyncAccount(getActivity());
        Bundle settingsBundle = new Bundle() ;
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderEngine.AUTHORITY, settingsBundle);
    }
/* todo
 * add search functionality
 */
}
