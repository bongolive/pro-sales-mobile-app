/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StockItemListAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
AppPreference appPreference ;
Context _context ;
private List<StockItemList> mItems = new ArrayList<StockItemList>() ;
public StockItemListAdapter(Context context, List<StockItemList> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
    this._context = context;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
	
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.fixedrowstockfull, null);
			holder = new ItemViewHolder() ;
			holder.txtprodname = (TextView)convertView.findViewById(R.id.autoproduct000);
			holder.txtquantity = (TextView)convertView.findViewById(R.id.autoquantity000); 
			holder.txtprice = (TextView)convertView.findViewById(R.id.autoprice000); 
			holder.txtindex = (TextView)convertView.findViewById(R.id.txtlabelindex); 
			holder.txtexpir = (TextView)convertView.findViewById(R.id.expirydate000);
			holder.txtproid = (TextView)convertView.findViewById(R.id.autoproid000);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		StockItemList cl = mItems.get(position);
		holder.txtprodname.setText(cl.getProductname());
		holder.txtquantity.setText(String.valueOf(cl.getQuantity())); 
		holder.txtprice.setText(String.valueOf(cl.getPrice())); 
		holder.txtindex.setText(cl.getId());
		holder.txtexpir.setText(cl.getExpirydate());
		holder.txtproid.setText(String.valueOf(cl.getProdid()));
        appPreference = new AppPreference(_context.getApplicationContext());
        String lang = appPreference.getDefaultCurrency();
        if(lang.equals("Tshs") || lang.isEmpty() || lang.equals("-1")){
            holder.txtprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dptz,0,0,0);
        } else{
            holder.txtprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dp,0,0,0);
        }
		return convertView ;
	} 
	static class ItemViewHolder {
		TextView txtprodname ;
		TextView txtquantity ; 
		TextView txtprice ; 
		TextView txtindex;
		TextView txtexpir;
		TextView txtproid;
	}
  
}