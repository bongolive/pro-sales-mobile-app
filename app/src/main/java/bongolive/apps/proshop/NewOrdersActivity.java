/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.fiscalprinter.tza.FMP10TZA;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.db.Customers;
import bongolive.apps.proshop.db.Order;
import bongolive.apps.proshop.db.OrderItems;
import bongolive.apps.proshop.db.PaymentMode;
import bongolive.apps.proshop.db.Products;
import bongolive.apps.proshop.networking.Constants;

public class NewOrdersActivity extends ActionBarActivity implements  OnItemClickListener,
        TextWatcher,OnCreateContextMenuListener, OnFocusChangeListener
{

	Button btnSave,btnSavePrint;
	static String customeridLocal , customerid ;/*customerid captures the customer id*/;
	TextView txreportdata,txtheading ; 
    ListView  lvitems ;
    Spinner sp ;
    AutoCompleteTextView edclientname,etproduct ;
    EditText etquantity,etprice,etpartial ;
    Dialog dialogmode ;
    CheckBox chkreceipt,chkfull,chkpartial;
    TextView txtsku,txtprice  ;
     AppPreference appPreference;
    String priceval="",finalcost = "";
    public static String _customer = "";
    boolean tabletSize = false;

    ArrayAdapter<String> adapters, proadapter,paymodeadapter,paystatusadapter ;
    OrderItemListAdapter adapter;
	String[] businesslist,productlist,paymode,paystatus;
	TextView txtorderitem ;
	String paymodess = "", amountpaid = "";
	static int __stockquantity ;
    int receipt = 0, payinfull = 0, quantitytype = 0;
	String orderstatus = null;
	static String _prodname ;
    private static final int REQUEST_PRINT = 1;
    Button btnsaveorder,btnadditem;

	private ArrayList<OrderItemList> marraylist = new ArrayList<OrderItemList>();
   @Override
   public void onCreate(Bundle savedInstanceState)
   {
	   super.onCreate(savedInstanceState);

       tabletSize = getResources().getBoolean(R.bool.isTablet);
       if(!tabletSize){
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
       }
	   setContentView(R.layout.addsales);

       appPreference = new AppPreference(this.getApplicationContext());

       createSalesForm();

	   ActionBar ab = getSupportActionBar();
	   ab.setDisplayShowTitleEnabled(false);
	   ab.setIcon(R.drawable.ic_cancel_black_36dp_sale);
	   ab.setDisplayUseLogoEnabled(true); 
	   ab.setDisplayHomeAsUpEnabled(true);
       ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
   }

   private void createSalesForm() { 
       lvitems = (ListView)findViewById(R.id.orderlist);  
         adapter = new OrderItemListAdapter(this,  marraylist); 
         lvitems.setAdapter(adapter) ; 
         lvitems.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
			
			@Override
			public void onCreateContextMenu(ContextMenu menu, View view,
					ContextMenuInfo menuInfo) {  
			    getMenuInflater().inflate(R.menu.removesale, menu);  
			    menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
	             menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp); 
	             AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
	             int position = info.position;
	             OrderItemList list = marraylist.get(position);
	             Toast.makeText(NewOrdersActivity.this, "ok "+list.getPrice(), Toast.LENGTH_LONG).show();
	 	    	double sales = list.getPrice();
	 	    	String total = txreportdata.getText().toString();
	 	    	if(Validating.areSet(total)){
	 	    	double remained = Double.parseDouble(total);
	 	    	double rem = remained - sales ;
	 	    	 txreportdata.setText(String.valueOf(rem));
	 	    	}
	 	    	 marraylist.remove(position);
	 	    	 adapter.notifyDataSetChanged();
	 	    	 txtorderitem.setText(getString(R.string.straddeditem) + " ("+ marraylist.size()+")" );
	        	 
	        	    
			}
		}) ;
         orderstatus = " ("+ marraylist.size()+")" ;
		txtorderitem = (TextView)findViewById(R.id.txtlabel);
		txtorderitem.append(orderstatus) ;
        txtsku = (TextView)findViewById(R.id.productskuvalue);
        txtsku.setOnFocusChangeListener(this);
        txtprice = (TextView)findViewById(R.id.txtpricevalue);
        txtprice.setOnFocusChangeListener(this);
	    businesslist = Customers.getAllCustomers(this);
		productlist = Products.getAllProducts(this);
       if(tabletSize) {
           btnsaveorder = (Button) findViewById(R.id.btnsaveorder);
           btnadditem = (Button) findViewById(R.id.btnadditem);

           btnadditem.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
                   add_item();
               }
           });

           btnsaveorder.setOnClickListener(new OnClickListener() {
               @Override
               public void onClick(View v) {
                   saveOrder(0, finalcost, amountpaid);
               }
           });
       }
       
		adapters = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, businesslist);
		proadapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, productlist);
		
		    etproduct = (AutoCompleteTextView)findViewById(R.id.autoproduct00);
		    etproduct.addTextChangedListener(this);
		    etproduct.setOnItemClickListener(this);
            etproduct.setOnFocusChangeListener(this);
		    etproduct.setAdapter(proadapter); 
		    etproduct.setThreshold(1);
		    
		    etquantity = (EditText)findViewById(R.id.autoquantity00);
            etquantity.setOnFocusChangeListener(this);
		    etquantity.setOnFocusChangeListener(new OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        /* String p = etproduct.getText().toString();
                         if(!p.isEmpty()) {
                             chooseQty(p);
//                             if (quantitytype == 1 || quantitytype == 2)
                         }*/
                        etprice.setEnabled(true);
                    }
                }
            }) ;
		    etprice = (EditText)findViewById(R.id.autoprice00);
            String lang = appPreference.getDefaultCurrency();
            if(lang.equals("Tshs") || lang.isEmpty() || lang.equals("-1")){
                etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dptz,0,0,0);
            } else{
                etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dp,0,0,0);
            }

		    etprice.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        String q = etquantity.getText().toString();
                        String p = etproduct.getText().toString();
                        etprice.setText("");
                        int qa = Products.getAllProductQuantity(NewOrdersActivity.this, p);
                        if(Validating.areSet(new String[] {p,q})){

                        if (Integer.parseInt(q) > qa && Integer.parseInt(q) != 0) {
                            Toast.makeText(NewOrdersActivity.this, p + " " + getResources().getString(R.string.strsalesquantityerror1) +
                                    " " + qa + " " + getResources().getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                            etquantity.setText("");
                            etquantity.setHint(qa + " " + getString(R.string.strqtymax));
                            etprice.setText("");
                            etquantity.requestFocus();
                        } else {
                            double pr = 0;
                            double tx = 0;
                            if (!p.isEmpty()) {
                                pr = Products.getUnitPrice(NewOrdersActivity.this, p);
                                tx = Products.getTax(NewOrdersActivity.this, p);
                            }
                            double reportdata = 0;
                            double grandtotal = 0;

                            int istaxed = 1; /*appPreference.getVatStatus()*/;

                            if (istaxed == 0 && pr > 0) {
                                grandtotal = Constants.calculateTotalNoTax(NewOrdersActivity.this, pr, Integer.parseInt(q), tx);
                            } else if (istaxed == 1 && pr > 0) {
                                grandtotal = Constants.calculateTotalWithTax(NewOrdersActivity.this, pr, Integer.parseInt(q), tx);
                            }

                            etprice.setText(String.valueOf(grandtotal));
                            etprice.clearFocus();
                            String strfinalprice = txreportdata.getText().toString();
                            if (TextUtils.isEmpty(strfinalprice)) {
                                reportdata = 0;
                            } else {
                                reportdata = Double.parseDouble(strfinalprice);
                            }
                            double total = grandtotal + reportdata;
                            String totals = String.valueOf(total);
                            txreportdata.setText(totals);
                            finalcost = totals;
                        }
                    } else {
                        etprice.setEnabled(false);
                    }
                }
				}
			}) ;
		    etprice = (EditText)findViewById(R.id.autoprice00);

		 
		txreportdata = (TextView)findViewById(R.id.txtreportdata);
		 
		//client name is chosen once
		edclientname = (AutoCompleteTextView)findViewById(R.id.autotxtsaleclientname);
		edclientname.setAdapter(adapters);
		edclientname.setOnItemClickListener(this); 
		edclientname.setThreshold(1);
	}
	 
	public void getPaymode(final String totalCost){
		paymode = PaymentMode.getPaymod(this);

		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.spinnerlayout, null) ;
		
		dialogmode = new Dialog(this, R.style.CustomDialog	);
		dialogmode.setCancelable(false);
		dialogmode.setContentView(view);

        TextView txtfinal = (TextView)dialogmode.findViewById(R.id.txttitle);
        txtfinal.setText(dialogmode.getContext().getString(R.string.stramount) + " " + totalCost + " " + appPreference.getDefaultCurrency());
        btnSave = (Button)dialogmode.findViewById(R.id.btnprocess);
        btnSavePrint = (Button)dialogmode.findViewById(R.id.btnprocessprint);

        chkfull = (CheckBox)dialogmode.findViewById(R.id.chbfull);
        chkpartial = (CheckBox)dialogmode.findViewById(R.id.chbpartial);
        etpartial = (EditText)dialogmode.findViewById(R.id.etpartialpay);
        ImageView gobak = (ImageView)dialogmode.findViewById(R.id.goback);
        gobak.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmode.dismiss();
            }
        });

        chkfull.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etpartial.setVisibility(View.GONE);
                chkpartial.setChecked(false);
                payinfull = 0;
            }
        });
        chkpartial.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                NewOrdersActivity.this.etpartial.setVisibility(View.VISIBLE);
                etpartial.requestFocus();
                chkfull.setChecked(false);
                payinfull = 1;
                amountpaid = NewOrdersActivity.this.etpartial.getText().toString();
            }
        });


		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
                String str = NewOrdersActivity.this.etpartial.getText().toString();
                if (NewOrdersActivity.this.payinfull == 1)
                {
                    if (!str.isEmpty())
                    {
                        Double localDouble1 = Double.valueOf(str);
                        Double localDouble2 = Double.valueOf(totalCost);
                        if (localDouble1.doubleValue() <= localDouble2.doubleValue())
                        {
                            NewOrdersActivity.this.saveOrder(0, finalcost, str);
                            NewOrdersActivity.this.dialogmode.dismiss();
                            return;
                        }
                        Toast.makeText(NewOrdersActivity.this.dialogmode.getContext(),
                                NewOrdersActivity.this.getString(R.string.strwrongamount) + " " +
                                        totalCost, Toast.LENGTH_LONG).show();
                        return;
                    }
                    NewOrdersActivity.this.etpartial.requestFocus();
                    Toast.makeText(NewOrdersActivity.this.dialogmode.getContext(), NewOrdersActivity
                            .this.getString(R.string.strfilltheamount), Toast.LENGTH_LONG).show();
                    return;
                }
                NewOrdersActivity.this.saveOrder(0, finalcost, str);
                NewOrdersActivity.this.dialogmode.dismiss();
            }
		});

        btnSavePrint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = NewOrdersActivity.this.etpartial.getText().toString();
                if (NewOrdersActivity.this.payinfull == 1)
                {
                    if (!str.isEmpty())
                    {
                        Double localDouble1 = Double.valueOf(str);
                        Double localDouble2 = Double.valueOf(totalCost);
                        if (localDouble1.doubleValue() <= localDouble2.doubleValue())
                        {
                            NewOrdersActivity.this.saveOrder(1, NewOrdersActivity.this.finalcost, str);
                            NewOrdersActivity.this.dialogmode.dismiss();
                            return;
                        }
                        Toast.makeText(NewOrdersActivity.this.dialogmode.getContext(),
                                NewOrdersActivity.this.getString(R.string.strwrongamount) + " " +
                                        totalCost, Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                    NewOrdersActivity.this.etpartial.requestFocus();
                    Toast.makeText(NewOrdersActivity.this.dialogmode.getContext(), NewOrdersActivity
                            .this.getString(R.string.strfilltheamount), Toast.LENGTH_LONG).show();
                    return;
                }
                NewOrdersActivity.this.saveOrder(1, finalcost, str);
                NewOrdersActivity.this.dialogmode.dismiss();
            }
        });

        paymodess = "" ;
		sp = (Spinner)dialogmode.findViewById(R.id.lvpaymode);
        if(appPreference.get_accounttype() == 0){
            sp.setVisibility(View.GONE);
            paymodess = "1" ;
        }
		paymodeadapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, paymode);
		paymodeadapter.setDropDownViewResource(android.R.layout.select_dialog_item);
		sp.setAdapter(paymodeadapter);

		//sp.setSelection(-1);
        sp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String paymoding = (String) parent.getItemAtPosition(position);
                String paymodingid = PaymentMode.getPaymodId(NewOrdersActivity.this, paymoding);
                if (!paymodingid.isEmpty())
                    paymodess = paymodingid;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                dialogmode.show();
            }
        });



		dialogmode.show();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
	 
		 if(view.getId() == R.id.autotxtsaleclientname){
			String name = (String) parent.getItemAtPosition(position); 
			edclientname.setText(name);
			customerid = String.valueOf(Customers.getCustomerid(this, name));
             customeridLocal = String.valueOf(Customers.getCustomerlocalId(this,name));
			edclientname.clearFocus();
			etproduct.requestFocus();
		 } 
		 else if(view.getId() == etproduct.getId()){
			String pro = (String)parent.getItemAtPosition(position);
			etproduct.setText(pro); 
			_prodname = pro ;
			__stockquantity = Products.getAllProductQuantity(this, pro);
			edclientname.requestFocus();
		 }
	} 
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {  
	} 
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {  
		 
		 String clientname = edclientname.getText().toString();
			 if(TextUtils.isEmpty(clientname)){  
				    etproduct.clearFocus();
					edclientname.requestFocus();
					Toast.makeText(this, getResources().getString(R.string.strgetclient), Toast.LENGTH_LONG).show();
					marraylist.clear();
					adapter.notifyDataSetChanged();
				} else{  
					 customerid = String.valueOf(Customers.getCustomerid(this, clientname));
                 customeridLocal = String.valueOf(Customers.getCustomerlocalId(this,clientname));
				}
			  String productname = etproduct.getText().toString();
			 if(TextUtils.isEmpty(productname)){ 
					etproduct.requestFocus() ; 
					Toast.makeText(this, getResources().getString(R.string.strfilldata), Toast.LENGTH_LONG).show();
			    }  
			       
	           }  
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_sales_buttons, menu);
        MenuItem additem,saveitem;
        additem = (MenuItem)menu.findItem(R.id.action_add_item);
        saveitem = (MenuItem)menu.findItem(R.id.action_save);

        if(tabletSize){
            additem.setVisible(false);
            saveitem.setVisible(false);
        }

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case android.R.id.home:
			customerid = "";
            customeridLocal = "";
		    marraylist.clear();
		    paymodess = "" ;
		    adapter.notifyDataSetChanged(); 
		    Intent intent = new Intent();
             setResult(RESULT_CANCELED, intent) ;
             finish();
		case R.id.action_save:
			saveOrder(0,finalcost,amountpaid);
			break;
		case R.id.action_add_item:
			add_item();
			break;
		}
		
		return false;
		
	}
	private void add_item() {

		    String p = etproduct.getText().toString();
			String suserq = etquantity.getText().toString();
			String price = etprice.getText().toString();
			String[] va = new String[]{p,suserq,price} ;
			if(Validating.areSet(va)){
                if(Integer.parseInt(suserq) > 0 && Double.parseDouble(price) > 0) {
                    double pr = Products.getUnitPrice(this, p);
                    double tx = Products.getTax(this, p);
                    int proid = Products.getProdid(this, p);
                    double grandtotal = Double.parseDouble(etprice.getText().toString());

                    double grandtotalwithnotax = Constants.calculateTotalNoTax(NewOrdersActivity.this, pr, Integer.parseInt(suserq), tx);


                    long i = 0;
                    if (!marraylist.isEmpty() && marraylist != null) {
                        i = adapter.getCount() + 1;
                    } else {
                        i = 1;
                    }
                    double taxamount = grandtotal - grandtotalwithnotax;
                    if (!suserq.isEmpty() && grandtotal > 0 && pr > 0 && grandtotalwithnotax > 0) {
                        marraylist.add(new OrderItemList(p, Integer.parseInt(suserq), grandtotal, getString(R.string.strlabelindex) + " " + i,
                                pr, proid, grandtotalwithnotax, tx, taxamount));
                    }
                    //val[2],val[6],val[7] //totalwithtax,totalwithouttax,taxrate
                    adapter = new OrderItemListAdapter(this, marraylist);
                    adapter.notifyDataSetChanged();
                    txtorderitem.setText(getString(R.string.straddeditem) + " (" + marraylist.size() + ")");
                    lvitems.setAdapter(adapter);
                    Toast.makeText(this, p.toUpperCase(Locale.getDefault()) + " " + getString(R.string.straddedtocart), Toast.LENGTH_LONG).show();
                    etproduct.setText("");
                    etquantity.setText("");
                    etprice.setText("");
                    txtprice.setText("");
                    txtsku.setText("");
                } else {
                    Toast.makeText(this, getString(R.string.strzeroqty), Toast.LENGTH_LONG).show();
                }
			}  
	}

	private void saveOrder(int printstatus,String totalcost, String paid){
		if(marraylist.size() != 0){
            String address = appPreference.getAddress();
			if(!Validating.areSet(paymodess))
			{
				getPaymode(totalcost);
			}  else {
				OrderItemList list = null ;
				double payamount = 0 ;
	            double tax = 0;
	            int orderid = 0;
                double totalsalewithnotax = 0;
	            if(!TextUtils.isEmpty(paymodess) && !TextUtils.isEmpty(customerid)) 
				{
                    _customer = Customers.getCustomerBusinessName(this, customerid);
	            	for(int i=0; i<marraylist.size(); i++)
	            	{
	            		list = marraylist.get(i);
	            		
		                double sales = list.getPrice(); 
		                double taxes = list.getUnittax();
                        double salenotax = list.getGrandtotal();
                        totalsalewithnotax += salenotax;
		                tax += taxes ; 
		                payamount += sales ;
	            	}

	    			String status = "" ;

	    			String totaltax2 = String.valueOf(tax);
	    			String tsales2 = String.valueOf(payamount);
	    			String payamount2 = String.valueOf(totalsalewithnotax);
                    int iscustomersynced = Customers.isCustomerSynced(this, customeridLocal);

                    String[] processOrder = new String[7];
                    if(paid.isEmpty())
                        paid = tsales2;
                    if(iscustomersynced == 0){
                        processOrder = new String[]{customerid,status,paymodess,paid,tsales2,totaltax2,"2",customeridLocal};
                    } else if (iscustomersynced == 1){

                        processOrder = new String[]{customerid,status,paymodess,paid,tsales2,totaltax2,"0", customeridLocal};
                    }
	    		    int insertorder = Order.insertOrders(this, processOrder);
	    		    if(insertorder == 1){
                        int insert = 0;
	    				for(int i=0; i< marraylist.size(); i++) {
                            list = marraylist.get(i);

                            String productid = String.valueOf(list.getProdid());
                            String quantity = String.valueOf(list.getQuantity());
                            String sprice = String.valueOf(list.getUnitprice());
                            String taxrate = String.valueOf(list.getTax());
                            orderid = Order.getId(this);
                            String productname = String.valueOf(list.getProductname());
                            String[] orderitem = new String[6];

                            if (iscustomersynced == 1) {
                                orderitem = new String[]{productid, quantity, sprice, taxrate, String.valueOf(orderid), "0"};
                            } else if (iscustomersynced == 0) {
                                orderitem = new String[]{productid, quantity, sprice, taxrate, String.valueOf(orderid), "2"};
                            }

                            insert = OrderItems.insertItems(this, orderitem);
                            if(insert == 1)
                            Products.updateProductQty(this, new int[]{list.getProdid(),list
                                    .getQuantity(),1});
                        }
	    					if(insert == 1)
	    					{

                                    if(printstatus == 1){
                                        if(address.isEmpty()) {
                                            Intent selectDevice = new Intent(this, PrintUtils.class);
                                            Bundle b = new Bundle();
                                            b.putSerializable("key", marraylist);
                                            selectDevice.putExtras(b);
                                            selectDevice.putExtra("customer", _customer);
                                            startActivityForResult(selectDevice, REQUEST_PRINT);
                                        } else {
                                            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter().getDefaultAdapter();
                                            final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
                                            final BluetoothSocket socket;
                                                try {
                                                    socket = device.createRfcommSocketToServiceRecord(UniversalBluetoothInstance.SPP_UUID);
                                                    socket.connect();
                                                    final InputStream in = socket.getInputStream();
                                                    final OutputStream out = socket.getOutputStream();
                                                    FMP10TZA fmp10TZA = new FMP10TZA(in, out);
                                                    nonFiscalReceipt(fmp10TZA, marraylist, _customer);
                                                    socket.close();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }

                                            customerid = "" ;
                                            customeridLocal = "";
                                            paymodess = "" ;
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    } else {// do not print the receipts

                                        customerid = "" ;
                                        customeridLocal = "";
                                        paymodess = "" ;
                                        Intent intent = new Intent();
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }
	    					} 
	    				 }
	    			    }
				}
			}
	}


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String p = etproduct.getText().toString();
        if(txtprice.hasFocus() || txtsku.hasFocus() || etquantity.hasFocus() && !p.isEmpty()){
                int ppr = Products.getUnitPrice(NewOrdersActivity.this, p);
                String sku = Products.getSku(NewOrdersActivity.this, p);
                txtprice.setText(String.valueOf(ppr));
                txtsku.setText(sku);
        } else if (!hasFocus && !p.isEmpty()){
                int ppr = Products.getUnitPrice(NewOrdersActivity.this, p);
                String sku = Products.getSku(NewOrdersActivity.this, p);
                txtprice.setText(String.valueOf(ppr));
                txtsku.setText(sku);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PRINT: {
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            }
        }
    }
    private void nonFiscalReceipt(FMP10TZA mFMP, final List<OrderItemList> itemLists, final String custname) {
        try {
            mFMP.openFiscalCheck();

        mFMP.cmd54v0b0(" *   "+custname.toUpperCase()+"   *");
                for(OrderItemList itemList: itemLists) {
                    String prodname = itemList.getProductname();
                    double price = itemList.getUnitprice();
                    int qty = itemList.getQuantity();
                    double tax = itemList.getTax();
//                    price = price+(price * tax/100);
                    String txtgrp = "";
                    if(tax == 0){
                        txtgrp = "B";
                    } else if(tax == 18){
                        txtgrp = "A";
                    } else {
                        txtgrp = "B";
                    }

                    mFMP.sellThis(prodname, txtgrp, String.valueOf(price),String.valueOf(qty));
                }
                mFMP.totalInCash();
                mFMP.closeFiscalCheck();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
