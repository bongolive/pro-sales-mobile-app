/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class ItemList {
    String itemname;
    String itemname1;
    String itemname2;
    String itemname3;
    String itemname4;
    long id;

    /*public ItemList(String n, String p, long i, String a){
        itemname = n;
        itemname1  = p ;
        itemname2 = a ;
        id = i ;
    }*/
    public ItemList(String n, String pn, String p, long i, String a) {
        itemname = n;
        itemname3 = pn;
        itemname1 = p;
        itemname2 = a;
        id = i;
    }

    public ItemList(String n, String pn, String p, String a, long i) {
        itemname = n;
        itemname1 = pn;
        itemname2 = p;
        itemname3 = a;
        id = i;
    }

    public ItemList(String n, String pn, String p, long i, String a, String ex) {
        itemname = n;
        itemname3 = pn;
        itemname1 = p;
        itemname2 = a;
        id = i;
        itemname4 = ex;
    }

    public String getitemname() {
        return itemname;
    }

    public String getitemname1() {
        return itemname1;
    }

    public String getitemname3() {
        return itemname3;
    }

    public String getitemname4() {
        return itemname4;
    }

    public long getId() {
        return id;
    }

    public String getitemname2() {
        return itemname2;
    }

}
