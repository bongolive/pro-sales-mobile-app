/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.db.Settings;
import bongolive.apps.proshop.networking.Constants;
import bongolive.apps.proshop.networking.MCrypt;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class DeviceRegistration extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = DeviceRegistration.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;
    AppPreference appPreference;
    Button btn;

    public DeviceRegistration(Context context, ProgressBar pbar, TextView textView, Button btnstart) {
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
        appPreference = new AppPreference(context);
        btn = btnstart;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving settings");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("processing");
    }

    @Override
    protected Integer doInBackground(String... params) {
        Log.v("action", params[0]);
        int[] ret = {0};
        MCrypt mCrypt = new MCrypt();
        if (params[0].equals(Constants.TAGREGISTRATION)) {
            //register the device now
            try {
                JSONObject jsonObject = Settings.registerDevice(context);
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.SUCCESS)) {

                        String token = jsonObject.getString(Constants.AUTHTOKENSERVER);
                        String account = jsonObject.getString(Constants.DEVICEACCOUNTTYPESERVER);
                        String[] vals = {token, account};
                        if (Validating.areSet(vals)) {
                            if (vals[1].equals(Constants.ACCOUNT_TYPE_NORMAL)) {
                                appPreference.save_accounttype(0);
                            } else if (vals[1].equals(Constants.ACCOUNT_TYPE_PREMIUM)) {
                                appPreference.save_accounttype(1);
                            }
                            appPreference.save_auth(vals[0]);
                            ret[0] = 1;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //validate the token now
            try {

                JSONObject jsonObject = Settings.validateToken(context);
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.SUCCESS)) {
                        ret[0] = 1;
                    } else {
                        if (jsonObject.has(Constants.DEVICEACCOUNTTYPESERVER)) {
                            String account = jsonObject.getString(Constants.DEVICEACCOUNTTYPESERVER);
                            if (!account.isEmpty()) {
                                int acctype = appPreference.get_accounttype();
                                String currentac = null;
                                if (acctype == 0)
                                    currentac = Constants.ACCOUNT_TYPE_NORMAL;
                                if (acctype == 1)
                                    currentac = Constants.ACCOUNT_TYPE_PREMIUM;
                                if (!currentac.equals(account))
                                    ret[0] = 2;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret[0];
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        btn.setVisibility(View.VISIBLE);
        bar.setProgress(100);
        if (ret == 1) {
            txt.setText(context.getString(R.string.strvalidtkenmsg));
            btn.setText(context.getString(R.string.strstart));
        } else if (ret == 2) {
            new DeviceRegistration(context, bar, txt, btn).execute(Constants.TAGREGISTRATION);
        } else {
            txt.setText(context.getString(R.string.strtokenerrormsg));
            btn.setText(context.getString(R.string.strclose));
        }
    }
}
