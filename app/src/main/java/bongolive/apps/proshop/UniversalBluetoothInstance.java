/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.Toast;

import com.datecs.fiscalprinter.tza.FMP10TZA;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class UniversalBluetoothInstance extends Activity {
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_DEVICE = 2;

    public static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    public static BluetoothAdapter mBtAdapter;
    public static  BluetoothSocket mBtSocket;
    private FMP10TZA mFMP;

    private final Handler mHandler = new Handler();
    boolean tabletSize = false;

    private interface MethodInvoker {
        public void invoke() throws IOException;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.print);

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter != null) {
            if (mBtAdapter.isEnabled()) {
                selectDevice();
            } else {
                enableBluetooth();
            }
        } else {
            Toast.makeText(this, R.string.msg_bluetooth_is_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//    	disconnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT: {
                if (resultCode == RESULT_OK) {
                    selectDevice();
                } else {
                    finish();
                }
                break;
            }
            case REQUEST_DEVICE: {
                if (resultCode == RESULT_OK) {
                    String address = data.getStringExtra(DeviceActivity.EXTRA_ADDRESS);
                    AppPreference appPreference = new AppPreference(this);
                    appPreference.saveAddress(address);
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
//					connect(address);
                } else {
                    finish();
                }
                break;
            }
        }
    }

    private void enableBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    private void selectDevice() {
        Intent selectDevice = new Intent(this, DeviceActivity.class);
        startActivityForResult(selectDevice, REQUEST_DEVICE);
    }

    private void postToast(final String text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void connect(final String address) {
        invokeHelper(new MethodInvoker() {
            @Override
            public void invoke() throws IOException {
                final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
                final BluetoothSocket socket = device.createRfcommSocketToServiceRecord(SPP_UUID);
                socket.connect();

                mBtSocket = socket;
                final InputStream in = socket.getInputStream();
                final OutputStream out = socket.getOutputStream();
                mFMP = new FMP10TZA(in, out);
                postToast("Connected");

            }
        });
    }

    public void saveConnection(final String address) {
        invokeHelper(new MethodInvoker() {
            @Override
            public void invoke() throws IOException {
                final BluetoothDevice device = mBtAdapter.getRemoteDevice(address);
                final BluetoothSocket socket = device.createRfcommSocketToServiceRecord(SPP_UUID);
                socket.connect();

                mBtSocket = socket;
                final InputStream in = socket.getInputStream();
                final OutputStream out = socket.getOutputStream();
                mFMP = new FMP10TZA(in, out);
                postToast("Connected");

            }
        });
    }

    public synchronized void disconnect() {
        if (mBtSocket != null) {
            try {
                mBtSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void invokeHelper(final MethodInvoker invoker) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.msg_please_wait));
        dialog.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });
        dialog.show();

        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    invoker.invoke();
                } catch (final IOException e) { // Critical exception
                    e.printStackTrace();
                    disconnect();
                    selectDevice();
                } finally {
                    dialog.dismiss();
                }
            }
        });
        t.start();
    }
}
